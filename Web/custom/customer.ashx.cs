﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using System.Linq;
using System.Web;
using Omnicell.Services;
using System.Xml;

namespace Omnicell.Web.custom
{
    /// <summary>
    /// Summary description for customer
    /// </summary>
    public class customer : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {

            context.Response.ContentType = "text/plain";

            StringBuilder sbTrace = new StringBuilder();
            WriteMessageToFile("1 handler start for: " + context.Request.UserAgent.ToString() + " from: " + context.Request.UserHostAddress.ToString());

            try
            {
                Utilities util = new Utilities();

                // string xmlFile = context.Request["xmlfile"];
                string apiKey = context.Request.QueryString["apikey"];

                if (apiKey != "gmjak34qvtveqvyg0ua")
                {
                    WriteMessageToFile("2.1 invalid api key");

                    context.Response.Write("invalid api key");
                }
                else
                {
                    // setup the stream because it will be used one way or another
                    StreamReader sr = new StreamReader(context.Request.InputStream);
                    string strSr = sr.ReadToEnd();
                    // check to see if the batchprocess flag was passed in
                    string BatchProcess = "No";
                    if (context.Request.QueryString["NoProcess"] != null && context.Request.QueryString["NoProcess"] != "")
                        BatchProcess = context.Request.QueryString["NoProcess"];

                    if (BatchProcess.ToString() == "Yes")
                    {
                        WriteMessageToFile("2.5 Write out the contents to a file only.");
                        var fileName = "customer.xml";
                        string xmlfileNameWithPath = System.Web.HttpContext.Current.Server.MapPath("~/custom/data/" + fileName);
                        String holdXmlData = HttpUtility.HtmlDecode(strSr);
                        File.WriteAllText(xmlfileNameWithPath, holdXmlData);
                        // write the file name to the trace log
                        WriteMessageToFile("2.6 Wrote Stream to File customer.xml.");
                    }
                    else
                    {
                        // SMR - added logic here so the stream coming in will get written to file.
                        WriteMessageToFile("3 ParseCustomerXmlFile InputStream: ");

                        var stream = context.Request.InputStream;
                        stream.Position = 0;

                        var result = util.ParseCustomerXmlFile(stream);
                        // var result = util.ParseCustomerXmlFile(context.Request.GetBufferlessInputStream());

                        WriteMessageToFile("4 ParseCustomerXmlFile result: " + result.ToString());
                        // SMR - 20130904 -- Added to write out the feed that came in
                        string WriteFileFlag = "No";
                        if (context.Request.QueryString["WriteFile"] != null && context.Request.QueryString["WriteFile"] != "")
                            WriteFileFlag = context.Request.QueryString["WriteFile"];

                        if (WriteFileFlag == "Yes")
                        {
                            var fileName = "customer" + DateTime.Now.ToString("yyyy-dd-MM_hh-mm-ss") + ".txt";
                            string xmlfileNameWithPath = System.Web.HttpContext.Current.Server.MapPath("~/custom/data/" + fileName);
                            String holdXmlData = HttpUtility.HtmlDecode(strSr);
                            File.WriteAllText(xmlfileNameWithPath, holdXmlData);
                            // write the file name to the trace log
                            WriteMessageToFile("4.1 Wrote Stream to File: " + xmlfileNameWithPath.ToString());
                        }
                    }
                    context.Response.Write("completed.");
                }
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("Message:  {0}\n", ex.Message);
                if (ex.InnerException != null)
                    sb.AppendFormat("InnerException: {0}\n", ex.InnerException);
                if (ex.StackTrace != null)
                    sb.AppendFormat("StackTrace: {0}\n", ex.StackTrace);
                if (ex.Source != null)
                    sb.AppendFormat("Source: {0}\n", ex.Source);

                WriteMessageToFile("4.5 ParseCustomerXmlFile error: " + sb.ToString());
                context.Response.Write(sb.ToString());
            }
            WriteMessageToFile("10 Handler End for: " + context.Request.UserAgent.ToString() + " from: " + context.Request.UserHostAddress.ToString() + " on: " + DateTime.Now.ToString());
        }

        private void WriteMessageToFile(string strMessage)
        {
            // write trace log
            try
            {
                string fileNameWithPath = System.Web.HttpContext.Current.Server.MapPath("~/custom/data/" + "customer.txt");
                using (StreamWriter sw = File.AppendText(fileNameWithPath))
                {
                    sw.WriteLine(strMessage.ToString());
                }
            }
            catch (Exception ex)
            {
                // do something with the error
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}