SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

DECLARE @Major int, @Minor int, @Patch int, @Installed DateTime, @Prereqs int

Set @Major = 1;
Set @Minor = 0;
Set @Patch = 1;

Select @Prereqs = isnull(Count(InstallDate),0)  from [telligent_SchemaVersion] where Major=@Major and Minor=@Minor and Patch<@Patch

Select @Installed = InstallDate  from [telligent_SchemaVersion] where Major=@Major and Minor=@Minor and Patch=@Patch

--This patch is always run

If(1=1) --@Installed is null AND @Prereqs = @Patch)
	BEGIN
--## Schema Patch ##

Print 'Inserting License'

exec sp_ExecuteSQL N'truncate table dbo.cs_Licenses'

exec sp_ExecuteSQL N'insert into dbo.cs_Licenses (LicenseID, LicenseValue, InstallDate) values (''0b364dc8-e0f6-4e5e-833c-dc7c183586f2'',
N''<?xml version="1.0" encoding="utf-16"?>
<document>
  <name>Professional Services</name>
  <email>services@telligent.com</email>
  <company>Telligent Systems 6.x</company>
  <licenseID>dad55181-3260-4e07-b645-9a17379bc379</licenseID>
  <generated>12/19/2011 5:28:26 PM</generated>
  <licenses>
    <license>
      <version>6.x</version>
      <licenseType>CommunityServer</licenseType>
      <productName>Telligent Community</productName>
      <edition>Enterprise</edition>
      <expires>7/31/2012 11:59:59 PM</expires>
      <signature>Le1tOhgp2q9cgKnRuQUy4ztw7Iy28hZyS8YJqG5mJ5473V+YUzKohaManfiOrtAbopcO0vwDrZ8hCwK0QbiibsaQ2i9V84J9WSIGqSQqB4vGv35HDlke3TPtqbcVN721eiTsdaulpjg6cliuacgkmddIXbbJ3mXaEgxumKObYXN+pDsNWMEhgrt6kTBy4sm8C4mMs0cJLwivosx7xU1BB3d4XwKQinMdARXUo0BAWwbilV9L06IElE5HJYQFHyVzhknOcdGbMHYWEVT10QJLeci8+pdrszYDuu50eQLwE/oGXXiuxf94AHzf5eUSQPkK22/Uce8gMtsD7xMzgxZJZ9DGuaYHDeUYeOzyGxbE2kxDTM8/Iun5/bhfdH/aEycYl2tlCtvbtzZ38s/Q7KMfE70JuubZ8XuXELIjK5g1NRewY8qBQ4rD7wBxIClxkO/mpAhgIC9PQ0mq1nPimap2NolOiYYUwkTCX53M0V6lDwKXQRVDZUfaP4JK737I7n3Cu7GYYe4oTUZQwmhJ5dAFPmy13bMeKpgO4qWWMIyvjU5pYZ1hWyWcjmaWFp+klgCT7JbKWxga9802MFlA2gy3OIdu3lqI/BgRnFQBcdNaqTsiLrdtQHaF1cKb2imQWmMudcPdSi0jPhNHCbJzc4FsXGaRDyG1aXZ2yNl0S2dOodFNSIuXeVyyI6ucILTlwDr6Ye8WEGNtcqP2MoInrpZLWxFkLcOAJde67Sp/P3Yxi2rHsjY8QVMVva/2F0kQYSv4wSxcI/bu5uoNdNbAu0DcKL6DUbKiXq8MYiHUrAHNO2L2ZlMVbqYFKTaDdecugfwqGiO/k/pfGgr0ckstJ8EAeSdraZQ/BMXyVtkv6vDOroHjWKESoc2e3RmeDJTr4WdpKnLnORlrltDXFsh+0bZKLlUToBNcF+p4QfW2w6VkKrVJYhaVm+tgyZjFJ3uC2eUzBFoqYCF21bMdkgzGv0Tv8xVwW8FzMP6YN04L9Pr4zDjvEaZmCP7l0L8zcYY3NwhvjqFCx/SGLrV3aevyPS4sSBdvpo+6i5v55+kiA5Nl+rQr03i1FX4bJKp8Vg0qSpexj1HAou/G6Dc/k3tRXf+K/tOicKz8Sh0OmrpTGX2KZC6T7EeAEBEkzee4ohS3/uX5WMmfN8fxFu1nE9qGRZCj41hDzFYkjq+BSUuE+CLa0EISK11RqA+uDvnvhI51aIYDOzR+HunkCH6kaDpZRyPZTxM9ltT2mwR4wAEwL7thv4uhxzyzZrWUOqRrWXYik8hUQT8TtdJec+TKlvrNX9lqyA9w3dTRHXug1UiLdl0CL5eZBi4LFH2FqeBBMSe8K9IWCNUWm2a12EG+BMYvV0FSWp4npp9rXuLsqZ6TE0+w1dHHBLjYa2ScKTAZWKGKTsCmPCQlbrGE0Fykg9pFc0lm/D2XNzWFapuohjuyDwf+qqKkznuvOWjFGPSzYM9/aMm1A7n7SS1lqOAGwbGge1Mq3Q7BtqwAPEJtkmcHCiP/t8ICP1kffWi+PowfB6Vm2qHhIrybHTcx1f51wgKzjiZOtXEX7TjcYeGL68FqdM4Ebd+5HbyTvlyI+1JjHsfUO7h6wHAXPmlH34Cku/n4CpzzN3wBzJyHaf9E5Bdl+TvndSvKz4RV7me6vhuSY9Q4Zn1+Gsbh/3g7YyFDgYqFFfxVjxUKc2JDQ3r9dGv/JGU3EDiEpJhIEdbTOkds54QR29fJWtFHlOnec3FxymdUDIvHHM2/J4AmsTEwkaaHD7ZfjxxcNbvCE1eqRnVG1xOhLCgV8Ai3hwY4H2zoTowjhcjvceKbBNciz9C0MqHOEqU75ipeR2Qus99wzB/hCmE9L7fAHQMr2/zwH5FfiC99Z8KY7Jh3VxWZyIdDeR8kp5ep0fWu76FTlTAQAzmOTo4d0gYdfLwg5RLYwb6HQvavugUWAhK2TNv5zIf5StI1+dHZF4cTMYg+7l/cG1BGsLhr0DYvGfG8uZkqSPpGDH5JtvHXBr83oCVxU3gSImXjrIi4BvclJwyBqzYFSfk2xGOjpjPbz7jnHT3UCxmvlZsJfF1Rax99xHLC+E9knqQ2dHHAnBbac03URugPHUouAhYVgNsecE7OWwYeqApvIWGqZP6LtnvgSzE5Hula7quiRKiRSXDNdUYxVhSqN3ttUuWgZquN2o7/XIoiY1gDCf0BH+zXlRzuOevJC1CqKB7Ox/Fg+8yTJrd9bWaa3QrY05EYo4lMJOBtZJPQu5OsXoDCT2co0ZA5nVlRLH3H0Mw66K6dbnHUBSsG6ShHZ4FE9zTG1+HQKz9TIXF6FjW2aaAIH6uIQ82w4+c3QDiFruYApDvL7MG6FHYuZSHn5G0kIG3YaE87ccqiQ1JUQx05fWUkS5g1y8KVaiMKIfXYWRBaxVkwi6qkJ1BWuf+YQpCgHf8Xv5MS/4EvFqaw9YBI5rgaJMz8zIAtG2u+P/d5xr9k9Zm9QwJFophdOW0TXti/12ppI6RKPDOwni1F+rfrMYcovB76EFJ7mS/Fx1/vh23tGptCezqgK7UZPNZqTqzRaZGpoatZ0uL6PLf0Q/PQeRhRk78cKZvpm2KfR2aOkj7cJZ/p6sSryFUfyVp0HpVXhw7w6dv//PM52mzGD4t0n0zlGC8KrdbS2ikmb57hrSNa6e8WEHFougSlSTMm3300HXcUnGneLSqDwOvnQN+lN870OzjM+VWSVL8n7cVeuGqIWMs/sjZxphHxMuF0nZbhabq5Ok6gJbaPm1szffJOMhZiReMdUCM/BcWwXOzMB2TpNbw3p0/yYEfBGKpulrPrrSfpyWdoI3TRL3asWANUEP8x/DkGal7kOs2Qv5xSccS9ALkTP2c5nvS5mq752fWKneVNNMGrXWI2PMgUFNeCAlExIQiFOgLtEZXz/iKq+6XDFXN2zalJmEyQn/JjVNq5tAXTZ68QrcxLslc8jca6gMT0s532F5i2px9KZQXI0bxF7g24Ale0vmduP1NspFIgw+HqhiTBnjH0nbskRkMPEDRkbWrZGM7bYg2YMolfkLV/UAdBZiVriSCt5R0KBHjJoRLANxERWrj2jCi91nXClS8o6ereXZmYEFCWNfl6GG2tasG1g1XbbNJCn3Iib2xjp0Sf0VE71ZzaWDDyZr79Atlc/UBpAj6DtkfhXr9md6DX5X0e/R1UrgRW/NLv4G3z+BEQtGGciBGyvgbVAzJACUvAz5oR86UgGq2Dk7Fz39/q1AwX77+q3PSIxBO/MN2kWEX+ljH0V/bJAyoUiHgnquIDcpg3w456J6bnMJNNwbL9/M4M75Z6uNW6ZPK+r5c2F7VjGxNkns9RX1qnb0Wxq2uZw7e4L5AaeEXgDnitN3EJnGtDtyarg0iUgiyqO+wlnusu25VBLLX7A7YNx4zG5bXnsGNd2h5MSt9kjjKTPrEDI63Tqf96nOTdE8kZnUZHmys8u98L8Mk4+MQTYv/h3H1peowbKtbTGz+I7Wz8gGTP6japt0hizOB/bVO8iujB62I2XZigff16Dlo4orIZ/UEHd210mdxhZZj6/xgACkn9nkB71ccvkah6WNZuR16w9jEB/pnRVa8do7YNz1BhzwnnqWJ8nPdpEcSEKpp1N8uWp8G6xCZ2WcvZIqEVF47VggYx/zifOTqDdPTpj8V0gcJKMcdVVgHugn8eQ//QkFUf4tFUbriFd6TPiroxDMOlVGrBJ8Tu4h63MB1O/hBPzo/ZmoP6Gdtr06Pb26i7kJt2rWxlvI5tTJPDa6L5AiVxzF2cFijbL6hyOpP/8v5OrQHBerU5XofrCHUVdRj5Rb+k0bvsCJ1H9AObAFA03xYWcVRTQDlESxa3epPlmMQ=</signature>
    </license>
    <license>
      <version>6.x</version>
      <licenseType>SecurityModules</licenseType>
      <productName>Security Modules</productName>
      <moduleTypes>Windows, Forms, Cookies, Passport</moduleTypes>
      <expires>7/31/2012 11:59:59 PM</expires>
      <signature>nh9oFMNzsSPkhugMjwejJ1Ch3r0iwo/tjs5p8aPkDsKxUQtMESifJFlt0MCcn1c7fju+uKQKy4Ntt1VHaj/p/sacx3PrcjvT/ZE6Q4IOvDqH33KNG2ud+te2Ewo0oU/dRRpp2cN/rAZvQz4djwSl8f88zhSJgKF8Sz8JKPPSiMzGw3+kQYX3TKYf1+ro3EhktGWb74AFei9ncbI2pnMAtEewkFDdnMXF/Rkp1tSJ8vKU2ddmOGmzaNGxd3plBba9/2t0dsudI2Ji5bKHm6oCPoXNe3U6aPZN/ixLBiVj807yKkJBi2LTMXvvWxSSBM2UW8nRH3hF/mqQykrdNLEju7MsnA8N9W+hDAucdu+Zz4uhp6n4ZmeprOKlQSNUd17VBEfsj/7NggZuktfFfziYX/JH55xFmRJGcP1lfMd1hYQ7Sdx3WLqpeNPM/ZkkJqj5Wi7j0RRyoN4G/0CW7VNODx+PsuCGkf6m4k8PlsWxrmLkbPmqDp6SN0kTzvU86GChMAApjQHjr6/CTG6+NdkCwFcpEtKgW1Gw3VRmzvFyAUXJ4hGXCLU8vOE43/lFPSLZCkdOlL2x1UfL+oLRlU0e53zT/+hfochJZYOMFQyrgvL7C0sAKCUKjAIAF7fkzk7OGQ46F533u1X/BrsxuYzMeAz9eUy61d3ydhh57hfUORVpQn48XaODz3cbXZp4uRXnNEMSnQCAt+8=</signature>
    </license>
    <license>
      <version>6.x</version>
      <licenseType>MailGateway</licenseType>
      <productName>Mail Gateway</productName>
      <maxEmailAddresses>Unlimited</maxEmailAddresses>
      <expires>7/31/2012 11:59:59 PM</expires>
      <signature>pBc00+zLflk6JqtrDAKStvWTCeR2Zr3eDW6f9BtTJi95ni8HJi9ZhePtLr40tJOHxiaqA/EQrO8LfzsEgydJbUo9jPU/Ort7vMEtQyLTzVuqq+ydoArn0RAofEeEfofEvRyZjOzzQNeHZS+Ra6Nf74fYHY3EJ6TaeUfjZnBd478FgjA+9hzSPRWO7PeJJy2V2DXxmbSv5C/Ih5eKLEavF0KiB2QsJhff9ss3ySmC9+GAYLuCo2MVTnwrWNBQCBpNeIJ41ui7aG3Nw09/phr+bgRMpgcZNLd3ZgLVhqaMUrrMwj3XEGB8g6wlpe4d5zm4L+QH3hExPi8UDExAVEsHw7IU21TfYAGWQhNDu/nY7Pcfa3sf7pTsHZe6R5hwueK0E7u75p8FZdnXiSLwOBkHHRDaQUvrchx4QyOcTc/GBVv/xdv2knSUtR2vr9C7Q0ALQxGTocgzXdpIQr0zPJ8HcgyetlW1PKp8XwNv7+n1+HnE86cK0bk1CByMM5G/nzGKF8hSnNsGMqqAky4OZVW5XAq/z1bFIAmZ2i+B3ZGxuGEeYMZ4jK2J7isccQ2QDEwYKVp/ocuVMQmRqBJcxILvWIfHOMl4V+wgCENiBm79QMppqnZGe8AkeegGe1YhRRL3gOZx01xnIiTXwjUKgflgJRDis83+jcUs/uhZfZwXeNkKKFoW4Z3wIa4ht5QMIJPXuFqm7sZc0AD1q8LiJvjkgt4uTmI5czeoGZEWSFCZ1nBnaWTSh7SYiDWOBTeYBmG0YkucMwo5J6M=</signature>
    </license>
  </licenses>
<Signature xmlns="http://www.w3.org/2000/09/xmldsig#"><SignedInfo><CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315" /><SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1" /><Reference URI=""><Transforms><Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature" /></Transforms><DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1" /><DigestValue>sPUAiBiuRLsAsHc6eBDiXELas60=</DigestValue></Reference></SignedInfo><SignatureValue>lQsxXt94m/nljUXKJ5BfzMv6gx4bObmrjVTosSgHo9GtaCDq2nqyDxyRb5XtqdMaIOnOWdkOQPPBwh1ZPLqpijguktss5hrLG89Wkcl1IRp2hc+UE9QdySOyQUXHECkDZcoC2KppnWTzA1jxPnVir5RHphYtVM8/6cXw+UoJ6ck=</SignatureValue></Signature></document>'',
getdate())'



--## END Schema Patch ##
IF(@Installed is null)
  Insert into [telligent_SchemaVersion](Major, Minor, Patch, InstallDate) values (@Major, @Minor, @Patch, GetDate())

Print 'Schema Patch v' + Convert(Varchar(2),@Major) + '.' + Convert(Varchar(2),@Minor) + '.' +  Convert(Varchar(3),@Patch) + ' was applied successfully '

	END
ELSE IF(@Installed is not null)
	BEGIN
Print 'Schema Patch v' + Convert(Varchar(2),@Major) + '.' + Convert(Varchar(2),@Minor) + '.' +  Convert(Varchar(3),@Patch) + ' was already applied on ' + Convert(varchar(50), @Installed)  
	END 
ELSE
	BEGIN
Print 'The patch could not be applied because your current schema is missing previous updates (Schema Patch v' + Convert(Varchar(2),@Major) + '.' + Convert(Varchar(2),@Minor) + '.' +  Convert(Varchar(3),@Patch) + ')' 
	END 

