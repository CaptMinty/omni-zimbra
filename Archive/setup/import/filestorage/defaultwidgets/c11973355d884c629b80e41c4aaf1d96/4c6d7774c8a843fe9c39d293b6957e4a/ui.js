(function ($) {
    var deleteAddresses = function(context, addressIds) {
            $.telligent.evolution.post({
                    url: context.deleteUrl,
                    data: {
                            selectedAddresses: addressIds.join()
                    },
                    success: function(response) {
		          //window.location.href = window.location.href;
		          window.location.reload();
                    }
             });

        },
        addAddress = function(context, address, type) {
            $.telligent.evolution.post({
                    url: context.addUrl,
                    data: {
                            address: address,
                            addressType: type
                    },
                    success: function(response) {
		          //window.location.href = window.location.href;
		          window.location.reload();
                    }
            });
        };
    var api = {
//    $.widgets.listBannedAddressesWidget = {
        register: function (context) {
			context.wrapperId = $(context.wrapperId);
			$('a.addAddress', context.wrapperId).bind('click', function(e,data){
				var address = $('input#address', context.wrapperId).val();
				var type = $('select#addressType', context.wrapperId).val();
				
                                    if(!type || !address || type.length == 0 || address.length == 0) {
                                            alert('You must enter an address and select an address type before you can add it to the list of banned addresses.');
                                    }
                                    else if(confirm(context.addAddressConfirmMessage)){
					addAddress(context, address, type);
				}
				return false;
			});
			$('a.deleteAddresses', context.wrapperId).bind('click', function(e,data){
				var addresses = new Array();
                                    $('input:checked', context.wrapperId).each(function() {
                                        addresses.push($(this).val());
                                    });
				
                                    if(!addresses || addresses.length == 0) {
                                            alert('You must select one or more addresses, before you can delete them from the list of banned addresses.');
                                    }
			         else if(confirm(context.deleteConfirmMessage)){
					deleteAddresses(context, addresses);
				}
				return false;
			});

        }
    };
    
    if (typeof $.widgets == 'undefined') { $.widgets = {}; }
    $.widgets.listBannedAddressesWidget = api;

}(jQuery));