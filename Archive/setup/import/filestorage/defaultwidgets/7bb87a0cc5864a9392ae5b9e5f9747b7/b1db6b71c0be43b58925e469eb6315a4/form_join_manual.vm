##
## Form rendered when a new user is joining the site manually (with username/password/email)
##

## if there were auth providers, render them
#set ($hasOAuthProviders = false)
#set ($providers = $core_v2_authentication.ListOAuthProviders())
#if ($providers.Count > 0)
	#set ($hasOAuthProviders = true)
#end

<div class="page-header"></div>
<div class="page join-manual #if (!$requiresJoinDetails) no-details #end">
	<div class="page-content">

		<h2>$core_v2_language.GetResource('CreateNewAccount_Register')</h2>

		#if ($hasOAuthProviders)
			<div class="join-providers">
				<input type="hidden" name="$core_v2_encoding.HtmlAttributeEncode($providerInput)" id="$core_v2_encoding.HtmlAttributeEncode($providerInput)" />
				<div>
					$core_v2_language.FormatString($core_v2_language.GetResource('CreateNewAccount_JoinOAuth'), $siteInfo.SiteName)
					#set ($providerCount = 0)
					#foreach ($provider in $providers)
						<span class="login-provider" data-clienttype="$core_v2_encoding.HtmlAttributeEncode(${provider.ClientType})">
							<a href="#" class="internal-link connect submit-button">
								<span>$core_v2_ui.GetResizedImageHtml($provider.IconUrl, 16, 16, "%{border='0'}")</span>
								$provider.ClientName
							</a>
						</span>
						#set ($providerCount = $providerCount + 1)
					#between
						#if ($providerCount == $providers.Count - 1)
							<span> $core_v2_language.GetResource('CreateNewAccount_JoinOAuth_Divider_Last') </span>
						#else
							<span> $core_v2_language.GetResource('CreateNewAccount_JoinOAuth_Divider') </span>
						#end
					#end
				</div>
			</div>
		#end

		<h3>$core_v2_language.GetResource('CreateNewAccount_Join_Manual_Text')</h3>

		## Details
		#if ($requiresJoinDetails)
			<div class="field-list-header"></div>
			<fieldset class="field-list join-details">
				<legend class="field-list-description"><span>$core_v2_language.GetResource('CreateNewAccount_Details')</span></legend>
				<ul class="field-list">
					$core_v2_widget.ExecuteFile('sub_form_join_details.vm')

					<li class="field-item">
						<span class="field-item-input">
							<input type="hidden" name="$core_v2_encoding.HtmlAttributeEncode($actionInput)" id="$core_v2_encoding.HtmlAttributeEncode($actionInput)" value="join_manual" />
							<a href="#" class="internal-link create-account submit-button" >
								<span></span>$core_v2_language.GetResource('CreateNewAccount_CreateAccount')
							</a>
							<span class="processing" style="visibility: hidden;"></span>
						</span>
					</li>
				</ul>
			</fieldset>
			<div class="field-list-footer"></div>
		#end

		## Account Information
		<div class="field-list-header"></div>
		<fieldset class="field-list join-account #if (!$requiresJoinDetails) no-details #end">
			<legend class="field-list-description"><span>$core_v2_language.GetResource('CreateNewAccount_AccountInfo')</span></legend>
			<ul class="field-list">
				<li class="field-item required user-name">
					<label for="$core_v2_encoding.HtmlAttributeEncode($usernameInput)" class="field-item-header">$core_v2_language.GetResource('CreateNewAccount_UserName')</label>
					<span class="field-item-validation" style="display: none;"></span>
					<span class="field-item-input">
						<input type="text" name="$core_v2_encoding.HtmlAttributeEncode($usernameInput)" id="$core_v2_encoding.HtmlAttributeEncode($usernameInput)" MaxLength="64" size="31" value="$!core_v2_encoding.HtmlAttributeEncode($username)" />
					</span>
				</li>
				<li class="field-item required email">
					<label for="$core_v2_encoding.HtmlAttributeEncode($emailInput)" class="field-item-header">$core_v2_language.GetResource('CreateNewAccount_Email')</label>
					<span class="field-item-validation" style="display: none;"></span>
					<span class="field-item-description">$core_v2_language.GetResource('CreateNewAccount_EmailDescription')</span>
					<span class="field-item-input">
						<input type="text" name="$core_v2_encoding.HtmlAttributeEncode($emailInput)" id="$core_v2_encoding.HtmlAttributeEncode($emailInput)" MaxLength="128" size="31" value="$!core_v2_encoding.HtmlAttributeEncode($email)" />
					</span>
				</li>
        		#if($core_v2_configuration.AccountActivation == "Email" && !$requiresJoinDetails)
					<li class="field-item">
						<span class="field-item-input">
							<input type="hidden" name="$core_v2_encoding.HtmlAttributeEncode($actionInput)" id="$core_v2_encoding.HtmlAttributeEncode($actionInput)" value="join_manual" />
							<a href="#" class="internal-link create-account submit-button" >
								<span></span>$core_v2_language.GetResource('CreateNewAccount_CreateAccount')
							</a>
							<span class="processing" style="visibility: hidden;"></span>
						</span>
					</li>
        		#end
			</ul>
		</fieldset>
		<div class="field-list-footer"></div>

		## Passwords
		#if ($core_v2_configuration.AccountActivation != "Email" || $core_v2_nodePermission.Get('Site_ManageMembership').IsAllowed)
			<div class="field-list-header"></div>
			<fieldset class="field-list join-password #if (!$requiresJoinDetails) no-details #end">
				<ul class="field-list">
					<li class="field-item required password">
						<label for="$core_v2_encoding.HtmlAttributeEncode($passwordInput)" class="field-item-header">$core_v2_language.GetResource('CreateNewAccount_PasswordDescription')</label>
						<span class="field-item-validation" style="display: none;"></span>
						<span class="field-item-description">$core_v2_language.FormatString($core_v2_language.GetResource('CreateNewAccount_PasswordLimits_Join'), $core_v2_configuration.PasswordMinLength.ToString())</span>
						<span class="field-item-input">
							<input name="$core_v2_encoding.HtmlAttributeEncode($passwordInput)" id="$core_v2_encoding.HtmlAttributeEncode($passwordInput)" MaxLength="64" type="password" autocomplete="off" size="31" value="$!core_v2_encoding.HtmlAttributeEncode($password)" />
						</span>
					</li>
					<li class="field-item required password2">
						<label for="$core_v2_encoding.HtmlAttributeEncode($password2Input)" class="field-item-header">$core_v2_language.GetResource('CreateNewAccount_ReEnterPassword')</label>
						<span class="field-item-validation" style="display: none;"></span>
						<span class="field-item-input">
							<input name="$core_v2_encoding.HtmlAttributeEncode($password2Input)" id="$core_v2_encoding.HtmlAttributeEncode($password2Input)" MaxLength="64" type="password" autocomplete="off" size="31" value="$!core_v2_encoding.HtmlAttributeEncode($password2)" />
						</span>
					</li>
					#if($core_v2_recaptcha && $captchaEnabled)
                    <li class="field-item required captcha">
                    	<span class="field-item-validation" style="display: none;"></span>
                    	<span class="field-item-input">
                    	   $core_v2_recaptcha.Render()
                           <input type="hidden"  name="$core_v2_encoding.HtmlAttributeEncode($captchaHiddenInput)" id="$core_v2_encoding.HtmlAttributeEncode($captchaHiddenInput)"  />
                    	</span>
                    </li>
                    #end

					## if there were no details, then the join button won't be rendered inside details
					## and hence needs to be rendered here
					#if (!$requiresJoinDetails)
						<li class="field-item">
							<span class="field-item-input">
								<input type="hidden" name="$core_v2_encoding.HtmlAttributeEncode($actionInput)" id="$core_v2_encoding.HtmlAttributeEncode($actionInput)" value="join_manual" />
								<a href="#" class="internal-link create-account submit-button" >
									<span></span>$core_v2_language.GetResource('CreateNewAccount_CreateAccount')
								</a>
								<span class="processing" style="visibility: hidden;"></span>
							</span>
						</li>
					#end
				</ul>
			</fieldset>
			<div class="field-list-footer"></div>
		#end
		<div class="login-alternative">
			#set ($loginUrl = $core_v2_urls.Login())
			#if ($loginUrl)
				$core_v2_language.GetResource('CreateNewAccount_AlreadyJoined') <a href="$core_v2_encoding.HtmlAttributeEncode($loginUrl)">$core_v2_language.GetResource('CreateNewAccount_SignIn')</a>
			#end
		</div>
	</div>
</div>
<div class="page-footer"></div>



