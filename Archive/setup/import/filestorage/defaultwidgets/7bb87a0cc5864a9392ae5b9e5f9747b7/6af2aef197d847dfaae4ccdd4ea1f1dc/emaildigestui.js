(function($)
{
	if (typeof $.telligent === 'undefined')
	        $.telligent = {};

	if (typeof $.telligent.evolution === 'undefined')
	    $.telligent.evolution = {};

	if (typeof $.telligent.evolution.widgets === 'undefined')
	    $.telligent.evolution.widgets = {};

	if (typeof $.telligent.evolution.widgets.editUser === 'undefined')
	    $.telligent.evolution.widgets.editUser = {};
		
	var _more = function(context) {
		$.telligent.evolution.get({
			url: context.moreUrl,
			data: {
				w_pageIndex: context.pageIndex,
				w_pageSize: context.pageSize,
				w_userId: context.userId
			},
			success: function(response)
			{
				$('#' + context.tableBodyId).append(response);
				context.groupCountAdded += context.pageSize;
				if(context.groupCountAdded >= context.totalCount)
					$('#' + context.moreId).hide();
				context.pageIndex++;
			}
		});
	};
	
	$.telligent.evolution.widgets.editUser.emailDigest = {
		register: function(context) {
			var w = $('#' + context.wrapperId);
			context.pageIndex = 0;
			context.groupCountAdded = 0;
			context.joinlessGroupCountAdded = 0;
			
			_more(context);
			$('#' + context.moreId, w).click(function() {
				_more(context);
			});
		}
	};
})(jQuery);
