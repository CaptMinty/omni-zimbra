(function($){

	var validationMessage = '*',
		createContactRequest = function(context) {
			$.telligent.evolution.post({
				url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/groups/{GroupId}/contactrequests.json',
				data: {
					GroupId: context.groupId,
					Subject: context.subject.val(),
					Body: context.message.val(),
					Name: context.name.val(),
					EmailAddress: context.email.val()
				},
				success: function(response) {
					// show success message
					context.success.show();
					// empty the form
					context.subject.val('');
					context.message.val('');
					context.name.val('');
					context.email.val('');
				}
			});
		},
		api = {
			register: function(context) {
				context.subject = $(context.subject);
				context.name = $(context.name);
				context.email = $(context.email);
				context.message = $(context.message);
				context.success = $(context.success);
				context.submit = $(context.submit)
					.evolutionValidation({
						onValidated: function(isValid, buttonClicked, c) { },
						onSuccessfulClick: function(e) {
							e.preventDefault();
							createContactRequest(context);
						}})
					.evolutionValidation('addField',
						context.subject, {
							required: true,
							messages: {
								required: validationMessage
							}
						},
						context.subject.closest('.field-item').find('.field-item-validation'), null)
					.evolutionValidation('addField',
						context.name, {
							required: true,
							messages: {
								required: validationMessage
							}
						},
						context.name.closest('.field-item').find('.field-item-validation'), null)
					.evolutionValidation('addField',
						context.email, {
							required: true,
							email: true,
							messages: {
								required: validationMessage,
								email: validationMessage
							}
						},
						context.email.closest('.field-item').find('.field-item-validation'), null)
					.evolutionValidation('addField',
						context.message, {
							required: true,
							messages: {
								required: validationMessage
							}
						},
						context.message.closest('.field-item').find('.field-item-validation'), null);
			}
	};

	if (typeof $.telligent === 'undefined') { $.telligent = {}; }
	if (typeof $.telligent.evolution === 'undefined') { $.telligent.evolution = {}; }
	if (typeof $.telligent.evolution.widgets === 'undefined') { $.telligent.evolution.widgets = {}; }
	$.telligent.evolution.widgets.groupContact = api;

}(jQuery));
