(function(j, global)
{
	if (typeof j.telligent === 'undefined')
		j.telligent = {};

	if (typeof j.telligent.evolution === 'undefined')
		j.telligent.evolution = {};

	if (typeof j.telligent.evolution.widgets === 'undefined')
		j.telligent.evolution.widgets = {};

	var _save = function(context)
	{
		context.successMessage.hide();
		context.moderateMessage.hide();
		context.errorMessage.hide();
		var w = j('#' + context.wrapperId);

		context.save
			.html('<span></span>' + context.publishingText)
			.addClass('disabled');

		j.telligent.evolution.post({
			url: j.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/media/{MediaGalleryId}/files/{FileId}/comments.json?IncludeFields=Comment.Id,Comment.IsApproved',
			data:
			{
				Body: j(context.bodySelector).evolutionComposer('val'),
				MediaGalleryId: context.galleryId,
				FileId: context.mediaId
			},
			success: function(response)
			{
				j('.processing', w).css('visibility', 'hidden');

				if(response.Comment.IsApproved)
				{
					context.successMessage.slideDown();
					global.setTimeout(function() { context.successMessage.fadeOut().slideUp(); }, 9999);
					j(document).trigger('telligent_mediagalleries_commentposted', '');
				}
				else
				{
					context.moderateMessage.slideDown();
					global.setTimeout(function() { context.moderateMessage.fadeOut().slideUp(); }, 9999);
				}

				j(context.bodySelector).evolutionComposer('val','');
				j(context.bodySelector).change();
				context.save.evolutionValidation('reset');
				context.save.html('<span></span>' + context.publishText).removeClass('disabled');
			},
			error: function(xhr, desc, ex)
			{
				j('.processing', w).css("visibility", "hidden");
				context.save.html('<span></span>' + context.publishText).removeClass('disabled');
				context.errorMessage.html(context.publishErrorText + ' (' + desc + ')').slideDown();
			}
		});
	};

	j.telligent.evolution.widgets.addMediaGalleryPostComment =
	{
	    register: function (context) 
        {
            $('textarea').evolutionResize();

			var body = j(context.bodySelector);
			body.one('focus', function(){
				body.evolutionComposer({
					plugins: ['mentions','hashtags']
				});
			});

			if (document.URL.indexOf('#addcomment') >= 0)
				body.focus();

			j('.internal-link.close-message', j('#' + context.wrapperId)).click(function()
			{
				j(this).blur();
				j(this).closest('.message').fadeOut().slideUp();
				return false;
			});

			context.save.evolutionValidation(
			{
				onValidated: function(isValid, buttonClicked, c)
				{
					if (isValid)
						context.save.removeClass('disabled');
					else
						context.save.addClass('disabled');
				},
				onSuccessfulClick: function(e)
				{
					e.preventDefault();
					j('.processing', context.save.parent()).css("visibility", "visible");
					context.save.addClass('disabled');
					_save(context);
				}
			});

			context.save.evolutionValidation('addField',context.bodySelector,
			{
				required: true,
				maxlength: 1000000,
				messages:
				{
					required: context.bodyRequiredText
				}
			}, '#' + context.wrapperId + ' .field-item.post-body .field-item-validation', null);
		}
	};
})(jQuery, window);
