(function($){
	var countLoaded = function(context) {
		return context.wrapper.find('.abbreviated-post').length;
	};
	var api = {
		register: function(context) {
			context.wrapper = $(context.wrapper);
			context.viewMore = $(context.viewMore)
				.bind('click', function(e,data){

					var loadedCount = countLoaded(context);
					var pageIndex = loadedCount/context.pageSize;

					$.telligent.evolution.get({
						url: context.viewMoreUrl,
						data: {
							w_filters: context.filters,
							w_uniqueId: context.uniqueId,
							w_pageSize: context.pageSize,
							w_pageIndex: pageIndex
						},
						success: function(response) {
							context.wrapper.find('.abbreviated-post-footer:last').after(response);
							if(countLoaded(context) >= context.totalCount) {
								context.viewMore.hide();
							}
						}
					});

					return false;
				});
		}
	};

	if (typeof $.telligent === 'undefined') { $.telligent = {}; }
	if (typeof $.telligent.evolution === 'undefined') { $.telligent.evolution = {}; }
	if (typeof $.telligent.evolution.widgets === 'undefined') { $.telligent.evolution.widgets = {}; }
	$.telligent.evolution.widgets.relatedContent = api;

}(jQuery));
