(function($) {
	var createApiKey = function(context) {
		$.telligent.evolution.post({
			url: context.createUrl,
			data: {
				Name: $(context.nameInput).val()
			},
			success: function (response) {
				window.location.reload(true);
			}
		});		
	};
	var api = {
		register: function(context) {
			context.submitInput = $(context.submitInput)
				.evolutionValidation({
					onValidated: function(isValid, buttonClicked, c) { },
					onSuccessfulClick: function(e) {
						e.preventDefault();
						createApiKey(context);
						return false;
					}
				})
				.evolutionValidation('addField', $(context.nameInput), {
					required: true,
					messages: { required: '*' }
				},	$(context.nameInput).closest('.field-item').find('.field-item-validation'), null);
		}
	};

	if (typeof $.telligent === 'undefined') { $.telligent = {}; }
	if (typeof $.telligent.evolution === 'undefined') { $.telligent.evolution = {}; }
	if (typeof $.telligent.evolution.widgets === 'undefined') { $.telligent.evolution.widgets = {}; }
	$.telligent.evolution.widgets.createApiKey = api;

}(jQuery));
