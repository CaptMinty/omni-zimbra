(function($) {

	// returns the friend id related to a given child element of that friend id's row item
	var getFriendId = function(element) {
		return Number($(element).closest('li.content-item').attr('data-friendid'));
	};

	var api = {
		register: function(context) {
			// starting conversations
			$('#' + context.wrapperId + ' a.start-conversation').live('click', function(){
				var url = $(this).attr('href');
				Telligent_Modal.Open(url,550,360,null);
				return false;
			});

			// cancelling
			$('#' + context.wrapperId + ' a.cancel-friend').live('click', function(){
				if(confirm(context.friendshipRequestCancelConfirmation)) {
					$.telligent.evolution.del({
						url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/users/{RequestorId}/friends/{RequesteeId}.json',
						data: {
							RequestorId: context.userId,
							RequesteeId: getFriendId($(this))
						},
						success: function(){ window.location.reload(true); }
					});
				}
				return false;
			});

			// unfriending
			$('#' + context.wrapperId + ' a.delete-friend').live('click', function(){
				if(confirm(context.friendshipDeleteConfirmation)) {
					$.telligent.evolution.del({
						url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/users/{RequestorId}/friends/{RequesteeId}.json',
						data: {
							RequestorId: context.userId,
							RequesteeId: getFriendId($(this))
						},
						success: function(){ window.location.reload(true); }
					});
				}
				return false;
			});

			// friending
			$('#' + context.wrapperId + ' a.request-friendship').live('click', function(){
				var url = $(this).attr('href');
				Telligent_Modal.Open(url,550,360,null);
				return false;
			});

			// approving friend request
			$('#' + context.wrapperId + ' a.approve-friend-request').live('click', function(){
				$.telligent.evolution.put({
					url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/users/{RequestorId}/friends/{RequesteeId}.json',
					data: {
						RequestorId: getFriendId($(this)),
						RequesteeId: context.userId,
						FriendshipState: 'Approved'
					},
					success: function(){ window.location.reload(true); }
				});
				return false;
			});

			// rejecting friend request
			$('#' + context.wrapperId + ' a.deny-friend-request').live('click', function(){
				$.telligent.evolution.del({
					url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/users/{RequestorId}/friends/{RequesteeId}.json',
					data: {
						RequestorId: getFriendId($(this)),
						RequesteeId: context.userId
					},
					success: function(){ window.location.reload(true); }
				});
				return false;
			});

			// follow toggling
			$('#' + context.wrapperId + ' a.follow-user').each(function(){
				var link = $(this);
				link.evolutionToggleLink({
					onHtml: '<span></span>' + context.followOnTitle,
					offHtml: '<span></span>' + context.followOffTitle,
					onTitle: context.followOnTitle,
					offTitle: context.followOffTitle,
					processingHtml: '<span></span>...',
					changeState: function(val) {
						if(val) {
							// follow
							$.telligent.evolution.post({
								url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/users/{FollowerId}/following.json',
								data: {
									FollowerId: context.userId,
									FollowingId: getFriendId(link)
								},
								success: function(response) { link.evolutionToggleLink('val', typeof response.Follow !== 'undefined'); },
								error: function(xhr, desc, ex) { link.evolutionToggleLink('val', !val); }
							});
						} else {
							// unfollow
							$.telligent.evolution.del({
								url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/users/{FollowerId}/following/{FollowingId}.json',
								data: {
									FollowerId: context.userId,
									FollowingId: getFriendId(link)
								},
								success: function(response) { link.evolutionToggleLink('val', response.Errors.length !== 0); },
								error: function(xhr, desc, ex) { link.evolutionToggleLink('val', !val); }
							});
						}
					},
					onCssClass: 'internal-link favorite-on',
					offCssClass: 'internal-link favorite-off',
					processingCssClass: 'internal-link processing',
					val: (link.attr('data-follows') === 'true')
				});

			});
		}
	};



	if (typeof $.telligent === 'undefined') { $.telligent = {}; }
	if (typeof $.telligent.evolution === 'undefined') { $.telligent.evolution = {}; }
	if (typeof $.telligent.evolution.widgets === 'undefined') { $.telligent.evolution.widgets = {}; }
	$.telligent.evolution.widgets.friendshipList = api;

}(jQuery));
