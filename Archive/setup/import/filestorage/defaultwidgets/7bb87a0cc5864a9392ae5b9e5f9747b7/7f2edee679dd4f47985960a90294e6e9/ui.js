(function($) {
    var _attachHandlers = function(context) {
            var loadMore = $(context.wrapper + ' .load-more a');
            if (context.endlessScroll) {
                context.hasMore = loadMore.length > 0;
                loadMore.parents('li').hide();
            } else {
                loadMore.click(function() {
                    _loadNextPage(context, context.pageIndex + 1);
                    return false;
                });
            }
        },
        _isLoadingMore = false,
        _loadNextPage = function(context, pageIndex) {
            _isLoadingMore = true;
            return $.telligent.evolution.get({
                url: context.loadNextPageUrl,
                data: {
            		GroupId: context.groupId,
                    PageIndex: pageIndex
            	},
            	success: function(response) {
                    $(context.wrapper + ' .load-more a').parents('li').show().replaceWith(response);
                    context.pageIndex = pageIndex;
                    _attachHandlers(context);
                    _isLoadingMore = false;
            	}
            });
        };
	var api = {
		register: function(context) {
            context.pageIndex = 0;
			_attachHandlers(context);

            if (context.endlessScroll) {
                $(document).bind('scrollend', function() {
                    if (context.hasMore && !_isLoadingMore) {
                        _loadNextPage(context, context.pageIndex + 1);
                    }
                });

                // if the window isn't yet scrollable, try loading more
                var extraLoadCount = 0,
                    maxExtraLoads = 10,
                    loadUntilPageFull = function() {
                        if(!($(document).height() > $(window).height()) && (extraLoadCount < maxExtraLoads)) {
                            extraLoadCount++;
                            _loadNextPage(context, context.pageIndex + 1).then(loadUntilPageFull);
                        }
                    };
                loadUntilPageFull();
            }
		}
	};

	if (typeof $.telligent === 'undefined') { $.telligent = {}; }
	if (typeof $.telligent.evolution === 'undefined') { $.telligent.evolution = {}; }
	if (typeof $.telligent.evolution.widgets === 'undefined') { $.telligent.evolution.widgets = {}; }
	$.telligent.evolution.widgets.groupMentionList = api;

}(jQuery));