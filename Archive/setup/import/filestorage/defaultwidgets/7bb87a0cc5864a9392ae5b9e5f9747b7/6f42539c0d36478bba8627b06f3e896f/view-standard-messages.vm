#set ($dateTimeFormat = "yyyy-MM-ddTHH:mm:ss")
#set ($pageSize = 10)
$core_v2_conversation.MarkAsRead($conversationId)

## paging of conversation messages is reversed
## (requesting 0 returns the last possible page, 1 the second to last possible page)

## first determine the index of the last page of messages
#set ($totalCount = 0)
#set ($totalCount = $core_v2_conversationMessage.List($conversationId, "%{PageIndex = 0, PageSize = 1}").TotalCount)
#set ($remainder = $totalCount % $pageSize)
#if ($remainder > 0)
    #set ($lastPageIndex = ($totalCount / $pageSize))
#else
    #set ($lastPageIndex = ($totalCount / $pageSize) - 1)
#end

## then create a usable pageindex based on distance from the last page
#set ($pageIndex = $lastPageIndex - $pageIndex)
#if ($pageIndex < 0)
    #set ($pageIndex = 0)
#end

## get messages
#set ($messages = $core_v2_conversationMessage.List($conversationId, "%{ PageIndex = $pageIndex, PageSize = $pageSize }"))

## there are more pages yet to be paged if the actual page index was still > 0
#set ($hasMore = false)
#if ($pageIndex > 0)
    #set ($hasMore = true)
#end
#set ($hasMore = $pageIndex > 0)

#set ($lastDate = false)
#set ($startedFlag = false)
#if ($pageIndex > 0)
    #set ($startedFlag = true)
#end

#foreach ($message in $messages)
    #set ($formattedDate = $core_v2_language.FormatDate($message.CreatedDate))
    #if ($lastDate != $formattedDate)
        #set ($lastDate = $core_v2_language.FormatDate($message.CreatedDate))
        #if (!$startedFlag)
            #set ($startedFlag = true)
            <li class="content-item day start">$core_v2_language.GetResource('ConversationStarted') $lastDate</li>
        #else
            <li class="content-item day">$lastDate</li>
        #end
    #end

    #set ($id = $core_v2_widget.UniqueId("conversation-${message.ConversationId}-message-${message.Id}"))
    <li class="content-item conversation-message"
        id="$core_v2_encoding.HtmlAttributeEncode($id)"
        data-conversationid="$!message.ConversationId"
        data-messageid="$!message.Id"
        data-createddate="$!core_v2_language.FormatDate($message.CreatedDate, $dateTimeFormat)"
        data-subject="$core_v2_encoding.HtmlAttributeEncode($core_v2_language.Truncate($message.Subject, 50, "..."))"
        data-fullsubject="$core_v2_encoding.HtmlAttributeEncode($message.Subject)"
        #if($hasMore) data-hasmore="true" #end>

        <div class="full-post-header"></div>
        <div class="full-post">
            <div class="post-author">
                <span class="avatar">
                    #if ($message.Author.ProfileUrl)
                        <a href="$core_v2_encoding.HtmlAttributeEncode($message.Author.ProfileUrl)">
                            $core_v2_ui.GetResizedImageHtml($message.Author.AvatarUrl, 32, 32, "%{border='0', alt=$message.Author.DisplayName}")
                        </a>
                    #else
                        $core_v2_ui.GetResizedImageHtml($message.Author.AvatarUrl, 32, 32, "%{border='0', alt=$message.Author.DisplayName}")
                    #end
                </span>
                <span class="user-name">
                    #if ($message.Author.ProfileUrl)
                        <a href="$core_v2_encoding.HtmlAttributeEncode($message.Author.ProfileUrl)" class="internal-link view-user-profile">
                            <span></span>$message.Author.DisplayName
                        </a>
                    #else
                        <span></span>$message.Author.DisplayName
                    #end
                </span>
            </div>

            <div class="post-date" title="$core_v2_encoding.HtmlAttributeEncode($core_v2_language.FormatDateAndTime($message.CreatedDate))">
                <span class="value">$core_v2_language.FormatDate($message.CreatedDate, 't')</span>
            </div>

            <div class="post-content user-defined-markup">$message.RenderedBody</div>
        </div>
        <div class="full-post-footer"></div>
    </li>
#end
