﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TelligentEvolution.Mobile.Web.Controllers;
using TelligentEvolution.Mobile.Web.Data;
using Omnicell.Data.Model;
using System.Text;
using System.Net.Mail;
using Telligent.Evolution.Extensibility.Api.Version1;
using TelligentEvolution.Mobile.Web.Models;
using TelligentEvolution.Mobile.Web.Data.Entities;
using TelligentEvolution.Mobile.Web.Data.Implementations;
using Telligent.Evolution.Extensibility.Api.Entities.Version1;

//[Authorize(Roles="Employee,Administrators,Moderators")]
public class TicketController : BaseController
{
    private readonly IUserContext _userContext;

    public TicketController(IUserContext userContext)
    {
        _userContext = userContext;
    }

    public TicketController() : this(Services.Get<IUserContext>())
    {
    }

    // GET api/<controller>
    public ActionResult SearchByCsn(SearchTicketModel viewModel)
    {
        if (viewModel != null && !string.IsNullOrWhiteSpace(viewModel.txtOmniSearch))
        {
            return SearchByCsn_Post(viewModel);
        }
        else
        {
            var db = new OmnicellEntities();
            var newViewModel = new SearchTicketModel(_userContext)
            {
                isCsnSearch = true
            };
            return View(newViewModel);
        }
    }

    [HttpPost, ActionName("SearchByCsn")]
    public ActionResult SearchByCsn_Post(SearchTicketModel viewModel)
    {
		// SMR - Added to get the CSNs for the current user
		var ur = new UserRepository();
		var user = ur.GetUser(viewModel.UserContext.UserName);
		var csn = user.ProfileFields.FirstOrDefault(x => x.Info.Name.Contains("CSN")).Value.Trim();
		var allCsns = user.ProfileFields.FirstOrDefault(x => x.Info.Name.Contains("RelatedCSNs")).Value.Trim().Split(',').ToList<string>();
		allCsns.Add(csn);
		
		var chkCsns = new List<string>();
		foreach(string item in allCsns){
			if(!String.IsNullOrEmpty(item) && item.ToLower() != "employee")
				chkCsns.Add(item);
		}		
		
		//chkCsns.Add("22281");
		// SMR - 
		// PublicApi.Users	tcuser= new PublicApi.Users("customer-1");
        var db = new OmnicellEntities();
		
        if (!viewModel.activeOnly)
        {
            var tickets = db.Tickets.Where(x => x.CSN.Contains(viewModel.txtOmniSearch)).Take(20);
			if(chkCsns.Count > 0)
				tickets = db.Tickets.Where(x => chkCsns.Contains(x.CSN) ).Take(20);
				
            ModelState.Remove("Tickets");
            viewModel.Tickets = tickets.ToList();
        }
        else
        {
            var tickets = db.Tickets.Where(x => x.IsActive && !x.Status.ToLower().Contains("closed") && x.CSN.Contains(viewModel.txtOmniSearch)).Take(20);
			if(chkCsns.Count > 0)
				tickets = db.Tickets.Where(x => x.IsActive && !x.Status.ToLower().Contains("closed") && chkCsns.Contains(x.CSN) ).Take(20);
            ModelState.Remove("Tickets");
            viewModel.Tickets = tickets.ToList();
        }
        //var viewModel = new SearchTicketModel(_userContext);
        return View("SearchByCsn", viewModel);
    }

    // GET api/<controller>
    public ActionResult SearchBySrn(SearchTicketModel viewModel)
    {
        if (viewModel != null && !string.IsNullOrWhiteSpace(viewModel.txtOmniSearch))
        {
            return SearchBySrn_Post(viewModel);
        }
        else
        {
            var db = new OmnicellEntities();
            var newViewModel = new SearchTicketModel(_userContext)
            {
                isCsnSearch = false
            };
            return View(newViewModel);
        }
    }

    [HttpPost, ActionName("SearchBySrn")]
    public ActionResult SearchBySrn_Post(SearchTicketModel viewModel)
    {
        var db = new OmnicellEntities();
        if (!viewModel.activeOnly)
        {
            var tickets = db.Tickets.Where(x => x.Number.Contains(viewModel.txtOmniSearch)).Take(20);
            ModelState.Remove("Tickets");
            viewModel.Tickets = tickets.ToList();
        }
        else
        {
            var tickets = db.Tickets.Where(x => x.IsActive && !x.Status.ToLower().Contains("closed") &&  x.Number.Contains(viewModel.txtOmniSearch)).Take(20);
            ModelState.Remove("Tickets");
            viewModel.Tickets = tickets.ToList();
        }
        //var viewModel = new SearchTicketModel(_userContext);
        return View("SearchBySrn", viewModel);
    }

    [HttpPost]
    public ActionResult UpdateTicketStatuses(TicketUpdatedModel ticketModel)
    {
        Ticket ticket = null;
        var sb = new StringBuilder();
        sb.AppendLine("<h2>myOmnicell Mobile site ticket status change.</h2>");
        sb.Append("<p><span style=\"text-decoration:underline\">Number :</span> ");
        using (var db = new OmnicellEntities())
        {
            ticket = db.Tickets.Single(x => x.Id == ticketModel.Ticket.Id);

            sb.Append(ticket.Number);
            sb.AppendLine("</p>");

            sb.Append("<p><span style=\"text-decoration:underline\">Status x :</span> ");
            if (ticket.Status != null && ticket.Status.Equals(ticketModel.Ticket.Status) || ticket.Status == null)
            {
                sb.Append(ticket.Status);
            }
            else
            {
                sb.Append("<span style=\"font-weight:bold\">");
                sb.Append(ticketModel.Ticket.Status);
                sb.Append("</span> (<span style=\"text-decoration:underline\">Old Value :</span> ");
                sb.Append(ticket.Status);
                sb.AppendLine(")");
            }

            sb.AppendLine("</p>");

            sb.Append("<p><span style=\"text-decoration:underline\">SubStatus :</span> ");
            if (ticket.SubStatus != null && ticket.SubStatus.Equals(ticketModel.Ticket.SubStatus) || ticket.SubStatus == null)
            {
                sb.Append(ticket.SubStatus);
            }
            else
            {
                sb.Append("<span style=\"font-weight:bold\">");
                sb.Append(ticketModel.Ticket.SubStatus);
                sb.Append("</span> (<span style=\"text-decoration:underline\">Old Value :</span> ");
                sb.Append(ticket.SubStatus);
                sb.AppendLine(")");
            }

            sb.AppendLine("</p>");


            sb.Append("<p><span style=\"text-decoration:underline\">Detail :</span> ");
            sb.Append(ticket.Abstract);
            sb.AppendLine("</p>");

            ticket.Status = ticketModel.Ticket.Status;
            ticket.SubStatus = ticketModel.Ticket.SubStatus;
            ticket.LastActivityUpdateDate = DateTime.Now;
            db.SaveChanges();
        }

        var emailAddy = System.Configuration.ConfigurationManager.AppSettings["OmnicellUpdateEmail"].ToString();
        String SMTPRelay = System.Configuration.ConfigurationManager.AppSettings["OmnicellEmailRelay"].ToString();

        var services = new Omnicell.Services.Utilities();
        services.SendEmail(emailAddy, "Omnicell Support", emailAddy, "Omnicell Support", "myOmnicell Mobile site ticket update", sb.ToString(), SMTPRelay.ToString(),"",true);

        ModelState.Remove("Ticket");
        ticketModel.Ticket = ticket;

        ModelState.Remove("IsPost");
        ticketModel.IsPost = true;
        
        return View("Details", ticketModel);
    }

    public ActionResult Details(int Id, string searchText, bool isActiveOnly, bool isCsnSearch)
    {
        var db = new OmnicellEntities();
        var ticket = db.Tickets.SingleOrDefault(x => x.Id == Id);
        var viewModel = new TicketUpdatedModel
        {
            Ticket = ticket,
            Statuses = Statuses,
            SubStatuses = SubStatuses,
            SearchText = searchText,
            IsCsnSearch = isCsnSearch,
            IsActiveOnly = isActiveOnly
        };
        return View(viewModel);
    }

    private List<string> statuses = null;
    public List<string> Statuses {
        get
        {
            if (statuses == null)
            {
                statuses = this.HttpContext.Application["TicketStatuses"] as List<string>;
                if (statuses == null)
                {
                    var db = new OmnicellEntities();
                    this.HttpContext.Application["TicketStatuses"] = statuses = db.Tickets.Where(x => x.Status != null && x.Status.Trim().Length > 0).Select(x => x.Status.Trim()).Distinct().ToList();
                }
            }

            return statuses;
        }
    }

    private List<string> subStatuses = null;
    public List<string> SubStatuses
    {
        get
        {
            if (subStatuses == null)
            {
                subStatuses = this.HttpContext.Application["TicketSubStatuses"] as List<string>;
                if (subStatuses == null)
                {
                    var db = new OmnicellEntities();
                    this.HttpContext.Application["TicketSubStatuses"] = subStatuses = db.Tickets.Where(x => x.SubStatus != null && x.SubStatus.Trim().Length > 0).Select(x => x.SubStatus.Trim()).Distinct().ToList();
                }
            }

            return subStatuses;
        }
    }
}
