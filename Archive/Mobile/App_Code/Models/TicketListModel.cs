﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TelligentEvolution.Mobile.Web.Models;
using TelligentEvolution.Mobile.Web.Data;
using Omnicell.Data.Model;

/// <summary>
/// Summary description for SearchByCsnModel
/// </summary>
public class TicketListModel :BasePageViewModel
{
    public List<Ticket> Tickets { get; set; }

    public List<string> Statuses { get; set; }
    public List<string> SubStatuses { get; set; }

    public TicketListModel(IUserContext userContext)
        : base(userContext)
	{
        Statuses = new List<string>();
        Statuses.Add("New");
        Statuses.Add("Pending");
        Statuses.Add("Closed");

        SubStatuses = new List<string>();
        SubStatuses.Add("Replace");
        SubStatuses.Add("Repair");
        SubStatuses.Add("Closed");
    }

    public TicketListModel()
        : this(Services.Get<IUserContext>())
    {
    }
}