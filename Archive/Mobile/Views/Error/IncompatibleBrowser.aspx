﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title><%= Html.LocalizedString("Error_IncompatibleBrowser_Title")%> - <%= Html.SiteName() %></title>
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;"/>
    <meta name="apple-mobile-web-app-capable" content="no" /> 
    <style type="text/css">
        body { background-color: #fff; font-size: 14px; font-family: Helvetica, Arial; margin: 0; padding: 0; color: #000; -webkit-text-size-adjust: none; }

        a:link { color: #0055AA; text-decoration: none; }
        a:visited { color: #0055AA; text-decoration: none; }
        a:hover { color: #0055AA; text-decoration: none; }
        a:active { color: #0055AA; text-decoration: none; outline: none; }
        
        .error-container { padding: 10px 4px; color: #333; text-align: center; }
            .error-container .error-header { margin-bottom: 0px; font-weight: bold; font-size: 24px; }
            .error-container .error-message { display: block; padding: 5px; }
            .error-container .internal-link { display: block; margin-top: 20px; margin-bottom: 20px; }
    </style>
</head>
<body>
    <div>
        <div class="content">
            <div class="error-container">
                <h2 class="error-header"><%= Html.LocalizedString("Error_Oops") %></h2>
                <div class="error-message">
                    <%= Html.LocalizedString("Error_IncompatibleBrowser_Content")%>
                </div>
                <%= Html.FullSiteLink(Html.LocalizedString("Core_Master_FullSite"))%>
            </div>
        </div>
    </div>
</body>

