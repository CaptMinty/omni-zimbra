﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<TicketUpdatedModel>"  %>

<div class="UpdateResult" style="color:Red">
    Ticket <%= Model.Ticket.Number %> updated successfully!
</div>

