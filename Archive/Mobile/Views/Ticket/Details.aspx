﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<TicketUpdatedModel>" MasterPageFile="~/Views/Shared/Site.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
			<div class="landing-page updateStatus">
				
				<div id="AllList">
					
					<div class="list-view" style="">
						<ul class="list-view-container item-list groups navigation-list">

                            <% if (Model.IsPost)
                                { %>
   							        <li class="list-item" style="text-align:left;">
								        SRN: <%= Html.DisplayFor(model => model.Ticket.Number) %> <br />
								        Status: <%= Html.DisplayFor(model => model.Ticket.Status) %><br />
								        Substatus: <%= Html.DisplayFor(model => model.Ticket.SubStatus) %> <br />
								        <div class="issue">
									        <%= Html.DisplayFor(model => model.Ticket.Abstract) %> <br />
								        </div>
							        </li>
                            <% }
                                else
                                { %>

							    <li class="list-item" style="text-align:left;">
								    <div id="srn">SRN: <%= Html.DisplayFor(model => model.Ticket.Number) %> <br /></div>
								    <div id="currentStatus">
									    Status: <%= Html.DisplayFor(model => model.Ticket.Status) %> <br />
								    </div>
								    <div id="currentSubstatus">
									    Substatus: <%= Html.DisplayFor(model => model.Ticket.SubStatus) %> <br />
								    </div>
                                <% using (Html.BeginForm("UpdateTicketStatuses", "Ticket"))
                                   {
                                       var StatusList = new SelectList(Model.Statuses, Model.Ticket.Status);
                                       var SubStatusList = new SelectList(Model.SubStatuses, Model.Ticket.SubStatus); %>
                                        <%= Html.HiddenFor(model => model.Ticket.Id)%>
                                        <%= Html.HiddenFor(model => model.SearchText)%>
                                        <%= Html.HiddenFor(model => model.IsActiveOnly)%>
                                        <%= Html.HiddenFor(model => model.IsCsnSearch)%>

					                    Status: <%= Html.DropDownList("Ticket.Status", StatusList, string.Empty, new { @class = "StatusDropDown" })%> <br />
					                    Substatus: <%= Html.DropDownList("Ticket.SubStatus", SubStatusList, string.Empty, new { @class = "SubStatusDropDown" })%> 

									    <div class="issue" id="issue"><%: Model.Ticket.Abstract%></div>
									    <div id="date" class="date"><%: Model.Ticket.LastActivityUpdateDate.Value.ToShortDateString()%></div><div id="time" class="time"><%: Model.Ticket.LastActivityUpdateDate.Value.ToShortTimeString()%></div><br />
									    <input type="submit" value="Save" style="display: none;" />
                                <% }%>

							    </li>
                                <% }
                                    if (Model.IsPost)
                                    {
                                        if (Model.IsCsnSearch)
                                        {%>

                                            <li class="navigation-item activity-link">
                                                <a class="internal-link view-activity" href="<%= Url.Action("SearchByCsn", "Ticket", new SearchTicketModel{ txtOmniSearch = Model.SearchText, activeOnly = Model.IsActiveOnly, isCsnSearch = true}) %>"><span></span>Update a new record</a>
                                            </li>
                                        <% }
                                        else
                                        { %>
                                            <li class="navigation-item activity-link">
                                                <a class="internal-link view-activity" href="<%= Url.Action("SearchBySrn", "Ticket", new SearchTicketModel{ txtOmniSearch = Model.SearchText, activeOnly = Model.IsActiveOnly, isCsnSearch = false}) %>"><span></span>Update a new record</a>
                                            </li>

                                <% } %>
                                            <li class="navigation-item activity-link">
                                                <a class="internal-link view-activity" href="<%= Url.Action("Index", "Home") %>"><span></span>myOmnicell Mobile</a>
                                            </li>

                                <%    }%>

						</ul>
					</div>
					<div class="error-container" style="display:none">
						<div class="error-message">Use the full site to become a member of groups.</div>
					</div>
				</div>
			</div>
    <span style="clear:both"></span>
</asp:Content>