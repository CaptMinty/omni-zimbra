﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<TelligentEvolution.Mobile.Web.Models.CreateMessageViewModel>" %>

<% if (Model.ShowForm) {%>
<script type="text/javascript">
// <![CDATA[
    function showCreateMessageContainer(link) { $('#createMessageContainer').createMessageContainer(link); };
    $(function () { $("textarea").maxlength(<%= Html.SiteSettings().MaxMessageLength %>); })
// ]]>
</script>

<div id="createMessageContainer" class="post-form-area" style="display: none;">
    <% using (Html.BeginForm(new { area = "Activity", controller = "Status", action = "CreateMessage"})) { %>
        <%= Html.Hidden("useRedirectUrl", true) %>
	    <div class="field-list-header"><span></span></div>
	    <fieldset class="field-list">
		    <ul class="field-list">
			    <li class="field-item post-content">
				    <label for="Message" class="field-item-header"></label>
				    <span class="field-item-input"><%= Html.TextAreaFor(m => m.Message, new { cols = "30", rows = "4" })%></span>
                    <% Html.ValidateFor(m => m.Message); %>
				    <%= Html.AntiForgeryToken()%>
			    </li>
			    <li class="field-item submit">
				    <span class="field-item-input">
					    <a href="#" class="internal-link cancel-post"><span></span></a>
					    <a href="#" class="internal-link add-post"><span></span></a>
				    </span>
			    </li>
		    </ul>
	    </fieldset>
	    <div class="field-list-footer"><span></span></div>
    <% } %>
</div>
<% } %>