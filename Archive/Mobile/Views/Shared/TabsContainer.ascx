﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<TelligentEvolution.Mobile.Web.Models.PageWithSubNavigationViewModel>" %>
<% 
    string currentTabName = ViewContext.HttpContext.Request.Params[Model.TabParameterName];
    TelligentEvolution.Mobile.Web.Data.Entities.NavigationMenuItem currentTab = null;
%>
<script type="text/javascript">
// <![CDATA[

    $(document).ready(function ()
    {
        $('.tab-container').tabsContainer({ 
                loadingHtml : "<div class='tab-content-loading'>&nbsp;</div>",
                errorHtml: "<div class='error-container'><div class='error-message'><%=Html.Encode(Html.LocalizedString("Core_Tabs_Loading_Error"))%></div></div>" });
    });
// ]]>
</script>
<div class="tab-container">
    <div class="tab-menu" <%if (Model.Items.Count() <= 1) {%> style="display:none;" <%} %>>
        <span>
            <%
                int i=0;
                var sortedList = Model.Items.OrderBy(m => m.OrderNumber).ToList();
                currentTab = sortedList.Find(item => !item.Disabled && item.Name.Equals(currentTabName, StringComparison.InvariantCultureIgnoreCase)) ?? sortedList.Find(item => !item.Disabled);
                foreach (var item in sortedList)
                {
                    i++;
            %>
                    <%= Html.TabButton(Model.TabParameterName, item, i == 1, i == Model.Items.Count(), item == currentTab, item.Disabled)%>
            <%
                } %>
        </span>
    </div>
    <div class="tab-content">
        <% if (currentTab != null) { %>
            <% Html.RenderAction(currentTab.ActionName, currentTab.ControllerName, new RouteValueDictionary(currentTab.RouteValue) { { "area", currentTab.AreaName } }); %>
        <% } %>
    </div>
</div>