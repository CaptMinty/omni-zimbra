﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<SearchTicketModel>" %>

<% if (Model != null && Model.Tickets != null) {  %>
	<div id="AllList">
					
		<div class="list-view" style="">
                <img alt="LoadingImage" src="<%= Url.Content("~/Content/Images/spinner-333.gif") %>" style="position:absolute; display:none" id="LoadingImage" />
                <span id="ResultsPane"></span>
			<ul class="list-view-container item-list groups">

                <% if (Model.Tickets.Count == 0) { %>
				<li id="itemno" class="list-item" style="text-align:left;">
					No Ticket Found.
					<div class="issue">
						&nbsp;
					</div>
					<div class="group-name">
						&nbsp;
					</div>
				</li>
				    <% 
                    } 
                    else 
                    {
                        var count = 0;
                        foreach (var ticket in Model.Tickets)
                        {
                           %>
				            <li id="item<%= ++count %>" class="list-item navigation-item" style="text-align:left;">
                                <a class="internal-link view-group" style="color:Black;" href="<%= Url.Action("Details","Ticket", new { Id = ticket.Id, searchText = Model.txtOmniSearch, isActiveOnly = Model.activeOnly, isCsnSearch = Model.isCsnSearch }) %>">
                                    <%= Html.HiddenFor(x => ticket.Id) %>
					                SRN: <%: ticket.Number %> <br />
					                Status: <%: ticket.Status %> <br />
					                Substatus: <%: ticket.SubStatus %> <br />
                                    <p class="issue">
						                <%: ticket.Abstract %> <br />
					                </p>
						            <span></span>
                                </a>
				            </li>
                        <% }
                   } %>
    			</ul>
						
				<div class="view-more-area" style="display: none;">
					<a class="internal-link view-more" href="#"><span></span>View More</a>
					<div class="view-more-loading"></div>
					<div class="view-more-complete">No more items to display.</div>
				</div>
						
		</div>
		<div class="error-container" style="display:none">
			<div class="error-message">Use the full site to become a member of groups.</div>
		</div>
	</div>

<%} %>