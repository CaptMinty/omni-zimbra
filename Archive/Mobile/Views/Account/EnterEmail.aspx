﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Simple.Master" Inherits="System.Web.Mvc.ViewPage<TelligentEvolution.Mobile.Web.Models.EnterEmailViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    <%= Html.LocalizedString("Core_EnterEmail_Title")%>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<% using (Html.BeginForm(new { area = "", controller = "Account", action = "EnterEmail", id = Model.Id, ReturnUrl = Model.ReturnUrl })) { %>
    <div class="field-list-header enter-email"></div>
    <fieldset class="field-list enter-email"><legend><%= Html.LocalizedString("Core_EnterEmail_Title")%></legend>
        <% if (ViewData.ModelState.ContainsKey("errorResourceName")){ %>
            <span class="field-validation-summary"><%= Html.LocalizedString(ViewData.ModelState["errorResourceName"].Errors[0].ErrorMessage)%></span>
        <% } %>
        <ul class="field-list">
            <li class="field-item email">
                <%= Html.ValidationMessageFor(m => m.Email) %>
                <span class="field-item-input"><%= Html.TextBoxFor(m => m.Email)%></span>
            </li>
            
            <li class="field-item submit">
                <span class="field-item-input">
                    <a href="#" onclick="$(this).parents('form').submit(); return false;" class="internal-link submit"><span></span><%= Html.LocalizedString("Core_EnterEmail_Submit")%></a>
                </span>
            </li>
        </ul>
    </fieldset>
    <div class="field-list-footer enter-email"></div>
<% } %>
</asp:Content>

