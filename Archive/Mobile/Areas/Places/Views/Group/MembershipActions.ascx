﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<TelligentEvolution.Mobile.Web.Data.Entities.GroupMembershipRequest>" %>
<%@ Import Namespace="TelligentEvolution.Mobile.Web.Data.Entities" %>

<div class="request-actions-area">
<% if (Model.MembershipType == MembershipType.None) { %>
    <% if (Model.UserCanJoinGroup) { %>
        <%= Html.CreateMembershipRequestActionLink(Model, Html.LocalizedString("Group_Membership_Create"), "internal-link create-membership")%>    
    <% } else if (Model.UserCanJoinGroupByRequest) { %>
        <%= Html.CreateMembershipRequestActionLink(Model, Html.LocalizedString("Group_Membership_Pending_Request_Create"), "internal-link create-pending-membership")%>    
    <% } %>
<% } else if (Model.MembershipType == MembershipType.PendingMember) { %>
    <%= Html.CancelMembershipRequestActionLink(Model, Html.LocalizedString("Group_Membership_Pending_Request_Cancel"), "internal-link cancel-pending-membership")%>
<% } else { %>
    <%= Html.CancelMembershipRequestActionLink(Model, Html.LocalizedString("Group_Membership_Cancel"), "internal-link cancel-membership")%>
<% } %>
</div>
