﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<TelligentEvolution.Mobile.Web.Data.Entities.Application>" %>


<li class="list-item">
    <div class="avatar"><%= Html.ApplicationInfoLink(Model, true)%></div>
    <div class="application-name">
        <%= Html.ApplicationInfoLink(Model, false)%>
    </div>
</li>