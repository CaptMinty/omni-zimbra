﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<TelligentEvolution.Mobile.Web.Data.Entities.Group>" %>

<li class="list-item">
    <div class="avatar"><%= Html.GroupInfoLink(Model, true)%></div>
    <div class="group-name">
        <%= Html.GroupInfoLink(Model, false)%>
    </div>
</li>