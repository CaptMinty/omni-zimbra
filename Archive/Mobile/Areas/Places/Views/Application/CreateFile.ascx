﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<TelligentEvolution.Mobile.Web.Models.CreateFileViewModel>" %>
<%@ Import Namespace="TelligentEvolution.Mobile.Web.Data.Entities" %>

<% bool SupportUploading = Html.UserAgent().SupportUpploading(); %>
<script type="text/javascript">
// <![CDATA[
    function showCreateFileContainer(link) { $('#createFileContainer').createMessageContainer(link, "#Name"); };
    <% if (SupportUploading) { %>
    $(function () 
    { 
        $("textarea").maxlength(<%= Html.SiteSettings().MaxMessageLength %>); 
        $(":radio").change(changedFileType);
        changedFileType();
    })
    function changedFileType()
    {
        var isFile = $(":radio:checked").val() == "file";
        $("input[name=FileUrl]").attr("disabled", isFile).val("");
        $("input[name=File]").attr("disabled", !isFile);
    }
    <% } %>
// ]]>
</script>

<div id="createFileContainer" class="post-form-area new-app-post" style="display: none;">
    <% using (Html.BeginForm("CreateFile", "Application", FormMethod.Post, new { area = "Places", enctype = "multipart/form-data" }))
       { %>
        <div class="field-list-header"><span></span></div>
        <fieldset class="field-list">
	        <ul class="field-list">
                <li class="field-item post-content">
                    <label class="field-item-header"><%= Html.LocalizedString("ApplicationContent_CreateFile_CreateFile")%></label>
                </li>
                <% if (SupportUploading) { %>
                <li class="field-item post-content">
                    <%= Html.RadioButtonFor(m => m.FileType, "file")%>
			        <label for="File" class="field-item-header-inline"><%= Html.LocalizedString("ApplicationContent_CreateFile_FileHeader")%></label>
                    <span class="field-item-input"><%= Html.FileBoxFor(m => m.File, new { maxlength = Html.Config().MaxSubjectLength })%></span>
                    <% Html.ValidateFor(m => m.File); %>
		        </li>
                <li class="field-item post-content">
                    <%= Html.RadioButtonFor(m => m.FileType, "url")%>
			        <label for="FileUrl" class="field-item-header-inline"><%= Html.LocalizedString("ApplicationContent_CreateFile_FileUrlHeader")%></label>
			        <span class="field-item-input"><%= Html.TextBoxFor(m => m.FileUrl, new { maxlength = Html.Config().MaxSubjectLength })%></span>
                    <%= Html.ValidationMessageFor(m => m.FileUrl)%>
		        </li>
                <li class="field-item post-content">
			        <label for="Name" class="field-item-header"><%= Html.LocalizedString("ApplicationContent_CreateFile_NameHeader")%></label>
			        <span class="field-item-input"><%= Html.TextBoxFor(m => m.Name, new { maxlength = Html.Config().MaxSubjectLength })%></span>
                    <% Html.ValidateFor(m => m.Name); %>
		        </li>
		        <li class="field-item post-content">
			        <label for="Description" class="field-item-header"><%= Html.LocalizedString("ApplicationContent_CreateFile_DescriptionHeader")%></label>
			        <span class="field-item-input"><%= Html.TextAreaFor(m => m.Description, new { cols = 30, rows = 4 })%></span>
			        <%= Html.AntiForgeryToken()%>
		        </li>
                <li class="field-item post-content">
			        <label for="Tags" class="field-item-header"><%= Html.LocalizedString("ApplicationContent_CreateFile_TagsHeader")%></label>
			        <span class="field-item-input"><%= Html.TextBoxFor(m => m.Tags, new { maxlength = Html.Config().MaxSubjectLength })%></span>
		        </li>
                <% } else { %>
                <li class="field-item post-content not-supported">
			        <label class="field-item-header"><%= Html.LocalizedString("ApplicationContent_CreateFile_NotSupported", Html.FullSiteUrl(Model.FullSiteUrl))%></label>
		        </li>    
                <% } %>
		        <li class="field-item submit">
			        <span class="field-item-input">
				        <a href="#" class="internal-link cancel-post"><span></span></a>
                        <% if (SupportUploading) { %>
				        <a href="#" class="internal-link add-post"><span></span></a>
                        <% } %>
			        </span>
		        </li>
	        </ul>
        </fieldset>
        <div class="field-list-footer comment"><span></span></div>
    <% } %>
</div>