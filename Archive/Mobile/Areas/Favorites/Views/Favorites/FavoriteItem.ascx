﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<TelligentEvolution.Mobile.Web.Data.Entities.Favorite>" %>

<li class="list-item">
    <div class="avatar"><%= Html.FavoriteInfoLink(Model, true)%></div>
    <div class="favorite-name">
        <%= Html.FavoriteInfoLink(Model, false)%>
    </div>
</li>