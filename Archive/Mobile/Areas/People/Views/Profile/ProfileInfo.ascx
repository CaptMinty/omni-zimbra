﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<TelligentEvolution.Mobile.Web.Data.Entities.User>" %>
<%@ Import Namespace="System.Globalization" %>
<% bool dataExist = false;
   List<String> excludedProfileFields = new List<string>() { "Birthday", "Gender", "Language", "Location", "PublicEmail", "Website" };%>
<ul class="user-profile-info">
    <% if (ViewBag.ShowEditLink) { %>
    <li class="user-profile-edit">
        <%=Html.ActionLink(Html.LocalizedString("People_Profile_Info_EditLink"), "Edit", "Profile", new { area = "People" }, new { @class = "internal-link edit-profile" })%>
    </li>
    <% } %>
    <% if (!String.IsNullOrEmpty(Model.Biography)) {
           dataExist = true; %>
        <li class="user-profile-field biography">   
            <span class="user-profile-field-name"><%=Html.LocalizedString("People_Profile_Biography", Model.DisplayName)%></span>
            <div class="user-profile-field-value"><%=Model.Biography%></div>
        </li>
    <% } %>
    <% if (!String.IsNullOrEmpty(Model.Location)) {
           dataExist = true; %>
        <li class="user-profile-field location">   
            <span class="user-profile-field-name"><%=Html.LocalizedString("People_Profile_Location")%>: </span>
            <span class="user-profile-field-value"><%:Model.Location%></span>
        </li>
    <% } %>
    <% if (Model.Birthday != null) {
           dataExist = true; %>
        <li class="user-profile-field birthday">   
            <span class="user-profile-field-name"><%=Html.LocalizedString("People_Profile_Birthday")%>: </span>
            <span class="user-profile-field-value"><%=Model.Birthday.Value.ToString(Model.DateFormat ?? Html.SiteSettings().DateFormat)%></span>
        </li>
    <% } %>
    <% if (!String.IsNullOrEmpty(Model.Gender)) {
           dataExist = true; %>
        <li class="user-profile-field gender">   
            <span class="user-profile-field-name"><%=Html.LocalizedString("People_Profile_Gender")%>: </span>
            <span class="user-profile-field-value"><%:Model.Gender%></span>
        </li>
    <% } %>
    <% if (!String.IsNullOrEmpty(Model.Language)) {
           dataExist = true; %>
        <li class="user-profile-field language">   
            <span class="user-profile-field-name"><%=Html.LocalizedString("People_Profile_Language")%>: </span>
            <span class="user-profile-field-value"><%=new CultureInfo(Model.Language).NativeName%></span>
        </li>
    <% } %>
    <% if (!String.IsNullOrEmpty(Model.PublicEmail)) {
           dataExist = true; %>
        <li class="user-profile-field public-email">   
            <span class="user-profile-field-name"><%=Html.LocalizedString("People_Profile_PublicEmail")%>: </span>
            <span class="user-profile-field-value"><%:Model.PublicEmail%></span>
        </li>
    <% } %>
    <% if (!String.IsNullOrEmpty(Model.WebUrl)) {
           dataExist = true; %>
        <li class="user-profile-field weburl">   
            <span class="user-profile-field-name"><%=Html.LocalizedString("People_Profile_WebUrl")%>: </span>
            <span class="user-profile-field-value"> <%=Html.LinkButton(Model.WebUrl, null, Model.WebUrl, new RouteValueDictionary(new { target="_blank" }), "internal-link ")%></span>
        </li>
    <% } %> 

    <% if (Model.ProfileFields != null && Model.ProfileFields.Count > 0) {
           foreach (var field in Model.ProfileFields) {
               if (field.Info != null && !String.IsNullOrEmpty(field.Info.Name) && !String.IsNullOrEmpty(field.Value))
               {
                   dataExist = true; %>
                    <li class="user-profile-field location">   
                        <span class="user-profile-field-name"><%:field.Info.DisplayName ?? field.Info.Name%>: </span>
                        <span class="user-profile-field-value"><%:field.Value%></span>
                    </li>
            <% } %>
        <% } %>
    <% } %>
    <% if (!dataExist) { %>
        <li class="user-profile-no-data">   
            <%=Html.LocalizedString("People_Profile_NoUserInfo")%>
        </li>
    <% } %>
</ul>