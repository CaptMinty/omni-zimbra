﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Omnicell.Data.Model;
using Telligent.Evolution.Extensibility.Api.Entities.Version1;
using Telligent.Evolution.Extensibility.UI.Version1;
using Telligent.Evolution.Extensibility.Version1;
using Omnicell.Services;
using System.Data.Objects;
using System.Web;

namespace WidgetExtensions.Extensions
{
    public class ServiceTicketCountPlugin : IScriptedContentFragmentExtension
    {
        public object Extension
        {
            get { return new ServiceTicketCount(); }
        }

        public string ExtensionName
        {
            get { return "omnicell_v1_ticketcount"; }
        }

        public string Description
        {
            get { return "Allows the user of ticket subscription data collection"; }
        }

        public void Initialize() {}

        public string Name
        {
            get { return "Omnicell Service Ticket (omnicell_v1_ticketcount)"; }
        }
    }

    public class ServiceTicketCount
    {
        // these are email digest test methods they are meant to be removed once done testing the functionality
        public bool testEmailDigest()
        {
            bool worked = true;

            DateTime today = DateTime.Now;
            DayOfWeek weekday = DateTime.Today.DayOfWeek;
            DateTime firstDay = new DateTime(today.Year, today.Month, 1);
            DateTime lastMonthFirst = firstDay.AddMonths(-1);

            DailyEmails(today);

            if (weekday == DayOfWeek.Friday)
            {
                WeeklyEmails(today.AddDays(-7));
            }

            if (firstDay.Date == today.Date)
            {
                MonthlyEmails(lastMonthFirst);
            }


            return worked;

        }

        private static void DailyEmails(DateTime today)
        {
            Utilities util = new Utilities();

            try
            {

                using (var db = new OmnicellEntities())
                {
                    var users = db.Omnicell_TicketEmailInfo.Where(x => x.IsImmediateUpdate == 0);
                    var tickets = new List<Omnicell_Ticket>();

                    foreach (var item in users)
                    {
                        var subscriptions = db.Omnicell_TicketSubscription.Where(x => x.UserId == item.UserId);

                        foreach (var sub in subscriptions)
                        {
                            if (sub.TicketId != null)
                            {
                                var ticket = db.Omnicell_Ticket.FirstOrDefault(x => x.Id == sub.TicketId);

                                if (ticket.Updated.Value.Date == today.Date)
                                {
                                    tickets.Add(ticket);
                                }
                            }

                        }

                        if (tickets.Count != 0)
                        {
                            BuildEmail(tickets, item.Email, item.UserId);
                        }

                        tickets.Clear();

                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private static void WeeklyEmails(DateTime today)
        {
            Utilities util = new Utilities();

            try
            {

                using (var db = new OmnicellEntities())
                {
                    var users = db.Omnicell_TicketEmailInfo.Where(x => x.IsImmediateUpdate == 2);
                    var tickets = new List<Omnicell_Ticket>();

                    foreach (var item in users)
                    {
                        var subscriptions = db.Omnicell_TicketSubscription.Where(x => x.UserId == item.UserId);

                        foreach (var sub in subscriptions)
                        {
                            var ticket = db.Omnicell_Ticket.FirstOrDefault(x => x.Id == sub.TicketId);

                            if (ticket.Updated > today)
                            {
                                tickets.Add(ticket);
                            }
                        }

                        if (tickets.Count != 0)
                        {
                            BuildEmail(tickets, item.Email, item.UserId);
                        }

                        tickets.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                Utilities.LogToTelligent("Exception occurred while sending weekly subscription emails:" + ex.ToString());
            }
        }

        private static void MonthlyEmails(DateTime first)
        {
            Utilities util = new Utilities();

            try
            {


                using (var db = new OmnicellEntities())
                {
                    var users = db.Omnicell_TicketEmailInfo.Where(x => x.IsImmediateUpdate == 3);
                    var tickets = new List<Omnicell_Ticket>();

                    foreach (var item in users)
                    {
                        var subscriptions = db.Omnicell_TicketSubscription.Where(x => x.UserId == item.UserId);

                        foreach (var sub in subscriptions)
                        {
                            var ticket = db.Omnicell_Ticket.FirstOrDefault(x => x.Id == sub.TicketId);

                            if (ticket.Updated > first)
                            {
                                tickets.Add(ticket);
                            }
                        }

                        if (tickets.Count != 0)
                        {
                            BuildEmail(tickets, item.Email, item.UserId);
                        }

                        tickets.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                Utilities.LogToTelligent("Exception occurred while sending monthly subscription emails:" + ex.ToString());
            }
        }

        // this method builds the email for the service request ticket digest.
        public static void BuildEmail(List<Omnicell_Ticket> tickets, string email, int? userId)
        {
            Utilities util = new Utilities();
            var content = new StringBuilder();
            content.Append("Below is your daily digest of updated service request tickets from myOmnicell:");
            content.Append(Environment.NewLine);
            content.Append(Environment.NewLine);
            string csn = "";

            List<string> exists = new List<string>();

            foreach (var ticket in tickets)
            {
                content.Append("Ticket Number: ");
                content.Append(ticket.Number);
                content.Append(Environment.NewLine);
                content.Append("CSN: ");
                content.Append(ticket.CSN);
                content.Append(Environment.NewLine);
                content.Append("Abstract: ");
                content.Append(ticket.Abstract);
                content.Append(Environment.NewLine);
                content.Append("Status: ");
                content.Append(ticket.Status);
                content.Append(Environment.NewLine);
                content.Append("Sub Status: ");
                content.AppendLine(ticket.SubStatus);
                content.Append(Environment.NewLine);
                content.Append("Last Update Date: ");
                content.AppendLine(ticket.Updated.Value.Date.ToString("MM/dd/yyyy"));
                content.Append(Environment.NewLine);
                content.AppendLine("___________________________________________________________________");
                content.Append(Environment.NewLine);

                if (exists.Count == 0)
                {
                    exists.Add(ticket.CSN);
                    csn += ticket.CSN + ",";
                }
                if (!exists.Exists(x => x == ticket.CSN))
                {
                    exists.Add(ticket.CSN);
                    csn += ticket.CSN + ",";
                }

            }


            string allCsn = csn.Remove(csn.LastIndexOf(','));
            string baseUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string unsubscribe = string.Format(baseUrl + "/customer_portal/p/service-request-tickets.aspx?CSN={0}", allCsn);
            content.Append("To unsubscribe from service ticket notifications, click the link below and follow the instructions on the Service Request Tickets homepage.");
            content.Append(Environment.NewLine);
            content.Append(unsubscribe);

            string fromEmail = "noreply-myomnicell@omnicell.com";
            string toEmail = email;//
            string toName = email;
            string fromName = "noreply-myomnicell@omnicell.com";
            string subject = "myOmnicell Service Request Ticket Digest";
            string body = content.ToString();

            util.SendEmail(fromEmail, fromName, toEmail, toName, subject, body);
            insertEmailCount(userId);
        }

        public static void insertEmailCount(int? userId)
        {
            var entities = new OmnicellEntities();
            try
            {
                var dbEntity = new Omnicell_EmailCounter()
                {
                    UserId = Convert.ToInt32(userId),
                    Date = DateTime.Now
                };
                entities.Omnicell_EmailCounter.AddObject(dbEntity);
                entities.SaveChanges();
            }
            catch (Exception ex)
            {
                // log the problem
                Utilities.LogToTelligent(ex.ToString());
            }

        }
    }
}
