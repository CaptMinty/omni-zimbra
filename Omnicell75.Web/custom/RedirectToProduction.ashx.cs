﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace Omnicell75.Web.custom
{
    /// <summary>
    /// Checks to see if the Correct query string is attached to
    /// the URL. If it isn't then the user is redirected to the
    /// myomnicell production instance. 
    /// </summary>
    public class RedirectToProduction : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string queryValue = AppGlobalSettings.RedirectQuery;
            string password = String.Empty;
            string returnUrl = String.Empty;

            // Check to see if development cookie exists
            HttpCookie devCookie = context.Request.Cookies["dev"];

            if (devCookie != null)
            {
                returnUrl = context.Request.Url.AbsoluteUri;
               // returnUrl = "http://omnicell.dev";
            }
            else
            {
                password = context.Request.QueryString["dev"];

                if (password == queryValue)
                {
                    HttpCookie newCookie = new HttpCookie("dev");
                    newCookie.Value = "true";
                    context.Response.Cookies.Add(newCookie);
                    returnUrl = context.Request.Url.AbsoluteUri;
                   // returnUrl = "http://omnicell.dev";

                }
                else
                {
                    returnUrl = "https://myomnicell.com";
                }
            }

            context.Response.Redirect(returnUrl);
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }

    public static class AppGlobalSettings
    {
        /// <summary>
        ///     Query string value for deciding if a user should
        ///     be redirected to the myomnicell production site
        /// </summary>
        static public string RedirectQuery { get; set; }

        static AppGlobalSettings()
        {
            RedirectQuery = WebConfigurationManager.AppSettings["RedirectQuery"];
        }
    }
}