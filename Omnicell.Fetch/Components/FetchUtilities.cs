﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Omnicell.Fetch
{
    public static class FetchUtilities
    {
        public static int GetLookupValue(string pair, int defaultValue)
        {
            int returnValue = defaultValue;
            if (!string.IsNullOrEmpty(pair) && pair.Contains("="))
            {
                if (pair.Contains("&"))
                    pair = pair.Split("&".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[0];

                string[] segs = pair.Split("=".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                if (segs.Length > 1)
                    int.TryParse(segs[1], out returnValue);
            }
            return returnValue;
        }
    }
}
