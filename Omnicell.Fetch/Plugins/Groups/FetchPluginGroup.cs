﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Evolution.Extensibility.Version1;

using Omnicell.Custom.Search;

namespace Omnicell.Fetch
{
    public class FetchPluginGroup : IPluginGroup
    {
        public void Initialize() { }
        public string Name
        {
            get { return "Omnicell Fetch Core Plugins"; }
        }
        public string Description
        {
            get { return "Contains all plugins defining core functionality for the Omnicell Fetch Service. This should not be removed."; }
        }
        public IEnumerable<Type> Plugins
        {
            get
            {
                return new[] {
                    typeof(OmnicellFetchWidgetProviderPlugin),
                    typeof(OmnicellFetchPlugin),
                    typeof(OmnicellFactSheetPlugin),
                    typeof(OmnicellImplementationRoomPlugin),
                    typeof(OmnicellProjectHealthPlugin)
                };
            }
        }
    }
}
