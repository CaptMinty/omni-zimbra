(function ($) {
if (typeof $.telligent === 'undefined') { $.telligent = {}; }
if (typeof $.telligent.evolution === 'undefined') { $.telligent.evolution = {}; }
if (typeof $.omnicell === 'undefined') { $.omnicell = {}; }
if (typeof $.omnicell.widgets === 'undefined') { $.omnicell.widgets = {}; }

setupColumnSort = function (context) {  

    var $currentCol =  $("td.tablecol a[rel='" + context.sortkey + "']") 
    if(context.sortdir == 'desc')
    {
        $currentCol.addClass("sort-button-desc");
    }
    else
    {
        $currentCol.addClass("sort-button-asc");
    }

    $('td.tablecol a').click(function() {
        var $this = $(this).closest('td');
   
       var $sortdir = context.sortdir; 
       
    if ( $(this).is( ".sort-button-asc" ) ) {
       $("td.tablecol a").removeClass("sort-button-asc");
       $("td.tablecol a").removeClass("sort-button-desc");
       $(this).addClass("sort-button-desc");
        $sortdir = "desc";
    }
    else
    {
         if ( $(this).is( ".sort-button-desc" ) ) {
           $("td.tablecol a").removeClass("sort-button-asc");
           $("td.tablecol a").removeClass("sort-button-desc");
           $(this).addClass("sort-button-asc");
           $sortdir = "asc";
        }
        else
        {
            $("td.tablecol a").removeClass("sort-button-asc");
            $("td.tablecol a").removeClass("sort-button-desc");
            $(this).addClass("sort-button-asc");
            $sortdir = "asc";
        }
    }
    var $sortkey = $(this).attr('rel');

    var qs = '?hth=' + encodeURIComponent(context.healthList.val())
                + '&pct=' + encodeURIComponent(context.percentagecompleteList.val())
                + '&start=' + encodeURIComponent(context.projectHealthDateStart.val())
                + '&end=' + encodeURIComponent(context.projectHealthDateEnd.val());

    if (typeof context.csn.val() !== 'undefined') {
        qs = qs + '&csn=' + encodeURIComponent(context.csn.val());
    }
    if (typeof context.facilityCity.val() !== 'undefined') {
        qs = qs + '&city=' + encodeURIComponent(context.facilityCity.val());
    }
    if (typeof context.facilityStateList.val() !== 'undefined') {
        qs = qs + '&state=' + encodeURIComponent(context.facilityStateList.val());
    }

    if (typeof context.territoryOpMgrInput.val() !== 'undefined') {
        qs = qs + '&tom=' + encodeURIComponent(context.territoryOpMgrInput.val());
    }
    if (typeof context.divisionalOpDirInput.val() !== 'undefined') {
        qs = qs + '&dod=' + encodeURIComponent(context.divisionalOpDirInput.val());
    }

    qs = qs + '&sort=' + encodeURIComponent($sortkey) + '&sdir=' + encodeURIComponent($sortdir);

    window.location = qs;
    return false;
    });
};

  var spinner = '<div style="text-align: center;"><img src="' + $.telligent.evolution.site.getBaseUrl() + 'utility/spinner.gif" /></div>',
searchUsers = function(context, textbox, searchText) {
    window.clearTimeout(context.inviteMemberByNameUserNameTimeout);
    if (searchText && searchText.length >= 2) {
        
		textbox.glowLookUpTextBox('updateSuggestions', [
        	textbox.glowLookUpTextBox('createLookUp', '', spinner, spinner, false)
        ]);                
        
        context.inviteMemberByNameUserNameTimeout = window.setTimeout(function () {
            $.telligent.evolution.get({
                url: context.findUsersOrRolesUrl,
                data: { w_SearchText: searchText, w_IncludeRoles: 'False' },
                success: function (response) {
                    if (response && response.matches.length > 1) {
                        var suggestions = [];
                        for (var i = 0; i < response.matches.length; i++) {
                            var item = response.matches[i];
                            if (item && item.userId) {
                                suggestions[suggestions.length] = textbox.glowLookUpTextBox('createLookUp', 'user:' + item.userId, item.title, item.title, true);
                            }
                            else if (item && item.ldapUserId) {
                                suggestions[suggestions.length] = textbox.glowLookUpTextBox('createLookUp', 'ldapUser:' + item.ldapUserId, item.title, item.title, true);
                            }
                        }
    
                        textbox.glowLookUpTextBox('updateSuggestions', suggestions);
                    }
                    else
                        textbox.glowLookUpTextBox('updateSuggestions', [textbox.glowLookUpTextBox('createLookUp', '', context.noUserOrRoleMatchesText, context.noUserOrRoleMatchesText, false)]);
                }
            });
        }, 749);
    }
},
attachHandlers = function(context) {
    var userBoxes = new Array();
    userBoxes[0] = $(context.territoryOpMgrInput);
    userBoxes[1] = $(context.divisionalOpDirInput);
    
    $.each(userBoxes, function(n) {
        if (this.length > 0) {

            var userVal = this.val();
            var userId = this.attr('data-id');

            this.glowLookUpTextBox({
            	delimiter: ',',
            	allowDuplicates: true,
            	maxValues: 1,
            	onGetLookUps: function(tb, searchText) {
            		searchUsers(context, tb, searchText);
            	},
            	emptyHtml: '',
            	selectedLookUpsHtml: [],
            	deleteImageUrl: ''
            });

            if (userVal.length > 0) {
                var userItem = this.glowLookUpTextBox('createLookUp', userId, userVal, userVal, true);
                this.glowLookUpTextBox('add', userItem);
            }
        }
    });
    
};









    $.omnicell.widgets.projecthealthreport = {
	register : function (context) {
 
         context.projectHealthDateStart.glowDateTimeSelector({
           pattern:"<01,02,03,04,05,06,07,08,09,10,11,12>/<1-31>/<0001-9999>",
           yearIndex:2,
           monthIndex:0,
           dayIndex:1,
           hourIndex:-1,
           minuteIndex:-1,
           amPmIndex:-1,
           showPopup:true,
           allowBlankValue:false
           });
   
   setTimeout(function(){
        var startDate = new Date();

        if (typeof context.startDate !== 'undefined'){
            if(context.startDate != '01/01/0001')
            {
                startDate = new Date(context.startDate);
            }
            else
            {
                startDate.setDate(startDate.getDate() -7);
            }
        }  
       context.projectHealthDateStart.glowDateTimeSelector('val', startDate);
   }, 10);


    context.projectHealthDateEnd.glowDateTimeSelector({
        pattern:"<01,02,03,04,05,06,07,08,09,10,11,12>/<1-31>/<0001-9999>",
        yearIndex:2,
        monthIndex:0,
        dayIndex:1,
        hourIndex:-1,
        minuteIndex:-1,
        amPmIndex:-1,
        showPopup:true,
        allowBlankValue:false
        });

    setTimeout(function(){
        var endDate = new Date();
   
        if (typeof context.endDate !== 'undefined'){
            if(context.endDate != '01/01/0001')
            {
                endDate = new Date(context.endDate);
            }
        }  
        context.projectHealthDateEnd.glowDateTimeSelector('val', endDate);
    }, 10);
    
        context.healthList.val(context.healthStatus);
        context.percentagecompleteList.val(context.healthPercentageComplete);

        setupColumnSort(context);
        attachHandlers(context);

context.searchButton.click(function()
{
    var qs = '?hth=' + encodeURIComponent(context.healthList.val()) 
                    + '&pct=' + encodeURIComponent(context.percentagecompleteList.val()) 
                    + '&start=' + encodeURIComponent(context.projectHealthDateStart.val())
                    + '&end=' + encodeURIComponent(context.projectHealthDateEnd.val());
                    
    if (typeof context.csn.val() !== 'undefined'){
        qs = qs  + '&csn=' + encodeURIComponent(context.csn.val());
    }
    if (typeof context.facilityCity.val() !== 'undefined'){
        qs = qs  + '&city=' + encodeURIComponent(context.facilityCity.val());
    }
    if (typeof context.facilityStateList.val() !== 'undefined'){
        qs = qs  + '&state=' + encodeURIComponent(context.facilityStateList.val());
    }
    
    if (typeof context.territoryOpMgrInput.val() !== 'undefined'){
        qs = qs  + '&tom=' + encodeURIComponent(context.territoryOpMgrInput.val());
    }
    if (typeof context.divisionalOpDirInput.val() !== 'undefined'){
        qs = qs  + '&dod=' + encodeURIComponent(context.divisionalOpDirInput.val());
    }
    
    window.location = qs;
	return false;
});    


	}
};

})(jQuery);