(function($, global) {

	var ActivityService = (function(){
	   var holds = 0,
			lockedDataKey = '_stream_locked_for_commenting',
			listUrl;
		return {
			lock: function(ta) {
				if(!$(ta).data(lockedDataKey)) {
					$(ta).data(lockedDataKey, true);
					holds++;
				}
			},
			unlock: function(ta) {
				if($(ta).data(lockedDataKey)) {
					$(ta).data(lockedDataKey, false);
					holds--;
					if(holds < 0)
						holds = 0;
				}
			},
			init: function(context) {
				listUrl = context.streamUrl;
			},
			list: function(query, success, error) {
				if(holds > 0) {
					return;
				}

				var data = {};
				$.each(query, function(key, value) {
					data['w_' + key] = value;
				})
				$.telligent.evolution.get({
					url: listUrl,
					data: data,
					cache: false,
					success: success,
					error: error
				});
			},
			del: function(id, success, error) {
				$.telligent.evolution.del({
					url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/stories/{StoryId}.json',
					data: { StoryId: id },
					cache: false,
					success: success,
					error: error
				});
			}
		};
	}());

	var ReplyService = (function(){
		var listUrl;
		return {
			init: function(context) {
				listUrl = context.moreCommentsUrl;
			},
			add: function(activityElement, comment, success, fail) {
				var data = {
						ContentId: activityElement.data('contentid'),
						ContentTypeId: activityElement.data('contenttypeid'),
						Body: comment
					};

				if (activityElement.data('typeid')) {
					data.CommentTypeId = activityElement.data('typeid');
				}

				$.telligent.evolution.post({
					url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/comments.json',
					data: data,
					cache: false,
					success: success,
					error: fail
				});
			},
			del: function(activityElement, comment, success, fail) {
				$.telligent.evolution.del({
					url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/comments/{CommentId}.json',
					data: {
						CommentId: comment.data('commentid')
					},
					cache: false,
					success: success,
					error: fail
				});
			},
			list: function(query, success, fail) {
				var data = {};
				$.each(query, function(key, value) {
					data['w_' + key] = value;
				})
				$.telligent.evolution.get({
					url: listUrl,
					data: data,
					cache: false,
					success: success,
					error: fail
				});
			}
		};
	}());

	var Controller = (function(){
		var activities,
			replies,
			widgetContext,
			loading = false,
			dayDividers = [],
			extraLoadCount = 0,
			maxExtraLoads = 10,
			activityMessageElementFor = function(element) {
				return $(element).closest('li.content-item.activity');
			},
			clear = function() {
				widgetContext.container.empty();
				widgetContext.wrapper.closest('.layout-region.content').css({'min-height':'900px'});
				widgetContext.wrapper.closest('.layout').find('.layout-region-inner.right-sidebar').css({'min-height':'900px'});
			},
			windowIsScrollable = function() {
				return $(document).height() > $(window).height();
			},
			load = function(showMore, autoRefresh) {
				loading = true;
				var handleResponse = function(response) {
					var response = $.trim(response);
					if (response) {
						var falseContainer = $('<div></div>');
						response = $(response);
						response.appendTo(falseContainer);

						var data = response.children('li.data').remove();
						if(!autoRefresh) {
							if (data.length > 0) {
								widgetContext.lastMessageDate = data.data('lastmessagedate');
								widgetContext.hasMoreData = true;
								widgetContext.pager.find('a').show();
							} else {
								widgetContext.hasMoreData = false;
								widgetContext.pager.find('a').hide();
							}
						}

						var items = response.children('li');

						widgetContext.wrapper.trigger('streamLoaded', {
							activities: items
						});

						renderMessages(items, autoRefresh);
					} else {
						widgetContext.hasMoreData = false;
						widgetContext.pager.find('a').hide();
						widgetContext.wrapper.trigger('streamLoadError');
					}
					loading = false;

					// if the window isn't yet scrollable and this wasn't for a single message,
					// try loading more
					if(!windowIsScrollable() && !widgetContext.messageId && widgetContext.hasMoreData && extraLoadCount < maxExtraLoads) {
						extraLoadCount++;
						load(true, false);
					} else {
						extraLoadCount = 0;
					}
				};

				if (widgetContext.messageId) {
					widgetContext.wrapper.trigger('streamLoading');

					activities.list({
							filterIndex: widgetContext.filterIndex,
							messageId: widgetContext.messageId,
							userId: widgetContext.userId
						},
						handleResponse,     // success
						function() {        // error
							widgetContext.wrapper.trigger('streamLoadError');
							loading = false;
						});

				} else {
					widgetContext.wrapper.trigger('streamLoading');

					activities.list({
						filterIndex: widgetContext.filterIndex,
						filterType: widgetContext.filterType,
						endDate: showMore ? widgetContext.lastMessageDate : '',
						pageSize: widgetContext.pageSize,
						userId: widgetContext.userId,
						group: widgetContext.groupId
					},
					handleResponse,     // success
					function() {        // error
						widgetContext.wrapper.trigger('streamLoadError');
						loading = false;
					});
				}
			},
			renderMessages = function(messages, autoRefresh) {
				// filter newly fetched messages into new messages and existing
				var newMessages = [],
					existingMessages = [];
				$.each(messages, function(i, message){
					var existingMessage = $('#' + message.id);
					(existingMessage.length === 0 ? newMessages : existingMessages).push(message);
				});

				var append = function(message) {
					var latestDate = null,
						latestMessage = widgetContext.container.find('li.activity:first');
					if(latestMessage.length > 0) {
						latestDate = $.telligent.evolution.parseDate(latestMessage.data('activitydate'));
					}
					var messageDate = message.data('activitydate') ? $.telligent.evolution.parseDate(message.data('activitydate')) : new Date();
					if(messageDate > latestDate) {
						widgetContext.container.prepend(message);
					} else {
						widgetContext.container.append(message);
					}
				}

				if(autoRefresh) {
					// reverse to inject from top down
					newMessages.reverse();
				}

				// add new messages to ui, initing against client behaviors as well
				$.each(newMessages, function(i, message) {
					message = $(message);
					widgetContext.wrapper.trigger('activityAppending', { activity: message, auto: autoRefresh });
					append(message);
					widgetContext.wrapper.trigger('activityAppended', { activity: message, auto: autoRefresh  });
				});

				// for existing messages, add any new comments to the ui
				$.each(existingMessages, function(i, message){
					// refresh dates of existing messages
					refreshContentItem($('#' + message.id), $(message), 'post-date');
					// refresh comment counts of existing messages
					refreshContentItem($('#' + message.id), $(message), 'collapsed-comments');
					// refresh comments
					renderComments($(message), $(message).find('li.content-item.comment').not('.comment-form'), autoRefresh);
				});

				renderDayDividers();
			},
			renderDayDividers = function() {
				$.each(dayDividers, function(i, divider) {
					divider.remove();
				});
				dayDividers = [];
				var currentDate = null;
				var activityItems = widgetContext.container.find('li.activity');
				$.each(activityItems, function(i, activity){
					var activity = $(activity),
						date = $.telligent.evolution.parseDate(activity.data('activitydate'));

					if(currentDate === null || !(currentDate.getFullYear() === date.getFullYear() && currentDate.getMonth() === date.getMonth() && currentDate.getDay() === date.getDay())) {
						currentDate = date;
						var divider = $('<li class="activity-date-grouping"><span></span></li>'),
							dividerSpan = divider.find('span');
						activity.before(divider);
						buildDivider(date, function(formattedDivider) {
							dividerSpan.html(formattedDivider.text);
							if (formattedDivider.today) {
								divider.addClass('today');
							}
						});
						dayDividers.push(divider);
					}
				});
			},
			buildDivider = function(date, complete) {
				var now = new Date();
				if (date.getFullYear() === now.getFullYear() && date.getMonth() === now.getMonth() && date.getDate() === now.getDate()) {
					complete({ text: widgetContext.todayText, today: true });
				} else {
					// floor the date at the beginning of the day to maximimze cache hits for formatted dates
					var dayBasedDate = new Date(date.getFullYear(), date.getMonth(), date.getDate());
					$.telligent.evolution.language.formatDate(dayBasedDate, function(formatted) {
						complete({ text: formatted, today: false });
					});
				}
			},
			contains = function(array, fn) {
				var contains = false;
				$.each(array, function(i, val){
					if(fn(val)) {
						contains = true;
						return false;
					}
				});
			},
			refreshContentItem = function(existingElement, refreshedElement, className) {
				var existing = existingElement.first().find('.' + className);
				var refreshed = refreshedElement.first().find('.' + className);
				if(existing.length > 0 && refreshed.length > 0) {
					existing.html(refreshed.html());
				}
			},
			refreshComments = function(activityElement, autoRefresh) {
				// when commenting is successful, get the new rendered comment list only
				// for this activity item
				activities.list({
						messageId: activityElement.data('activityid'),
						userId: activityElement.data('authorid'),
						endDate: activityElement.data('activitydate')
					},
					function(response) {        // success
						var response = $.trim(response);
						if (response) {
							response = $(response);
							renderComments(response.find('li.content-item.activity'),
								response.find('li.content-item.comment').not('.comment-form'));
						}
					},
					function() {        // error
						widgetContext.wrapper.trigger('streamLoadError');
					});
			},
			renderComments = function(messageItem, newCommentItems, autoRefresh) {
				var existingMessage = $('#' + messageItem.attr('id'));
				if(existingMessage) {
					var replyForm = existingMessage.find('li.content-item.comment.comment-form'),
						replyList = existingMessage.find('ul.content-list.comments'),
						existingComments = existingMessage.find('li.content-item.comment').not('.comment-form'),
						keepComments = [],
						appendReply = function(reply) {
							var replyDate = $.telligent.evolution.parseDate(reply.data('commentdate')),
								latestReply = replyList.find('li.comment:not(li.comment-form):last');
							if(latestReply.length > 0 && replyDate > $.telligent.evolution.parseDate(latestReply.data('commentdate'))) {
								latestReply.after(reply);
							} else if(latestReply.length > 0) {
								existingComments.before(reply);
							} else if(replyForm.length > 0) {
								replyForm.before(reply);
							} else {
								replyList.append(reply);
							}
						};

					// add new comments, track comments that shouildn't be removed
					$.each(newCommentItems, function(i, newComment){
						newComment = $(newComment);
						newComment.hide();

						var newCommentId = newComment.data('commentid');
						var existingComment = existingComments.filter('[data-commentid="'+newCommentId+'"]').first();

						if (existingComment.length === 1) {
							keepComments[newCommentId] = newComment;
						} else {
							appendReply(newComment);
							widgetContext.wrapper.trigger('commentAdded', { activityElement: existingMessage, comment: newComment, auto: autoRefresh });
						}
					});

					// remove deleted comments or update time of non-deleted existing
					$.each(existingComments, function(i, comm) {
						comm = $(comm);
						var keptComment = keepComments[comm.data('commentid')];
						if(!keptComment) {
							widgetContext.wrapper.trigger('commentDeleted', { comment: comm });
						} else {
							refreshContentItem(comm, keptComment, 'reply-date');
						}
					});
				}
			},
			debounce = function(fn, ms) {
				var calledInInterval = true;
				setInterval(function(){
					calledInInterval = false;
				}, ms);
				return function(){
					if(!calledInInterval) {
						calledInInterval = true;
						fn.apply(this, arguments)
					}
				};
			},
			bindEvents = function() {
				// load messages on browser focus, bounced to a max of 1 auto-refresh per minute
				$(window).bind('focus', debounce(function(){
					load(false, true);
				}, 1 * 60 * 1000));

				var refresh = function() { load(false, false); };
				// handle explicit updates triggered from other widgets
				$(document).bind('telligent_messaging_activitymessageupdated', refresh);
				$.telligent.evolution.messaging.subscribe('activity.commentadded', refresh);
				$.telligent.evolution.messaging.subscribe('activity.commentremoved', refresh);

				// automatically load more once scrolled to the end of the page
				if(widgetContext.endlessScroll) {
					$(document).bind('scrollend', function() {
						if(!loading && widgetContext.hasMoreData) {
							load(true, false);
						}
					});
				}

				// filter tbas
				if(widgetContext.tabs.find('a').length > 1) {
					widgetContext.tabs.find('span.filter-option:first').addClass('selected');
					widgetContext.tabs
						.show()
						.delegate('a', 'click', function(e){
							e.preventDefault();
							widgetContext.filterIndex = $(e.target).data('filterindex');
							widgetContext.wrapper.trigger('filterSelected', { filterElement: $(e.target) });
							clear();
							load();
						});
				}

				// activity story deleting
				widgetContext.wrapper.delegate('.full-post.activity','evolutionModerateLinkClicked', function(e, link){
					e.preventDefault();
					if(confirm(widgetContext.deleteActivityMessage)) {
						var activityElement = $(activityMessageElementFor(this));
						widgetContext.wrapper.trigger('activityDeleting', { activityElement: activityElement });
						activities.del(activityElement.data('activityid'),
							function(){
								widgetContext.wrapper.trigger('activityDeleted', { activityElement: activityElement });
							},
							function(){
								widgetContext.wrapper.trigger('activityDeleteError', { activityElement: activityElement });
							});
					}
					return false;
				});

				// private conversation starting
				widgetContext.wrapper.delegate('.post-actions.activity a.internal-link.start-conversation', 'click', function(e) {
					e.preventDefault();
					var conversationUrl = $(e.target).data('conversationurl');
					$.glowModal(conversationUrl, { width: 550, height: 360 });
				});
				// comment form revealing
				widgetContext.wrapper.delegate('.post-actions.activity a.internal-link.comment', 'click', function(e) {
					e.preventDefault();
					widgetContext.wrapper.trigger('commentBegin', { activity: activityMessageElementFor(e.target) });
				});
				// comment form focusing/unfocusing
				widgetContext.wrapper.delegate('.comment-form textarea', 'focus', function(e) {
					widgetContext.wrapper.trigger('commentEdit', { activity: activityMessageElementFor(e.target) });
					var ta = $(e.target);
					// when a text area is focused, set up its composer and set up handlers for submitting its results
					if(!ta.data('composer_inited')) {
						ta.data('composer_inited', true);
						ta.evolutionComposer({
							plugins: ['mentions','hashtags']
						});
						ta.evolutionComposer('onkeydown', function(e){
							if (e.which === 13)
							{
								var activityElement = $(activityMessageElementFor(e.target)),
									body = $(e.target).evolutionComposer('val'),
									replyData = {
										activityElement: activityElement,
										body: body
									};
								if($.trim(body).length > 0) {
									widgetContext.wrapper.trigger('commentAdding', replyData);
									replies.add(activityElement, body,
										function(response) {    // success
											if(!response.Comment.IsApproved) {
												$.telligent.evolution.notifications.show(widgetContext.commentModeratedMessaage, { type: 'success', duration: 10000 });
												widgetContext.wrapper.trigger('commentAddError', replyData);
											} else {
												refreshComments(activityElement);
											}
										},
										function(response) {    // error
											widgetContext.wrapper.trigger('commentAddError', replyData);
										});
								}
								return false;
							} else {
								return true;
							}
						});
					}
				});
				widgetContext.wrapper.delegate('.comment-form textarea', 'blur', function(e) {
					widgetContext.wrapper.trigger('commentCancel', { activity: activityMessageElementFor(e.target) });
				});

				// comment deleting
				// comment deleting
				widgetContext.wrapper.delegate('.full-post.comment','evolutionModerateLinkClicked', function(e, link){
					e.preventDefault();
					if(confirm(widgetContext.deleteCommentMessage)) {
						var activityElement = $(activityMessageElementFor(this)),
							commentElement = $(this).closest('li.content-item.comment'),
							data = {
								activityElement: activityElement,
								comment: commentElement
							};
						widgetContext.wrapper.trigger('activityDeleting', data);
						replies.del(activityElement, commentElement,
							function(){
								//widgetContext.wrapper.trigger('commentDeleted', data);
								refreshComments(activityElement);
							},
							function(){
								widgetContext.wrapper.trigger('activityDeleteError', data);
							});
					}
					return false;
				});
				// collapsed comment expanding
				widgetContext.wrapper.delegate('.comments .content-item.collapsed-comments a', 'click', function(e) {
					e.preventDefault();
					widgetContext.wrapper.trigger('commentsShowHidden', { activityElement: $(activityMessageElementFor(e.target)) });
				});
				widgetContext.wrapper.delegate('.comments .content-item.action.collapse a', 'click', function(e) {
					e.preventDefault();
					replies.list({
						commentPageIndex: $(e.target).data('pageindex'),
						storyId: $(e.target).data('storyid')
					}, function(response){
						widgetContext.wrapper.trigger('commentsMoreLoaded', {
							comments: $(response),
							activityElement: $(activityMessageElementFor(e.target))
						});
					}, function(){
						widgetContext.wrapper.trigger('commentsMoreError');
					});
				});
				$.telligent.evolution.messaging.subscribe('ui.like', function(data){
					var likedActivityElement = widgetContext.wrapper.find('li.activity-story[data-contentid="' + data.contentId + '"][data-typeid="' + data.typeId + '"]'),
						likedCommentElement = null;
					if(likedActivityElement == null || likedActivityElement.length === 0) {
						likedCommentElement = widgetContext.wrapper.find('li.comment[data-commentid="' + data.contentId + '"][data-contenttypeid="' + data.contentTypeId + '"]')
						if(data.count > 0) {
							widgetContext.wrapper.trigger('likeAdded', { commentElement: likedCommentElement });
						} else {
							widgetContext.wrapper.trigger('likesRemoved', { commentElement: likedCommentElement });
						}
					} else {
						if(data.count > 0) {
							widgetContext.wrapper.trigger('likeAdded', { activityElement: likedActivityElement });
						} else {
							widgetContext.wrapper.trigger('likesRemoved', { activityElement: likedActivityElement });
						}
					}
				});
			};
		return {
			init: function(context, activityService, replyService) {
				widgetContext = context;
				activities = activityService;
				replies = replyService;

				context.pager.find('a').bind('click', function(e){
					e.preventDefault();
					load(true)
				});

				bindEvents();
				load();
			},
			on: function(context, eventHandlers) {
				context.wrapper.bind(eventHandlers);
			},
			renderDayDividers: renderDayDividers
		};
	}());

	var View = (function(){
		var captureUiElements = function(context) {
				// select elements
				$.each(['tabs','wrapper','loader','pager','container'], function(i, val) {
					context[val] = $(context[val]);
				});
			},
			handleEvents = function(context, controller) {
				controller.on(context, {
					activityDeleting: function(e, data) { },
					activityDeleted: function(e, data) {
						data.activityElement.fadeOut(250, function(){
							data.activityElement.remove();
							Controller.renderDayDividers();
						});
					},
					activityDeleteError: function(e, data) { },
					streamLoading: function(e, data) {
						context.loader.show();
					},
					streamLoaded: function(e, data) {
						context.loader.hide();
						context.wrapper.find('li.highlight').removeClass('highlight');
					},
					streamLoadError: function(e, data) {
						context.loader.hide();
						context.wrapper.find('li.highlight').removeClass('highlight');
					},
					activityAppending: function(e, data) {
						data.activity.hide();
					},
					activityAppended: function(e, data) {
						if(data.auto) {
							data.activity.addClass('highlight').fadeIn(300);
						} else {
							data.activity.show();
						}
					},
					commentBegin: function(e, data) {
						data.activity
							.find('.comment-form')
							.show()
							.find('textarea')
							.focus();
					},
					commentEdit: function(e, data) {
						ActivityService.lock(data.activity.find('.comment-form textarea'));
						data.activity
							.find('.comment-form')
							.addClass('with-avatar');
					},
					commentCancel: function(e, data) {
						var ta = data.activity.find('.comment-form textarea');
						if(ta.val().length === 0) {
							ActivityService.unlock(ta);
						}
						data.activity
							.find('.comment-form')
							.removeClass('with-avatar');
					},
					commentAdding: function(e, data) {
						ActivityService.unlock(data.activityElement.find('.comment-form textarea'));
						data.activityElement
							.find('.comment-form textarea')
							.attr('disabled',true)
							.blur();
					},
					commentAdded: function(e, data) {
						data.activityElement
							.find('.comment-form textarea')
							.evolutionComposer('val','')
							.trigger('keydown') // to trigger autoresize to collapse
							.trigger('keyup')   // to trigger autoresize to collapse
							.removeAttr('disabled')
							.closest('.comment-form')
							.removeClass('with-avatar');
						if(data.auto) {
							data.comment.addClass('highlight').fadeIn(300);
						} else {
							data.comment.show();
						}
					},
					commentAddError: function(e, data) {
						data.activityElement
							.find('.comment-form textarea')
							.evolutionComposer('val','')
							.trigger('keydown')  // to trigger autoresize to collapse
							.trigger('keyup')   // to trigger autoresize to collapse
							.removeAttr('disabled')
							.closest('.comment-form')
							.removeClass('with-avatar');
					},
					commentDeleting: function(e, data) { },
					commentDeleted: function(e, data) {
						data.comment.remove();
					},
					commentDeleteError: function(e, data) {
					},
					commentsShowHidden: function(e, data) {
						data.activityElement.find('li.collapse').show().end().find('li.collapsed-comments').hide();
					},
					commentsMoreLoaded: function(e, data) {
						var viewMoreItem = data.activityElement.find('li.content-item.action.collapse');
						viewMoreItem.replaceWith(data.comments);
						data.activityElement.find('li.content-item.action.collapse').show();
					},
					commentsMoreError: function(e, data) {
					},
					filterSelected: function(e, data) {
						data.filterElement.closest('.filters').find('.filter-option').removeClass('selected');
						data.filterElement.closest('.filter-option').addClass('selected');
					},
					likesRemoved: function(e, data) {
						if(data.activityElement)
							data.activityElement.find('li.likes').removeClass('with-likes').addClass('without-likes');
						if(data.commentElement)
							data.commentElement.removeClass('with-likes');
					},
					likeAdded: function(e, data) {
						if(data.activityElement)
							data.activityElement.find('li.likes').removeClass('without-likes').addClass('with-likes');
						if(data.commentElement)
							data.commentElement.addClass('with-likes');
					}
				});
			};
		return {
			init: function(context, controller) {
				captureUiElements(context);
				handleEvents(context, controller);
			}
		};
	}());

	var api = {
		register: function(options) {
			var context = $.extend({ filterIndex: 0, hasMoreData: true }, options);
			ActivityService.init(context);
			ReplyService.init(context);
			View.init(context, Controller);
			Controller.init(context, ActivityService, ReplyService);
		}
	};

	if (!$.telligent) { $.telligent = {}; }
	if (!$.telligent.evolution) { $.telligent.evolution = {}; }
	if (!$.telligent.evolution.widgets) { $.telligent.evolution.widgets = {}; }
	$.telligent.evolution.widgets.activityStoryStream = api;

})(jQuery, window);
