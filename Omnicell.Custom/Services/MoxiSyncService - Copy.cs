﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using ICSharpCode.SharpZipLib.Zip;
using Microsoft.SharePoint.Client;
using Omnicell.Custom.Components;
using Omnicell.Data.Model;
using Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.Api.Version1;
using SPApi = Telligent.Evolution.Extensions.SharePoint.Client.Api.Version1;
using SPClient = Telligent.Evolution.Extensions.SharePoint.Client;
using SPExtv1 = Telligent.Evolution.Extensibility.Api.Entities.Version1;
using TApi = Telligent.Evolution.Extensibility.Api.Version1;
using TEntities = Telligent.Evolution.Extensibility.Api.Entities.Version1;
using TExV1 = Telligent.Evolution.Extensibility.Version1;
using Telligent.Evolution.Wikis;
using Microsoft.SharePoint.Client.Utilities;


namespace Omnicell.Custom.Components
{
    public interface IMoxiSyncService
    {
        SPExtv1.PagedList<SPApi.Document> GetSharepointLibrary(string title);
        DateTime GetWebCreatedDate();
        SPExtv1.PagedList<SPApi.Library> GetLibraryList(int groupID);
    }

    public class MoxiSyncService : IMoxiSyncService
    {
        private readonly ICacheService _cache = null;
        private readonly OmnicellSharepointPlugin _osp = null;
        private OmnicellSharepointPlugin _plgnSP = null;
        private List<string> documentIds = new List<string>();
        private List<OmnicellWikiPage> interLinks = new List<OmnicellWikiPage>();
        private int iCount = 0;


        public MoxiSyncService()
        {
            _cache = Telligent.Common.Services.Get<ICacheService>();
            _osp = Telligent.Evolution.Extensibility.Version1.PluginManager.Get<OmnicellSharepointPlugin>().FirstOrDefault();
            _plgnSP = TExV1.PluginManager.Get<OmnicellSharepointPlugin>().FirstOrDefault();
        }
        public SPExtv1.PagedList<SPApi.Library> GetLibraryList(int groupID)
        {
            SPApi.LibraryListOptions options = new SPApi.LibraryListOptions();
            options.PageIndex = 0;
            options.PageSize = 5;
            SPExtv1.PagedList<SPApi.Library> lstLibraries = SPApi.PublicApi.Libraries.List(groupID, options);

            return lstLibraries;
        }
        public SPExtv1.PagedList<SPApi.Document> GetSharepointLibrary(string guidID)
        {
            // connect to Sharepoint and get a list of the libraries
            SPClient.version2.SharePointLibrary spLib2 = new SPClient.version2.SharePointLibrary();
            SPExtv1.PagedList<SPApi.Document> lstDocs = new SPExtv1.PagedList<SPApi.Document>();

            SPApi.Library splib = new SPApi.Library();
            try
            {
                // convert the library ID to GUID
                Guid LibIDGuid = new Guid(guidID);
                splib = spLib2.Get(LibIDGuid);
                Guid AppGuidID = new Guid(splib.ApplicationId.ToString());
                // this library has a groupID, get all the files associated with the group ID
                int igroupID = splib.GroupId;
                SPApi.DocumentListOptions options = new SPApi.DocumentListOptions();
                options.FolderPath = splib.Root;  // /Repository/Publications /?
                options.PageSize = 5000;
                lstDocs = SPApi.PublicApi.Documents.List(LibIDGuid, options);
            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error converting library id to guid: {0}", guidID), ex).Log();
            }

            return lstDocs;
        }
        public DateTime GetWebCreatedDate()
        {
            using (ClientContext context = new ClientContext(_osp.SPFullUrl))
            {
                Web web = context.Web;
                context.Load(web, w => w.Created);
                context.ExecuteQuery();
                return web.Created;
            }
        }

        public ZipInputStream GetRemoteZipStream(string remoteUrl, string domain, string userName, string passWord)
        {
            ZipInputStream zistream;
            WebResponse response = null;
            WebRequest request = WebRequest.Create(remoteUrl);
            String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(userName + ":" + passWord));

            request.PreAuthenticate = true;
            request.Credentials = new NetworkCredential(userName, passWord, domain);

            if (request != null)
            {
                try
                {
                    response = request.GetResponse();
                    if (response != null)
                    {
                        zistream = new ZipInputStream(response.GetResponseStream());
                        return zistream;
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }
            return null;

        }


        public SharepointDITA.DitaDocMetaData ExtractMetaDataFromTOCFile(HtmlDocument doc)
        {
            SharepointDITA.DitaDocMetaData ddmd = new SharepointDITA.DitaDocMetaData();
            try
            {
                // Html Agility Code


                IEnumerable<HtmlNode> collection = doc.DocumentNode.Descendants("meta").Where(a => a.Attributes.Contains("name"));
                HtmlNode title = doc.DocumentNode.SelectSingleNode("//title");
                ddmd.CoreProperties.Title = title.InnerHtml;

                foreach (var meta in collection)
                {
                    string nodeName = meta.Attributes["name"].Value;
                    string value = meta.Attributes["content"].Value.Replace('\uFF06', '&').Replace('\uFF09', ')').Replace('\uFF08', '(');
                    //value = ConvertUTF16(value);
                    switch (nodeName)
                    {

                        case "Id": ddmd.CustomProperties.Id = meta.Attributes["content"].Value; break;
                        case "Product": ddmd.CustomProperties.Product = meta.Attributes["content"].Value; break;
                        case "ProductVersion": ddmd.CustomProperties.ProductVersion = meta.Attributes["content"].Value; break;
                        case "Audience": ddmd.CustomProperties.Audience = value; break;
                        case "Interfaces": ddmd.CustomProperties.Interfaces = value; break;
                        case "OmniFeatures": ddmd.CustomProperties.Features = value; break;
                        case "OmniDocumentType": ddmd.CustomProperties.DocumentType = meta.Attributes["content"].Value; break;
                        case "ShortDescription": ddmd.CoreProperties.Description = meta.Attributes["content"].Value; break;
                        case "targetgroup": ddmd.CustomProperties.TargetGroup = meta.Attributes["content"].Value; break;
                        case "RevNum": ddmd.CustomProperties.RevNum = meta.Attributes["content"].Value; break;
                        case "PartName": ddmd.CustomProperties.PartName = meta.Attributes["content"].Value; break;
                    }

                }

            }
            catch (Exception ex)
            {
                throw ex;

            }


            return ddmd;
        } // end of method

        public void CreateUpdateWiki(SPApi.Document doc)
        {

            string strDocName = "";
            strDocName = GetFileNameFromUrl(doc.Path);
            string docName = doc.Name.Replace(" - DITA Map XHTML.zip", "").ToLower();
            if (!String.IsNullOrEmpty(strDocName) && strDocName.Contains(" - DITA Map XHTML.zip"))
            {
                try
                {
                    SPApi.Document spZIPdoc = new SPApi.Document();

                    SPExtv1.PagedList<SPApi.Document> spdoclist = new SPExtv1.PagedList<SPApi.Document>();
                    spdoclist = GetSharepointLibrary(doc.Library.Id.ToString());

                    foreach (SPApi.Document document in spdoclist)
                    {
                        if (document.Name.ToLower() == strDocName.ToLower())
                        {
                            spZIPdoc = document;
                            break;
                        }
                    }
                    if (spZIPdoc != null)
                    {

                        if (spZIPdoc.Path != "" && doc.Path != "")
                        {

                            string directory = Environment.CurrentDirectory + Path.DirectorySeparatorChar + "WikiTemp";

                            CreateOrUpdateWiki(spZIPdoc, docName, directory, docName, spdoclist, doc);

                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

        }

        public void CreateNewWiki(int groupId, string path, string key, Guid contentId, SharepointDITA.DitaDocMetaData smdt)
        {

            string tocLink = path + Path.DirectorySeparatorChar + "toc.html";
            try
            {

                // Html Agility Code
                HtmlDocument doc = new HtmlDocument();
                doc.Load(tocLink, Encoding.UTF8);

                HtmlNode title = doc.DocumentNode.SelectSingleNode("//title");
                string wikiName = title.InnerHtml;
                wikiName = wikiName.Replace("&amp;", "&");

                WikisCreateOptions options = new WikisCreateOptions();
                options.Key = key;
                options.Description = smdt.CoreProperties.Description;

                var newWiki = PublicApi.Wikis.Create(groupId, wikiName, options);

                if (newWiki != null)
                {
                    OmnicellWiki oWiki = new OmnicellWiki(newWiki);
                    // process wiki metadata
                    OmnicellWikiMetadata wikiMetadata = PopulateMetadata(smdt);
                    wikiMetadata.GroupId = groupId;
                    wikiMetadata.WikiId = (int)oWiki.Id;
                    wikiMetadata.DocumentId = contentId;
                    oWiki.wikiMeta = wikiMetadata;
                    WikiPageExtensions.SaveMetaData(wikiMetadata);

                    string defaultPageKey = "DefaultWikiPage";
                    var parentWikiPage = PublicApi.WikiPages.Get(new TApi.WikiPagesGetOptions { WikiId = newWiki.Id, PageKey = defaultPageKey });

                    if (parentWikiPage != null)
                    {

                        WikiPagesUpdateOptions up = new WikiPagesUpdateOptions();
                        string defaultTags = "DocGUID:" + contentId.ToString();
                        up.Tags = defaultTags;
                        up.Body = smdt.CoreProperties.Description;
                        int newWikiId = (int)parentWikiPage.Id;
                        var wikiUpdate = PublicApi.WikiPages.Update(newWikiId, up);

                        OmnicellWikiPage dPage = new OmnicellWikiPage(wikiUpdate);



                        OmnicellWikiPageMetadata pageMeta = new OmnicellWikiPageMetadata(oWiki.wikiMeta);
                        pageMeta.contentId = contentId.ToString();
                        pageMeta.WikiPageId = (int)wikiUpdate.Id;
                        pageMeta.ParentPageId = 0;
                        pageMeta.OrderNumber = 0;
                        pageMeta.PageKey = dPage.PageKey;
                        pageMeta.ShortDesc = smdt.CoreProperties.Description;

                        WikiPageExtensions.SavePageMetaData(pageMeta);
                        dPage.metaData = pageMeta;

                        oWiki.wikiPages.Add(dPage);



                        CreateWikiPages(oWiki, dPage, path);
                    }

                }


            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error creating wiki with Key: {0}, and contentId: {1}", key, contentId), ex).Log();

            }
            finally
            {
                DeleteFiles(path);
            }

        } // end of method

        public void EditWiki(int groupId, string path, int wikiId, string key, Guid contentId, SharepointDITA.DitaDocMetaData smdt)
        {

            string tocLink = path + Path.DirectorySeparatorChar + "toc.html";
            try
            {

                // Html Agility Code
                HtmlDocument doc = new HtmlDocument();
                doc.Load(tocLink, Encoding.UTF8);

                HtmlNode title = doc.DocumentNode.SelectSingleNode("//title");
                string wikiName = title.InnerHtml;
                WikisUpdateOptions options = new WikisUpdateOptions();
                options.Name = title.InnerHtml;
                options.Key = key;
                options.GroupId = groupId;
                options.Description = smdt.CoreProperties.Description;
                var newWiki = PublicApi.Wikis.Update(wikiId, options);

                if (newWiki != null)
                {

                    OmnicellWiki oWiki = new OmnicellWiki(newWiki);
                    //process wiki metadata
                    OmnicellWikiMetadata wikiMetadata = PopulateMetadata(smdt);
                    wikiMetadata.GroupId = groupId;
                    wikiMetadata.WikiId = (int)oWiki.Id;
                    wikiMetadata.DocumentId = contentId;
                    oWiki.wikiMeta = wikiMetadata;
                    WikiPageExtensions.SaveMetaData(wikiMetadata);

                    string defaultPageKey = "DefaultWikiPage";
                    var parentWikiPage = PublicApi.WikiPages.Get(new TApi.WikiPagesGetOptions { WikiId = newWiki.Id, PageKey = defaultPageKey });

                    if (parentWikiPage != null)
                    {
                        WikiPagesUpdateOptions up = new WikiPagesUpdateOptions();
                        int newWikiId = (int)parentWikiPage.Id;
                        string defaultTags = "DocGUID:" + contentId.ToString();
                        documentIds.Add(contentId.ToString());
                        up.Tags = defaultTags;
                        up.Body = smdt.CoreProperties.Description;
                        var wikiUpdate = PublicApi.WikiPages.Update(newWikiId, up);

                        OmnicellWikiPage dPage = new OmnicellWikiPage(wikiUpdate);



                        OmnicellWikiPageMetadata pageMeta = new OmnicellWikiPageMetadata(oWiki.wikiMeta);
                        pageMeta.contentId = contentId.ToString();
                        pageMeta.WikiPageId = (int)dPage.Id;
                        pageMeta.ParentPageId = 0;
                        pageMeta.OrderNumber = 0;
                        pageMeta.PageKey = dPage.PageKey;
                        pageMeta.ShortDesc = smdt.CoreProperties.Description;

                        WikiPageExtensions.SavePageMetaData(pageMeta);
                        dPage.metaData = pageMeta;

                        oWiki.wikiPages.Add(dPage);

                        EditWikiPages(oWiki, dPage, path);
                        DeleteWikiPages(wikiId, tocLink);

                    }

                }

            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error updating wiki with Id: {0}, and contentId: {1}", wikiId, contentId), ex).Log();

            }
            finally
            {
                DeleteFiles(path);
            }

        } // end of method

        private void CreateWikiPages(OmnicellWiki oWiki, OmnicellWikiPage pWikipage, string path)
        {
            HtmlDocument tDoc = new HtmlDocument();
            string tocLink = path + Path.DirectorySeparatorChar + "toc.html";
            tDoc.Load(tocLink, Encoding.UTF8);
            HtmlNode bULTag = tDoc.DocumentNode.Descendants("ul").FirstOrDefault();

            IEnumerable<HtmlNode> tocNodes = from node in bULTag.ChildNodes
                                             where node.Name == "li"
                                             select node;

            TraverseNodes(tocNodes, pWikipage, oWiki, path);
            AdjustWikiLinks();
            DeleteFiles(path);
        }

        private OmnicellWikiPage CreateNewWikiPage(OmnicellWiki oWiki, OmnicellWikiPage pWikipage, string path, string nodePath, string directory, int count)
        {
            HtmlDocument pageDoc = new HtmlDocument();
            int index = path.IndexOf("#");

            if (index != -1)
            {
                path = path.Remove(index);
            }
            iCount = 0;
            int linkCount = 0;
            pageDoc.Load(path, Encoding.UTF8);
            string description = String.Empty;
            string Id = String.Empty;
            HtmlNode bodyNode = pageDoc.DocumentNode.Descendants("body").FirstOrDefault();



            WikiPagesCreateOptions options = new WikiPagesCreateOptions();

            HtmlNode title = pageDoc.DocumentNode.SelectSingleNode("//title");
            HtmlNode sClass = bodyNode.SelectSingleNode("//p[@class='shortdesc']");


            if (bodyNode.SelectSingleNode("//table[@class='table']") != null)
            {
                List<HtmlNode> table = bodyNode.SelectNodes("//table[@class='table']").ToList();
                foreach (HtmlNode cNode in table)
                {
                    List<HtmlNode> trs = cNode.SelectNodes("//tr").ToList();
                    List<HtmlNode> tds = cNode.SelectNodes("//td").ToList();

                    foreach (HtmlNode tr in trs)
                    {
                        tr.Attributes.Remove("width");
                    }

                    foreach (HtmlNode td in tds)
                    {
                        td.Attributes.Remove("width");
                    }
                }
            }



            string wikiPageName = title.InnerHtml.Replace("&amp;", "&");
            string contentId = RetrieveDocId(path);
            documentIds.Add(contentId);
            IEnumerable<HtmlNode> collection = pageDoc.DocumentNode.Descendants("meta").Where(a => a.Attributes.Contains("name"));
            foreach (var meta in collection)
            {
                string nodeName = meta.Attributes["name"].Value;

                switch (nodeName)
                {
                    case "description": description = meta.Attributes["content"].Value; break;
                    case "DC.Identifier": Id = meta.Attributes["content"].Value; break;
                }

            }

            HtmlNode body = ProcessImages(bodyNode, oWiki.Id, directory);

            body.InnerHtml = body.InnerHtml.Replace("&amp;", "&");

            string tagId = nodePath;
            int indexHtml = nodePath.IndexOf("#");


            if (indexHtml != -1)
            {
                tagId = nodePath.Remove(indexHtml);
            }

            tagId = tagId.Replace(".html", "");

            // call method to replace links with telligent wiki links
            body = ProcessLinks(body);

            int pWikiId = (int)oWiki.Id;
            string docIdTag = "DocGUID:" + Id;
            string docTags = docIdTag;
            options.ParentPageId = pWikipage.Id;
            options.Body = body.InnerHtml;
            options.Tags = docTags;
            var wikiPage = PublicApi.WikiPages.Create(pWikiId, wikiPageName, options);
            OmnicellWikiPage nWikiPage = null;

            if (wikiPage != null)
            {
                nWikiPage = new OmnicellWikiPage(wikiPage);
                OmnicellWikiPageMetadata nMeta = new OmnicellWikiPageMetadata(oWiki.wikiMeta);

                if (!string.IsNullOrEmpty(contentId))
                    nMeta.contentId = "DocGUID:" + Id;

                nMeta.WikiPageId = (int)nWikiPage.Id;
                nMeta.ParentPageId = (int)pWikipage.Id;
                nMeta.PageKey = nWikiPage.PageKey;
                nMeta.OrderNumber = ++count;
                nMeta.ShortDesc = description;

                WikiPageExtensions.SavePageMetaData(nMeta);
                nWikiPage.metaData = nMeta;

                if (iCount > linkCount)
                {
                    interLinks.Add(nWikiPage);
                }
                linkCount = iCount;
            }

            return nWikiPage;
        }

        private OmnicellWikiPage UpdateWikiPage(OmnicellWiki oWiki, OmnicellWikiPage pWikipage, string path, string nodePath, string directory, int wikiPageId, int count)
        {
            string tagId = String.Empty;
            WikiPagesGetOptions gOptions = new WikiPagesGetOptions();
            gOptions.WikiId = oWiki.Id;
            gOptions.Id = wikiPageId;
            OmnicellWikiPage nWikiPage = null;
            iCount = 0;
            int linkCount = 0;
            try
            {
                var wikiPage = PublicApi.WikiPages.Get(gOptions);

                if (wikiPage != null)
                {
                    WikiPagesUpdateOptions uOptions = new WikiPagesUpdateOptions();

                    HtmlDocument pageDoc = new HtmlDocument();
                    int index = path.IndexOf("#");

                    if (index != -1)
                    {
                        path = path.Remove(index);
                    }

                    pageDoc.Load(path, Encoding.UTF8);
                    string description = String.Empty;
                    string Id = String.Empty;
                    HtmlNode bodyNode = pageDoc.DocumentNode.Descendants("body").FirstOrDefault();

                    HtmlNode body = ProcessImages(bodyNode, oWiki.Id, directory);
                    body.InnerHtml = body.InnerHtml.Replace("&amp;", "&");
                    HtmlNode title = pageDoc.DocumentNode.SelectSingleNode("//title");
                    string wikiPageName = title.InnerHtml.Replace("&amp;", "&");
                    HtmlNode sClass = body.SelectSingleNode("//p[@class='shortdesc']");

                    if (bodyNode.SelectSingleNode("//table[@class='table']") != null)
                    {
                        List<HtmlNode> table = bodyNode.SelectNodes("//table[@class='table']").ToList();
                        foreach (HtmlNode cNode in table)
                        {
                            List<HtmlNode> trs = cNode.SelectNodes("//tr").ToList();
                            List<HtmlNode> tds = cNode.SelectNodes("//td").ToList();

                            foreach (HtmlNode tr in trs)
                            {
                                tr.Attributes.Remove("width");
                            }

                            foreach (HtmlNode td in tds)
                            {
                                td.Attributes.Remove("width");
                            }
                        }
                    }

                   
                    IEnumerable<HtmlNode> collection = pageDoc.DocumentNode.Descendants("meta").Where(a => a.Attributes.Contains("name"));
                    foreach (var meta in collection)
                    {
                        string nodeName = meta.Attributes["name"].Value;

                        switch (nodeName)
                        {
                            case "description": description = meta.Attributes["content"].Value; break;
                            case "DC.Identifier": Id = meta.Attributes["content"].Value; break;
                        }

                    }

                    tagId = nodePath;
                    int indexHtml = nodePath.IndexOf("#");


                    if (indexHtml != -1)
                    {
                        tagId = nodePath.Remove(indexHtml);
                    }

                    tagId = tagId.Replace(".html", "");

                    body = ProcessLinks(body);

                    int pWikiId = Convert.ToInt32(oWiki.Id);
                    string docIdTag = "DocGUID:" + Id;
                    string docTags = docIdTag;
                    uOptions.ParentPageId = pWikipage.Id;
                    uOptions.Body = body.InnerHtml;
                    uOptions.WikiId = oWiki.Id;
                    uOptions.Tags = docTags;
                    uOptions.Title = wikiPageName;

                    var wikiUpdatePage = PublicApi.WikiPages.Update(wikiPageId, uOptions);

                    nWikiPage = new OmnicellWikiPage(wikiUpdatePage);
                    OmnicellWikiPageMetadata nMeta = new OmnicellWikiPageMetadata(oWiki.wikiMeta);
                    string mContentId = nWikiPage.Tags.FirstOrDefault(s => s.StartsWith("DocGUID:"));


                    nMeta.contentId = "DocGUID:" + Id;

                    nMeta.WikiPageId = (int)nWikiPage.Id;
                    nMeta.ParentPageId = (int)pWikipage.Id;
                    nMeta.PageKey = nWikiPage.PageKey;
                    nMeta.OrderNumber = ++count;
                    nMeta.ShortDesc = description;

                    WikiPageExtensions.SavePageMetaData(nMeta);
                    nWikiPage.metaData = nMeta;

                    if (iCount > linkCount)
                    {
                        interLinks.Add(nWikiPage);
                    }

                    linkCount = iCount;
                }
            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error updating wiki page with Id: {0}, and contentId: {1}", wikiPageId, tagId), ex).Log();
            }

            return nWikiPage;

        }

        private void EditWikiPages(OmnicellWiki oWiki, OmnicellWikiPage pWikipage, string path)
        {
            HtmlDocument tDoc = new HtmlDocument();
            string tocLink = path + Path.DirectorySeparatorChar + "toc.html";
            tDoc.Load(tocLink, Encoding.UTF8);
            HtmlNode bULTag = tDoc.DocumentNode.Descendants("ul").FirstOrDefault();

            IEnumerable<HtmlNode> tocNodes = from node in bULTag.ChildNodes
                                             where node.Name == "li"
                                             select node;

            TraverseUpdateNodes(tocNodes, pWikipage, oWiki, path);
            AdjustWikiLinks();
            DeleteFiles(path);
        }

        private void DeleteWikiPages(int wikiId, string tocLink)
        {
            int pageSize = 5000;
            List<string> contentIds = new List<string>();
            WikiPagesListOptions pOptions = new WikiPagesListOptions();
            WikiPagesUpdateOptions uOptions = new WikiPagesUpdateOptions();

            pOptions.PageSize = pageSize;
            pOptions.IncludeDisabledPages = true;
            var children = PublicApi.WikiPages.List(wikiId, pOptions).ToList();
            var db = new OmnicellEntities();

            foreach (var child in children)
            {

                string docId = child.Tags.FirstOrDefault(s => s.StartsWith("DocGUID:"));
                docId = docId.Replace("DocGUID:", "");

                if (!documentIds.Any(a => a.Equals(docId)))
                {

                    uOptions.IsPublished = false;
                    int id = Convert.ToInt32(child.Id);
                    var page = PublicApi.WikiPages.Update(id, uOptions);

                    if (page != null)
                    {
                        var wikiPage = db.WikiPageMetaDatas.FirstOrDefault(x => x.WikiPageId == page.Id);
                        db.DeleteObject(wikiPage);
                        db.SaveChanges();
                    }


                }
                else
                {
                    uOptions.IsPublished = true;
                    int id = Convert.ToInt32(child.Id);
                    var page = PublicApi.WikiPages.Update(id, uOptions);
                }
            }

        }

        private OmnicellWikiMetadata PopulateMetadata(SharepointDITA.DitaDocMetaData propsDITA)
        {
            OmnicellWikiMetadata meta = new OmnicellWikiMetadata();
            SharepointDITA.ParseMetaData pmeta = new SharepointDITA.ParseMetaData();

            try
            {
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.DocumentType))
                {
                    int docType = pmeta.ParseDocumentId(propsDITA.CustomProperties.DocumentType);
                    meta.DocumentType = docType.ToString();
                }

                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.Audience))
                {
                    meta.Audiences = pmeta.TranslateAudienceTypes(propsDITA.CustomProperties.Audience);
                }

                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.Product))
                {
                    meta.Products = pmeta.TranslateProductsGroup(propsDITA.CustomProperties.Product);
                }

                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.ProductVersion))
                    meta.ProductVersion = propsDITA.CustomProperties.ProductVersion;
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.Interfaces))
                    meta.Interfaces = propsDITA.CustomProperties.Interfaces;
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.Features))
                    meta.Features = propsDITA.CustomProperties.Features;
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.RevNum))
                    meta.RevNum = propsDITA.CustomProperties.RevNum;
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.PartName))
                    meta.PartName = propsDITA.CustomProperties.PartName;




            }
            catch (Exception ex)
            {
                throw ex;
            }

            return meta;
        }

        private HtmlNode ProcessLinks(HtmlNode node)
        {
            List<HtmlNode> linkNodes = node.Descendants("a").Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Split(' ').Any(b => b.Equals("link") || b.Equals("xref"))).ToList();
            List<HtmlNode> dupNodes = new List<HtmlNode>();
            string linkText = String.Empty;

            foreach (HtmlNode cNode in linkNodes)
            {
                dupNodes.Add(cNode);
            }


            for (int i = dupNodes.Count - 1; i >= 0; i--)
            {
                string url = linkNodes[i].Attributes["href"].Value;
                if (url.Contains("http://") || url.Contains("https://"))
                {
                    if (url.Contains(_plgnSP.SPFullUrl))
                    {
                        if (!url.Contains("#"))
                        {
                            linkText = "<span>[[" + linkNodes[i].InnerHtml + "]]</span>";
                            var newNode = HtmlNode.CreateNode(linkText);
                            linkNodes[i].ParentNode.ReplaceChild(newNode, linkNodes[i]);
                            linkNodes.Remove(linkNodes[i]);
                        }
                        if (url.Contains("#"))
                        {
                            iCount++;
                        }
                    }
                }
                else
                {
                    linkText = "<span>[[" + linkNodes[i].InnerHtml + "]]</span>";
                    var newNode = HtmlNode.CreateNode(linkText);
                    linkNodes[i].ParentNode.ReplaceChild(newNode, linkNodes[i]);
                    linkNodes.Remove(linkNodes[i]);
                }
            }

            HtmlNode wikiBody = ProcessListItemLinks(node);

            return wikiBody;

        }

        private HtmlNode ProcessListItemLinks(HtmlNode node)
        {
            List<HtmlNode> linkNodes = node.Descendants("li").Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Split(' ').Any(b => b.Equals("link"))).ToList();
            List<HtmlNode> dupNodes = new List<HtmlNode>();
            string linkText = String.Empty;

            foreach (HtmlNode cNode in linkNodes)
            {
                dupNodes.Add(cNode);
            }

            for (int i = dupNodes.Count - 1; i >= 0; i--)
            {
                HtmlNode linkNode = linkNodes[i].Descendants("a").FirstOrDefault();
                string url = linkNode.Attributes["href"].Value;

                if (url.Contains("http://") || url.Contains("https://"))
                {
                    if (url.Contains(_plgnSP.SPFullUrl))
                    {
                        if (!url.Contains("#"))
                        {
                            linkText = "<span>[[" + linkNode.InnerHtml + "]]</span>";
                            var newNode = HtmlNode.CreateNode(linkText);
                            linkNode.ParentNode.ReplaceChild(newNode, linkNode);
                            linkNodes.Remove(linkNodes[i]);
                        }

                        if (url.Contains("#"))
                        {
                            iCount++;
                        }
                    }
                }
                else
                {
                    linkText = "<span>[[" + linkNode.InnerHtml + "]]</span>";
                    var newNode = HtmlNode.CreateNode(linkText);
                    linkNode.ParentNode.ReplaceChild(newNode, linkNode);
                    linkNodes.Remove(linkNodes[i]);
                }
            }


            return node;
        }

        public HtmlNode ProcessImages(HtmlNode node, int? wikiId, string directory)
        {
            List<HtmlNode> images = node.Descendants("img").ToList();
            List<HtmlNode> dupNodes = new List<HtmlNode>();
            string fileName = String.Empty;

            foreach (HtmlNode image in images)
            {
                dupNodes.Add(image);
            }

            for (int i = dupNodes.Count - 1; i >= 0; i--)
            {
                fileName = images[i].Attributes["src"].Value;
                string url = directory + Path.DirectorySeparatorChar + images[i].Attributes["src"].Value;


                byte[] imageByte = WikiPageExtensions.ImageToByteArray(url);
                int wikiFileId = (int)wikiId;
                var wikiFile = PublicApi.WikiFiles.Create(wikiFileId, fileName, imageByte);
                string src = wikiFile.FileUrl.Replace("/cfs-file.ashx/", "/resized-image.ashx/__size/300x0/");

                images[i].SetAttributeValue("src", src);

                string linkText = String.Format("<a href='{0}' target='_blank'>{1}</a>", wikiFile.FileUrl, images[i].OuterHtml);
                var newNode = HtmlNode.CreateNode(linkText);
                images[i].ParentNode.ReplaceChild(newNode, images[i]);
                images.Remove(images[i]);
            }

            return node;
        }

        public bool BatchDelete()
        {
            bool boolReturn = true;
            try
            {
                var db = new OmnicellEntities();
                var dbEntity = db.Omnicell_WikiMetaData2.ToList();
                // get the list of libraries and mediagalleries
                string[] arrMGs = GetMediaGalleryList(_plgnSP.DLMGMaps);
                string[] arrLibs = GetSPLibraryList(_plgnSP.DLMGMaps);
                // get the list of documents in the MG and the Lib
                SPExtv1.PagedList<SPApi.Document> spdoclist = new SPExtv1.PagedList<SPApi.Document>();

                for (int i = 0; i < arrLibs.Length; i++)
                {
                    spdoclist = GetSharepointLibrary(arrMGs[i].ToString());

                    foreach (var wik in dbEntity)
                    {
                        if (spdoclist.Any(x => x.ContentId.CompareTo(wik.ContentId) == 0))
                        {

                        }
                        else
                        {
                            int wikId = (int)wik.WikiId;
                            var result = PublicApi.Wikis.Delete(wikId);
                            var wikiPages = db.WikiPageMetaDatas.Where(x => x.WikiId == wikId);

                            foreach (var page in wikiPages)
                            {

                                db.DeleteObject(page);
                            }

                            db.SaveChanges();
                            db.DeleteObject(wik);
                            db.SaveChanges();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                boolReturn = false;
                new CSException(CSExceptionType.UnknownError, string.Format("Error deleting wikis"), ex).Log();
            }
            return boolReturn;
        }
        public bool BatchCreate()
        {
            bool boolReturn = true;
            try
            {
                // get the list of libraries and mediagalleries
                string[] arrMGs = GetMediaGalleryList(_plgnSP.DLMGMaps);

                // get the list of documents in the MG and the Lib
                SPExtv1.PagedList<SPApi.Document> spdoclist = new SPExtv1.PagedList<SPApi.Document>();

                documentIds.Clear();
                for (int i = 0; i < arrMGs.Length; i++)
                {
                    spdoclist = GetSharepointLibrary(arrMGs[i].ToString());

                    foreach (SPApi.Document doc in spdoclist)
                    {
                        interLinks.Clear();
                        iCount = 0;
                        Telligent.Evolution.Components.User admUser = Telligent.Evolution.Users.GetUser("admin");
                        CreateUpdateWiki(doc);
                    }
                }
            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error creating wikis"), ex).Log();
            }
            return boolReturn;
        }

        private void CreateOrUpdateWiki(SPApi.Document zDoc, string key, string filePath, string docName, SPExtv1.PagedList<SPApi.Document> spdoclist, SPApi.Document doc)
        {

            string[] arrGroupId = GetSPLibraryList(_plgnSP.MoxiLibraryMap);
            string targetGroup = String.Empty;
            string groupMetaTag = "Target Group";
            string groupName = String.Empty;
            int groupId = 0;
            int wikiGroupId = 0;
            string wKey = key;
            string strSharepointRootURL = _plgnSP.SPFullUrl;
            string directory = String.Empty;
            string tocLink = filePath + Path.DirectorySeparatorChar + "toc.html";
            SharepointDITA.DitaDocMetaData smdt = null;
            MoxiPDFExtensions mpf = new MoxiPDFExtensions();

            try
            {

                // will this retrieve the Target Group Meta Tag?
                groupName = zDoc.MetaInfo.FirstOrDefault(x => x.Equals(groupMetaTag)).ToString();


                groupId = GetGroupID(_plgnSP.MoxiLibraryMap, groupName);

                if (groupId > 0)
                {

                    TEntities.Wiki toWiki = TApi.PublicApi.Wikis.Get(new TApi.WikisGetOptions { GroupId = groupId, Key = wKey });

                    if (toWiki != null)
                    {
                        DateTime pDate = Convert.ToDateTime(toWiki.LastModifiedUtcDate);
                        int currentWikiId = (int)toWiki.Id;
                        OmnicellWiki currentWiki = new OmnicellWiki(toWiki);

                        if (zDoc.Modified > pDate)
                        {
                            // add code to retrieve the XHTML zip stream
                            ZipInputStream strmzipfile = GetRemoteZipStream(strSharepointRootURL.ToString() + zDoc.Path, _plgnSP.SPDomain, _plgnSP.SPLogin, _plgnSP.SPPassword);
                            directory = ExtractFiles(strmzipfile);

                            // code to extract metadata
                            HtmlDocument tdoc = new HtmlDocument();
                            tdoc.Load(tocLink, Encoding.UTF8);
                            smdt = ExtractMetaDataFromTOCFile(tdoc);

                            // code to edit wiki
                            wikiGroupId = Convert.ToInt32(toWiki.Group.Id);
                            int wikiId = Convert.ToInt32(toWiki.Id);
                            EditWiki(wikiGroupId, filePath, wikiId, key, zDoc.ContentId, smdt);
                        }

                    }
                    else
                    {
                        // code to retrieve the XHTML zip stream
                        ZipInputStream strmzipfile = GetRemoteZipStream(strSharepointRootURL.ToString() + zDoc.Path, _plgnSP.SPDomain, _plgnSP.SPLogin, _plgnSP.SPPassword);
                        directory = ExtractFiles(strmzipfile);

                        // code to extract metadata
                        HtmlDocument tdoc = new HtmlDocument();
                        tdoc.Load(tocLink, Encoding.UTF8);
                        smdt = ExtractMetaDataFromTOCFile(tdoc);

                        // code to create a new wiki
                        CreateNewWiki(groupId, filePath, key, zDoc.ContentId, smdt);
                    }

                    // Code to test for pdf creation 
                    // Change the extension for the XHTML file to .pdf
                    string pdfDocName = docName.ToLower().Replace(".pdf", " - DITA Map XHTML.zip").ToString();
                    SPApi.Document pdfDoc = null;

                    foreach (SPApi.Document document in spdoclist)
                    {
                        if (document.Name.ToLower() == pdfDocName.ToLower())
                        {
                            pdfDoc = document;
                            break;
                        }
                    }
                    if (pdfDoc != null)
                    {

                        if (pdfDoc.Path != "" && doc.Path != "")
                        {
                            string wikiGuid = zDoc.ContentId.ToString();
                            // code to call pdf create and update events smdt pdfDoc?
                            mpf.CreateUpdateDocument(doc, smdt, wikiGuid);

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error processing document with key: {0}, and contentId: {1}", key, zDoc.ContentId), ex).Log();
            }
            finally
            {
                if(!String.IsNullOrEmpty(directory))
                {
                    DeleteFiles(directory);
                }
                
            }

        }

        // hopefully this will solve the recursive issue
        private void TraverseNodes(IEnumerable<HtmlNode> nodes, OmnicellWikiPage pWikipage, OmnicellWiki oWiki, string path)
        {
            OmnicellWikiPage nWikiPage = null;
            int count = 0;
            // int interCopy = 0;
            foreach (HtmlNode child in nodes)
            {
                HtmlNode aNode = child.Descendants("a").FirstOrDefault();
                HtmlNode bnode = child.FirstChild;

                string nodePath = string.Empty;


                if (bnode.Name == "a")
                {
                    nodePath = path + Path.DirectorySeparatorChar + aNode.Attributes["href"].Value;
                    string refPage = aNode.Attributes["href"].Value;

                    nWikiPage = CreateNewWikiPage(oWiki, pWikipage, nodePath, refPage, path, count);
                    ++count;
                    oWiki.wikiPages.Add(nWikiPage);
                    pWikipage.wikiPages.Add(nWikiPage);

                }
                else
                {
                    int wIndex = child.InnerHtml.IndexOf("<");
                    string mainText = String.Empty;
                    string title = String.Empty;
                    string description = String.Empty;
                    string pKey = String.Empty;
                    if (wIndex != -1)
                    {
                        mainText = child.InnerHtml.Remove(wIndex);
                        title = mainText.Replace("&amp;", "&");
                        description = "";
                        pKey = Regex.Replace(mainText, @"\s+", "");
                    }
                    else
                    {
                        pKey = Regex.Replace(child.InnerText, @"\s+", "");
                        title = child.InnerText.Replace("&amp;", "&");
                        description = "";
                    }

                    int pWikiId = Convert.ToInt32(oWiki.Id);
                    WikiPagesCreateOptions options = new WikiPagesCreateOptions();
                    options.ParentPageId = pWikipage.Id;
                    options.Body = title;
                    options.Tags = "DocGUID:" + pKey;
                    var wikiPage = PublicApi.WikiPages.Create(pWikiId, title, options);


                    nWikiPage = new OmnicellWikiPage(wikiPage);
                    OmnicellWikiPageMetadata nMeta = new OmnicellWikiPageMetadata(oWiki.wikiMeta);
                    nMeta.contentId = pKey;
                    nMeta.WikiPageId = (int)nWikiPage.Id;
                    nMeta.ParentPageId = (int)pWikipage.Id;
                    nMeta.PageKey = nWikiPage.PageKey;
                    nMeta.OrderNumber = ++count;
                    nMeta.ShortDesc = string.Empty;

                    WikiPageExtensions.SavePageMetaData(nMeta);

                    nWikiPage.metaData = nMeta;

                    oWiki.wikiPages.Add(nWikiPage);
                    pWikipage.wikiPages.Add(nWikiPage);
                }
                int index = -1;

                if (!string.IsNullOrEmpty(nodePath))
                {
                    index = nodePath.IndexOf("#");
                }


                //cycle through child nodes hopefully
                HtmlNode nestedUl = child.Descendants("ul").FirstOrDefault();


                if (nestedUl != null && index == -1)
                {
                    IEnumerable<HtmlNode> childList = from node in nestedUl.ChildNodes
                                                      where node.Name == "li"
                                                      select node;

                    if (childList.Any())
                    {
                        TraverseNodes(childList, nWikiPage, oWiki, path);
                    }
                }



            }
        }

        private void TraverseUpdateNodes(IEnumerable<HtmlNode> nodes, OmnicellWikiPage pWikipage, OmnicellWiki oWiki, string path)
        {
            OmnicellWikiPage nWikiPage = null;
            int count = 0;
            foreach (HtmlNode child in nodes)
            {
                HtmlNode aNode = child.Descendants("a").FirstOrDefault();
                HtmlNode bnode = child.FirstChild;
                string nodePath = string.Empty;


                if (bnode.Name == "a")
                {
                    nodePath = path + Path.DirectorySeparatorChar + aNode.Attributes["href"].Value;
                    string refPage = aNode.Attributes["href"].Value;


                    int tagIndex = refPage.IndexOf("#");
                    string contentId = RetrieveDocId(nodePath);

                    int wikiPageId = Convert.ToInt32(oWiki.Id);

                    int wikiPageExists = WikiPageExtensions.WikiPageExists(wikiPageId, contentId);

                    if (wikiPageExists > 0)
                    {
                        // wire in update method
                        nWikiPage = UpdateWikiPage(oWiki, pWikipage, nodePath, refPage, path, wikiPageExists, count);
                        ++count;

                        oWiki.wikiPages.Add(nWikiPage);
                        pWikipage.wikiPages.Add(nWikiPage);

                    }
                    else
                    {

                        nWikiPage = CreateNewWikiPage(oWiki, pWikipage, nodePath, refPage, path, count);
                        ++count;

                        oWiki.wikiPages.Add(nWikiPage);
                        pWikipage.wikiPages.Add(nWikiPage);
                    }

                }
                else
                {
                    int wIndex = child.InnerHtml.IndexOf("<");
                    string mainText = String.Empty;
                    string title = String.Empty;
                    string description = String.Empty;

                    if (wIndex != -1)
                    {
                        mainText = child.InnerHtml.Remove(wIndex);
                        title = mainText.Replace("&amp;", "&");
                        description = string.Empty;
                    }
                    else
                    {
                        title = child.InnerText.Replace("&amp;", "&");
                        description = string.Empty;
                    }

                    string contentId = Regex.Replace(title, @"\s+", "");
                    documentIds.Add(contentId);
                    int pWikiId = Convert.ToInt32(oWiki.Id);
                    int wikiPageId = WikiPageExtensions.WikiPageExists(pWikiId, contentId);

                    if (wikiPageId > 0)
                    {
                        WikiPagesUpdateOptions options = new WikiPagesUpdateOptions();
                        options.ParentPageId = pWikipage.Id;
                        options.Body = description;
                        options.Title = title;
                        options.WikiId = oWiki.Id;
                        var wikiPage = PublicApi.WikiPages.Update(wikiPageId, options);

                        nWikiPage = new OmnicellWikiPage(wikiPage);
                        OmnicellWikiPageMetadata oMeta = new OmnicellWikiPageMetadata(oWiki.wikiMeta);
                        oMeta.WikiPageId = (int)nWikiPage.Id;
                        oMeta.ParentPageId = (int)pWikipage.Id;
                        oMeta.PageKey = nWikiPage.PageKey;
                        oMeta.OrderNumber = ++count;
                        oMeta.contentId = contentId;
                        oMeta.ShortDesc = string.Empty;

                        WikiPageExtensions.SavePageMetaData(oMeta);
                        nWikiPage.metaData = oMeta;
                        oWiki.wikiPages.Add(nWikiPage);
                        pWikipage.wikiPages.Add(nWikiPage);
                    }
                    else
                    {
                        WikiPagesCreateOptions options = new WikiPagesCreateOptions();
                        options.ParentPageId = pWikipage.Id;
                        options.Body = description;
                        options.Tags = "DocGUID:" + contentId;
                        var wikiPage = PublicApi.WikiPages.Create(pWikiId, title, options);

                        nWikiPage = new OmnicellWikiPage(wikiPage);
                        OmnicellWikiPageMetadata oMeta = new OmnicellWikiPageMetadata(oWiki.wikiMeta);
                        oMeta.WikiPageId = (int)nWikiPage.Id;
                        oMeta.ParentPageId = (int)pWikipage.Id;
                        oMeta.PageKey = nWikiPage.PageKey;
                        oMeta.OrderNumber = ++count;
                        oMeta.contentId = contentId;
                        oMeta.ShortDesc = string.Empty;

                        WikiPageExtensions.SavePageMetaData(oMeta);
                        nWikiPage.metaData = oMeta;
                        oWiki.wikiPages.Add(nWikiPage);
                        pWikipage.wikiPages.Add(nWikiPage);

                    }
                }

                int index = -1;

                if (!string.IsNullOrEmpty(nodePath))
                {
                    index = nodePath.IndexOf("#");
                }


                //cycle through child nodes hopefully
                HtmlNode nestedUl = child.Descendants("ul").FirstOrDefault();


                if (nestedUl != null && index == -1)
                {
                    IEnumerable<HtmlNode> childList = from node in nestedUl.ChildNodes
                                                      where node.Name == "li"
                                                      select node;
                    if (childList.Any())
                    {
                        TraverseUpdateNodes(childList, nWikiPage, oWiki, path);
                    }
                }

            }
        }

        private void AdjustWikiLinks()
        {
            foreach (OmnicellWikiPage page in interLinks)
            {
                WikiPagesGetOptions op = new WikiPagesGetOptions();
                op.Id = page.Id;

                var wiki = PublicApi.WikiPages.Get(op);

                if (wiki != null)
                {
                    string body = wiki.Body();
                    string html = "<div>" + body + "</div>";
                    // Html Agility Code
                    HtmlDocument doc = new HtmlDocument();

                    doc.LoadHtml(html);

                    HtmlNode main = doc.DocumentNode.SelectSingleNode("//div");

                    ProcessInlineLinks(ref main);
                    ProcessListItemLinks(ref main);


                    WikiPagesUpdateOptions up = new WikiPagesUpdateOptions();
                    up.Body = main.InnerHtml;
                    int pageId = (int)page.Id;
                    var wikiUpdate = PublicApi.WikiPages.Update(pageId, up);
                }



            }
        }

        private void ProcessInlineLinks(ref HtmlNode node)
        {
            List<HtmlNode> linkNodes = node.Descendants("a").Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Split(' ').Any(b => b.Equals("link") || b.Equals("xref"))).ToList();
            List<HtmlNode> dupNodes = new List<HtmlNode>();
            string linkText = String.Empty;
            string[] linkHash;

            foreach (HtmlNode cNode in linkNodes)
            {
                dupNodes.Add(cNode);
            }


            for (int i = dupNodes.Count - 1; i >= 0; i--)
            {
                string url = linkNodes[i].Attributes["href"].Value;
                if (url.Contains("http://") || url.Contains("https://"))
                {
                    if (url.Contains(_plgnSP.SPFullUrl))
                    {
                        if (url.Contains("#"))
                        {
                            linkHash = url.Split('#');
                            //char[] endChar = { '_' };
                            string id = "DocGUID:" + linkHash[1];

                            using (var db = new OmnicellEntities())
                            {
                                WikiPageMetaData dbEntity = null;

                                dbEntity = db.WikiPageMetaDatas.FirstOrDefault(x => id.Contains(x.ContentId));

                                if (dbEntity != null)
                                {
                                    int wikiPageId = (int)dbEntity.WikiPageId;
                                    OmnicellWikiPage page = new OmnicellWikiPage(wikiPageId);

                                    linkText = "<span>[[" + page.Title + "|" + linkNodes[i].InnerHtml + "]]</span>";
                                    var newNode = HtmlNode.CreateNode(linkText);
                                    linkNodes[i].ParentNode.ReplaceChild(newNode, linkNodes[i]);
                                    linkNodes.Remove(linkNodes[i]);
                                }
                            }

                        }
                    }
                }
            }
        }

        private void ProcessListItemLinks(ref HtmlNode node)
        {
            List<HtmlNode> linkNodes = node.Descendants("li").Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Split(' ').Any(b => b.Equals("link"))).ToList();
            List<HtmlNode> dupNodes = new List<HtmlNode>();
            string linkText = String.Empty;
            string[] linkHash;

            foreach (HtmlNode cNode in linkNodes)
            {
                dupNodes.Add(cNode);
            }

            for (int i = dupNodes.Count - 1; i >= 0; i--)
            {
                HtmlNode linkNode = linkNodes[i].Descendants("a").FirstOrDefault();
                string url = linkNode.Attributes["href"].Value;

                if (url.Contains("http://") || url.Contains("https://"))
                {
                    if (url.Contains(_plgnSP.SPFullUrl))
                    {
                        if (url.Contains("#"))
                        {
                            linkHash = url.Split('#');

                            string id = "DocGUID:" + linkHash[1];

                            using (var db = new OmnicellEntities())
                            {
                                WikiPageMetaData dbEntity = null;

                                dbEntity = db.WikiPageMetaDatas.FirstOrDefault(x => id.Contains(x.ContentId));

                                if (dbEntity != null)
                                {
                                    int wikiPageId = (int)dbEntity.WikiPageId;
                                    OmnicellWikiPage page = new OmnicellWikiPage(wikiPageId);

                                    linkText = "<span>[[" + page.Title + "|" + linkNodes[i].InnerHtml + "]]</span>";
                                    var newNode = HtmlNode.CreateNode(linkText);
                                    linkNodes[i].ParentNode.ReplaceChild(newNode, linkNodes[i]);
                                    linkNodes.Remove(linkNodes[i]);
                                }
                            }

                        }
                    }
                }
            }
        }

        private string RetrieveDocId(string docLink)
        {
            int index = docLink.IndexOf("#");

            if (index != -1)
            {
                docLink = docLink.Remove(index);
            }
            HtmlDocument pageDoc = new HtmlDocument();
            pageDoc.Load(docLink, Encoding.UTF8);
            string description = String.Empty;
            string Id = String.Empty;

            IEnumerable<HtmlNode> collection = pageDoc.DocumentNode.Descendants("meta").Where(a => a.Attributes.Contains("name"));
            foreach (var meta in collection)
            {
                string nodeName = meta.Attributes["name"].Value;

                switch (nodeName)
                {

                    case "DC.Identifier": Id = meta.Attributes["content"].Value; break;
                }

            }
            documentIds.Add(Id);
            return Id;

        }

        private int GetGroupID(string strGroupMap, string strGroup)
        {
            int MGId = 0;
            try
            {
                // split the string and find the match.
                var keyValuePairs = strGroupMap.Split(';')
                    .Select(x => x.Split('='))
                    .ToDictionary(x => x.First(), x => x.Last());


                if (keyValuePairs.ContainsKey(strGroup.ToUpper()))
                {
                    MGId = Convert.ToInt32(keyValuePairs[strGroup.ToUpper()].ToString());
                }

            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error retrieving media gallery id with target group: {0}", strGroupMap), ex).Log();
            }
            return MGId;
        }

        // methods to save the files in the zip stream to a temp folder
        private string ExtractFiles(ZipInputStream ziStream)
        {
            // this should lead to the job scheduler folder
            string directory = Environment.CurrentDirectory + Path.DirectorySeparatorChar + "WikiTemp";
            ZipEntry theEntry;

            try
            {
                // create folder destination if it does not exists
                if (Directory.Exists(directory) == false)
                {
                    Directory.CreateDirectory(directory);
                }

                while ((theEntry = ziStream.GetNextEntry()) != null)
                {
                    string fileName = Path.GetFileName(theEntry.Name);
                    string fileDestination = directory + Path.DirectorySeparatorChar + fileName;
                    SaveFileToDisk(ziStream, fileDestination);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ziStream.Close();
                ziStream.Dispose();
            }

            return directory;
        }

        private void SaveFileToDisk(ZipInputStream ziStream, string destination)
        {
            FileStream wstream = null;

            try
            {
                wstream = System.IO.File.Create(destination);

                const int block = 2048; // number of bytes to decompress for each read from the source
                byte[] data = new byte[block];

                while (true)
                {
                    int size = ziStream.Read(data, 0, data.Length);

                    if (size > 0)
                    {
                        wstream.Write(data, 0, size);
                    }

                    else
                    {
                        break;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (wstream != null)
                    wstream.Close();
            }
        }

        private void DeleteFiles(string path)
        {
            Array.ForEach(Directory.GetFiles(path), System.IO.File.Delete);
        }

        private string[] GetMediaGalleryList(string strMap)
        {
            string[] strReturn;
            var keyValuePairs = strMap.Split(';')
                .Select(x => x.Split('='))
                .ToDictionary(x => x.First(), x => x.Last());

            strReturn = keyValuePairs.Keys.ToArray();

            return strReturn;
        }

        private string[] GetSPLibraryList(string strMap)
        {
            string[] strReturn;
            var keyValuePairs = strMap.Split(';')
                .Select(x => x.Split('='))
                .ToDictionary(x => x.First(), x => x.Last());

            strReturn = keyValuePairs.Values.ToArray();

            return strReturn;
        }

        public string GetFileNameFromUrl(string url)
        {
            try
            {
                string[] fileUrl = url.Split('/');
                return fileUrl[fileUrl.Count() - 1];
            }
            catch (Exception)
            {

            }

            return String.Empty;
        }

        private static object _runAsUserLock = new object();
        public void RunAsUser(Action a, Telligent.Evolution.Components.IExecutionContext context, Telligent.Evolution.Components.User u)
        {
            lock (_runAsUserLock)
            {
                var originalUser = context.User;
                try
                {
                    context.User = u;
                    a();
                }
                finally
                {
                    context.User = originalUser;
                }
            }
        }
    }
}

