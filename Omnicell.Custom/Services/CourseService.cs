﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Caching;
using Telligent.Common;
using Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.Api.Version1;
using V1Entities = Telligent.Evolution.Extensibility.Api.Entities.Version1;

using Omnicell.Data.Model;
using Omnicell.Custom.Components;

namespace Omnicell.Custom.Components
{
    public interface ICourseService
    {
        OmnicellCoursePost Get(int coursePostId);
        OmnicellCoursePost Save(int coursePostId, int mediaFormatId, string productIdList);
    }
    public class CourseService : ICourseService
    {
        private readonly ICacheService _cache = null;
        private OmnicellEntities _odb = null;

        public CourseService()
        {
            _cache = Telligent.Common.Services.Get<ICacheService>();
            _odb = new OmnicellEntities();
        }

        public OmnicellCoursePost Get(int coursePostId)
        {
            OmnicellCoursePost post = GetCoursePostFromCache(coursePostId);
            if (post == null)
            {
                V1Entities.Media media = PublicApi.Media.Get(coursePostId);
                if (media != null)
                {
                    post = new OmnicellCoursePost(media);
                    PutCoursePostInCache(post);
                }
            }
            return post;
        }
        /// <summary>
        /// Updates values in omnicell custom database
        /// </summary>
        /// <param name="id">ID of the course post</param>
        /// <param name="mediaFormatId">Media Format Id for the course post</param>
        /// <param name="productList">Comma separated list of product group ids </param>
        /// <returns></returns>
        public OmnicellCoursePost Save(int coursePostId, int mediaFormatId, string productIdList)
        {
            OmnicellCoursePost returnPost = null;

            if (coursePostId > 0)
            {
                RemoveCoursePostFromCache(coursePostId);

                returnPost = Get(coursePostId);
            }
            return returnPost;
        }
        

        #region Caching
        
        private string GetCoursePostCacheKey(int coursePostId)
        {
            return string.Format("COURSE-POST:{0}", coursePostId);
        }
        private void PutCoursePostInCache(OmnicellCoursePost post)
        {
            _cache.Put(GetCoursePostCacheKey(post.Id.Value), post, CacheScope.All);
        }

        private OmnicellCoursePost GetCoursePostFromCache(int coursePostId)
        {
            return _cache.Get(GetCoursePostCacheKey(coursePostId), CacheScope.All) as OmnicellCoursePost;
        }
        private void RemoveCoursePostFromCache(int coursePostId)
        {
            _cache.Remove(GetCoursePostCacheKey(coursePostId), CacheScope.All);
        }

        #endregion Caching
    }
}
