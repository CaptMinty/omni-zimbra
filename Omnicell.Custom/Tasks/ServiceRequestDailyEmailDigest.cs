﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Common;
using TComp = Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.Version1;
using Telligent.Evolution.Extensibility.Api.Version1;
using Telligent.Evolution.Extensibility.Api.Entities.Version1;
using Telligent.Jobs;
using Telligent.Evolution.Extensibility.Jobs.Version1;

using Omnicell.Custom.Components;
using System.Net;
using System.Net.Mail;
using System.Collections.Specialized;
using Omnicell.Data.Model;

namespace Omnicell.Custom.Tasks
{
    public class ServiceRequestDailyEmailDigest : IRecurringEvolutionJobPlugin
    {
        public void Execute(JobExecutionContext context)
        {
            var db = new OmnicellEntities();
            var tickets = db.Tickets.Where(x => x.Updated == DateTime.Now);

            foreach (var tick in tickets)
            {
                var subscribers = db.TicketSubscriptions.Where(x => x.TicketId == tick.Id || x.TicketId == null);
            }
        }
    }
}
