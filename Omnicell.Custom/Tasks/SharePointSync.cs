﻿using Omnicell.Custom.Components;
using Telligent.Jobs;
using Telligent.Evolution.Extensibility.Jobs.Version1;

namespace Omnicell.Custom
{
    public class SharePointSync : IRecurringEvolutionJobPlugin
    {
       
        public JobSchedule DefaultSchedule
        {
            get { return JobSchedule.EveryMinutes(5); }
        }

        public System.Guid JobTypeId
        {
            get { return new System.Guid("8651e704716b471382cb57e3dd012399"); }
        }

        public JobContext SupportedContext
        {
            get { return JobContext.Service; }
        }

        public string Description
        {
            get { return "A job that retrieves documents from a SharePoint library and creates wikis and pdf files from the documents."; }
        }

        public void Initialize()
        {
            
        }

        public string Name
        {
            get { return "SharePoint Sync"; }
        }

        public void Execute(JobData jobData)
        {
            // create a reference to the sharepoint class and call the delete method
            MoxiWikiStream wMoxi = new MoxiWikiStream();

            wMoxi.BatchDelete();
            wMoxi.BatchCreate();
        }
    }
}
