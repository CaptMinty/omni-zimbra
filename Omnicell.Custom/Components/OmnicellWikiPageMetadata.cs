﻿using System;
using System.Reflection;

namespace Omnicell.Custom.Components
{
    public class OmnicellWikiPageMetadata : OmnicellWikiMetadata
    {
        public string PageKey { get; set; }
        public int WikiPageId { get; set; }
        public int ParentPageId { get; set; }
        public int OrderNumber { get; set; }
        public string contentId { get; set; }
        public string FileName { get; set; }

        public OmnicellWikiPageMetadata()
        {

        }

        public OmnicellWikiPageMetadata(OmnicellWikiMetadata meta)
        {
            MapFromWikiMeta(meta);
        }

        private void MapFromWikiMeta(OmnicellWikiMetadata meta)
        {
            PropertyInfo[] basePIs = typeof(OmnicellWikiMetadata).GetProperties();
            Type thisObjectType = this.GetType();

            foreach (PropertyInfo basePI in basePIs)
            {
                PropertyInfo newPI = thisObjectType.GetProperty(basePI.Name);
                if (newPI != null)
                {
                    try
                    {
                        newPI.SetValue(this, basePI.GetValue(meta, null), null);
                    }
                    catch { }
                }
            }
        }
    }
}
