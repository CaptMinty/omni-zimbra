﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using Telligent.Evolution.Extensibility.Api.Entities.Version1;
using TApi = Telligent.Evolution.Extensibility.Api.Version1;


namespace Omnicell.Custom.Components
{
    public class OmnicellMediaPost : Media
    {
        public OmnicellMediaPost() { }
        public OmnicellMediaPost(Guid contentId)
        {
            var media = TApi.PublicApi.Media.Get(contentId);
            if (media != null && media.ContentId == contentId)
                MapFromMedia(media);
        }
        public OmnicellMediaPost(int mediaId)
        {
            var media = TApi.PublicApi.Media.Get(mediaId);
            if (media != null && media.Id == mediaId)
                MapFromMedia(media);
        }
        public OmnicellMediaPost(Media media)
        {
            MapFromMedia(media);
        }

        public string RawBody { get; set; }
        public string PlainBody { get; set; }

        public Dictionary<int, string> GetValueDictionary(string key)
        {
            string valueString = GetValue<string>(key, string.Empty);
            return new Dictionary<int, string>().FromDelimitedString(valueString);
        }
        public T GetValue<T>(string key, T defaultValue)
        {
            T returnValue = defaultValue;
            ExtendedAttribute attr = this.ExtendedAttributes.Where(a => a.Key == key).FirstOrDefault();
            if (attr != null)
            {
                returnValue = (T)Convert.ChangeType(attr.Value, typeof(T));
            }
            return returnValue;
        }
        public string GetValue(string key)
        {
            return GetValue<string>(key, string.Empty);
        }
        public void SetValue(string key, object value)
        {
            ExtendedAttribute attr = this.ExtendedAttributes.Where(a => a.Key == key).FirstOrDefault();
            if (attr == null)
                this.ExtendedAttributes.Add(new ExtendedAttribute { Key = key, Value = value.ToString() });
            else
                attr.Value = value.ToString();
        }

        private void MapFromMedia(Media media)
        {
            PropertyInfo[] basePIs = typeof(Media).GetProperties();
            Type thisObjectType = this.GetType();

            foreach (PropertyInfo basePI in basePIs)
            {
                PropertyInfo newPI = thisObjectType.GetProperty(basePI.Name);
                if (newPI != null)
                {
                    try
                    {
                        newPI.SetValue(this, basePI.GetValue(media, null), null);
                    }
                    catch { }
                }
            }

            RawBody = media.Body("raw");
            PlainBody = media.Body();
        }
    }
}
