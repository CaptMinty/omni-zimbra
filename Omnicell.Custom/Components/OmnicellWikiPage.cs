﻿using System;
using System.Collections.Generic;
using System.Reflection;
using TApi = Telligent.Evolution.Extensibility.Api.Version1;
using TEpi = Telligent.Evolution.Extensibility.Api.Entities.Version1;


namespace Omnicell.Custom.Components
{
    public class OmnicellWikiPage : TEpi.WikiPage
    {
        public OmnicellWikiPageMetadata metaData;
        public List<OmnicellWikiPage> wikiPages;

        public OmnicellWikiPage() { }

        public OmnicellWikiPage(Guid contentId)
        {
            metaData = new OmnicellWikiPageMetadata();
            wikiPages = new List<OmnicellWikiPage>();
            var wikiPage = TApi.PublicApi.WikiPages.Get(contentId);
            if(wikiPage != null)
                MapFromWikiPage(wikiPage);
        }

        public OmnicellWikiPage(int wikiPageId)
        {
            metaData = new OmnicellWikiPageMetadata();
            wikiPages = new List<OmnicellWikiPage>();
            var wikiPage = TApi.PublicApi.WikiPages.Get(new TApi.WikiPagesGetOptions { Id = wikiPageId });
            if(wikiPage != null)
                MapFromWikiPage(wikiPage);
        }

        public OmnicellWikiPage(TEpi.WikiPage wikiPage)
        {
            metaData = new OmnicellWikiPageMetadata();
            wikiPages = new List<OmnicellWikiPage>();
            MapFromWikiPage(wikiPage);
        }

        private void MapFromWikiPage(TEpi.WikiPage wikiPage)
        {
            PropertyInfo[] basePIs = typeof(TEpi.WikiPage).GetProperties();
            Type thisObjectType = this.GetType();

            foreach (PropertyInfo basePI in basePIs)
            {
                PropertyInfo newPI = thisObjectType.GetProperty(basePI.Name);
                if (newPI != null)
                {
                    try
                    {
                        if (newPI.CanWrite && newPI != null)
                        {
                            newPI.SetValue(this, basePI.GetValue(wikiPage, null), null);
                        }
                        
                    }
                    catch { }
                }
            }
        }
    }
}
