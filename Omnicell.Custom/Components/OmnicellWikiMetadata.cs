﻿using System;

namespace Omnicell.Custom.Components
{
    public class OmnicellWikiMetadata
    {
        public string Products { get; set; }
        public string ProductVersion { get; set; }
        public string Audiences { get; set; }
        public string Interfaces { get; set; }
        public string Features { get; set; }
        public string DocumentType { get; set; }
        public int GroupId { get; set; }
        public int WikiId { get; set; }
        public Guid DocumentId { get; set; }
        public string ShortDesc { get; set; }
        public string RevNum { get; set; }
        public string PartName { get; set; }
    }
}
