(function ($) {
    if (typeof $.omnicell === 'undefined') { $.omnicell = {}; }
    if (typeof $.omnicell.widgets === 'undefined') { $.omnicell.widgets = {}; }

    $.omnicell.widgets.trainingGalleryAdmin = {
        register : function (context) {
            
            $(context.submitButton).click(function(e){
                e.preventDefault();
                
                var $this = $(this);
                
                if(!$this.hasClass('disabled')) {
                    $this.addClass('disabled');
                    
                    var selGalleryId = $(context.galleryList).val();
                    if($(context.galleryList).val() > 0) {
                    
                        var selFormats = $(context.mediaFormatList + ' input:checked').map(function() { return $(this).val(); }).get().join(',');
                        var selTypes = $(context.courseTypeList + ' input:checked').map(function() { return $(this).val(); }).get().join(',');
                        
                        var data = {
                            galleryId: selGalleryId,    
                            mediaFormatFilters: selFormats,
                            courseTypeFilters: selTypes,
                            galleryType: $(context.galleryTypeList).val(),
                            showCourseInfo: $(context.showCourseInfo).is(':checked'),
                            courseDateType: $(context.courseDateTypeList).val()
                        }
                        
                        $.telligent.evolution.post({
                            url: context.gallerySaveUrl,
                            data: data,
                            success: function(response) {
                                alert('The settings were saved successfully.');
                                $(context.submitButton).removeClass('disabled');
                            }
                        });    
                    }
                } else {
                    alert('Please choose a gallery to edit');
                }
                
            });
            
            $(context.galleryList).change(function(e) {
                if($(this).val() > 0) {
                    window.location = "?tg=" + $(this).val();
                }
            });
        }
    }
    
})(jQuery);
