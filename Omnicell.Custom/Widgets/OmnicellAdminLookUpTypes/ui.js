(function ($) {
    var deleteLookupTypes = function (context, lookupTypeIds) {
		$.telligent.evolution.post({
			url : context.deleteUrl,
			data : {
				selectedLookupTypes : lookupTypeIds.join()
			},
			success : function (response) {
				window.location.reload();
			}
		});

	},
	saveLookupType = function (context, id, value, selectedLookupTypeId) {
		$.telligent.evolution.post({
			url : context.saveUrl,
			data : {
				Id : id,
				Value : value,
				SelectedEntityTypeId : selectedLookupTypeId
			},
			success : function (response) {
				window.location.reload();
			}
		});
	};
	var api = {
		register : function (context) {
			context.wrapperId = $(context.wrapperId);
			$(context.selectedEntityHtmlId).change(function (event) {
				event.preventDefault();

				var url = context.url;
				if (url.indexOf('?') > -1) {
					url = url + '&SelectedEntityTypeId=';
				} else {
					url = url + '?SelectedEntityTypeId=';
				}

				url = url + $(this).val();
				window.location.href = url;

				return false;
			});
			$('a.lookupTypeEdit', context.wrapperId).bind('click', function (e, data) {
				e.preventDefault();
				var id = $('input#Id', context.wrapperId);
				var value = $('input#Value', context.wrapperId);
				var tr = $(this).closest('tr');
				id.val(tr.attr('data-id'));
				value.val(tr.attr('data-value'));
				return false;
			});
			$('a#saveEntity', context.wrapperId).bind('click', function (e, data) {
				e.preventDefault();
				var id = $('input#Id', context.wrapperId).val();
				var value = $('input#Value', context.wrapperId).val();
				var selectedLookupTypeId = $('select' + context.selectedEntityHtmlId, context.wrapperId).val();

				if (!value || !selectedLookupTypeId || value.length == 0 || selectedLookupTypeId.length == 0) {
					alert('You must select a lookup Type and enter in a value before you can save changes to lookupTypes.');
				} else if (confirm(context.addLookupTypeConfirmMessage)) {
					saveLookupType(context, id, value, selectedLookupTypeId);
				}
				return false;
			});
			$('a#deleteEntities', context.wrapperId).bind('click', function (e, data) {
				e.preventDefault();
				var lookupTypes = new Array();
				$('input:checked', context.wrapperId).each(function () {
					lookupTypes.push($(this).val());
				});

				if (!lookupTypes || lookupTypes.length == 0) {
					alert('You must select one or more lookupTypes, before you can delete them from the list of lookupTypes.');
				} else if (confirm(context.deleteLookupTypesConfirmMessage)) {
					deleteLookupTypes(context, lookupTypes);
				}
				return false;
			});

		}
	};

	if (typeof $.widgets == 'undefined') {
		$.widgets = {};
	}
	$.widgets.adminLookupManager = api;

}(jQuery));
