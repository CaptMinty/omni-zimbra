(function ($) {
    var deleteFailedAccountAttempts = function(context, lookupTypeIds) {
            $.telligent.evolution.post({
                    url: context.deleteUrl,
                    data: {
                            selectedLookupTypes: lookupTypeIds.join()
                    },
                    success: function(response) {
    	          //window.location.href = window.location.href;
		          window.location.reload();
                    }
             });

        };
    var api = {
//    $.widgets.listFailedAccountAttemptsWidget= {
        register: function (context) {
			context.wrapperId = $(context.wrapperId);
			$('a#deleteFailedAccountAttempts', context.wrapperId).bind('click', function(e,data){
				e.preventDefault();
				var lookupTypes = new Array();
				$('input:checked', context.wrapperId).each(function() {
					lookupTypes.push($(this).val());
				});

				if(!lookupTypes || lookupTypes.length == 0) {
						alert('You must select one or more failed account items, before you can delete them from the list.');
				}
				else if(confirm(context.deleteConfirmMessage)){
					deleteFailedAccountAttempts (context, lookupTypes);
				}
				return false;
			});
        }
    };
    
    if (typeof $.widgets == 'undefined') { $.widgets = {}; }
    $.widgets.listFailedAccountAttemptsWidget = api;

}(jQuery));