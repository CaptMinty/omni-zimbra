﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Common;
using TComp = Telligent.Evolution.Components;
using Telligent.DynamicConfiguration.Components;
using Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.Api.Version1;
using Telligent.Evolution.Extensibility.Api.Entities.Version1;
using Telligent.Evolution.Extensibility.Version1;
using Telligent.Evolution.Extensibility.UI.Version1;

using Omnicell.Custom.Components;


namespace Omnicell.Custom
{
    public class OmnicellDocumentGalleryPlugin : IConfigurablePlugin, IScriptedContentFragmentExtension
    {
        #region IConfigurablePlugin Members

        IPluginConfiguration Configuration { get; set; }

        public Telligent.DynamicConfiguration.Components.PropertyGroup[] ConfigurationOptions
        {
            get
            {
                PropertyGroup[] groups = new[] { new PropertyGroup("option", "Options", 0) };

                Property productGroupIdProp = new Property("documentGroupId", "Document Group", PropertyType.Custom, 0, "Group=28");
                productGroupIdProp.DescriptionText = "Select the parent Techical Documentation group.";
                productGroupIdProp.ControlType = typeof(Telligent.Evolution.Controls.GroupSelectionList);
                groups[0].Properties.Add(productGroupIdProp);

                Property documentGalleryIdProp = new Property("documentGalleryId", "Document Gallery", PropertyType.Custom, 0, "");
                documentGalleryIdProp.DescriptionText = "Enter the name of the document gallery that contains the technical documentation.";
                documentGalleryIdProp.ControlType = typeof(Telligent.Evolution.MediaGalleries.Controls.MediaGallerySelectionControl);
                documentGalleryIdProp.Attributes.Add("maximumMediaGallerySelections", "1");
                documentGalleryIdProp.Attributes.Add("enableGroupSelection", "false");
                groups[0].Properties.Add(documentGalleryIdProp);

                return groups;
            }
        }
        public void Update(IPluginConfiguration configuration)
        {
            Configuration = configuration;
        }

        #endregion IConfigurablePlugin Members

        #region IScriptedContentFragmentExtension Members

        public void Initialize() { }
        public string Name
        {
            get { return "Omnicell Document Gallery Widget Methods"; }
        }
        public string Description
        {
            get { return "Enables scripted content fragments to use functionality for the Omnicell Document Gallery."; }
        }
        public object Extension
        {
            get { return new OmnicellDocumentGalleryWidgetExtension(Configuration); }
        }
        public string ExtensionName
        {
            get { return "omnicell_v1_documentgallery"; }
        }

        #endregion IScriptedContentFragmentExtension Members
    }
    public class OmnicellDocumentGalleryWidgetExtension
    {

        public int DocumentGroupId { get; private set; }
        public int TechnicalDocumentsGalleryId { get; private set; }

        public OmnicellDocumentGalleryWidgetExtension(IPluginConfiguration configuration)
        {
            ExtractConfiguration(configuration);
        }

        private void ExtractConfiguration(IPluginConfiguration configuration)
        {
            DocumentGroupId = GetValue(configuration.GetCustom("documentGroupId"), -1);
            TechnicalDocumentsGalleryId = GetValue(configuration.GetCustom("documentGalleryId"), -1);
        }
        private int GetValue(string pair, int defaultValue)
        {
            int returnValue = defaultValue;
            if (!string.IsNullOrEmpty(pair) && pair.Contains("="))
            {
                if (pair.Contains("&"))
                    pair = pair.Split("&".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[0];

                string[] segs = pair.Split("=".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                if (segs.Length > 1)
                    int.TryParse(segs[1], out returnValue);
            }
            return returnValue;
        }

    }
}
