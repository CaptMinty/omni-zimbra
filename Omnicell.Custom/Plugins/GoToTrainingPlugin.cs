﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Common;
using Telligent.DynamicConfiguration.Components;
using Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.Api.Version1;
using V1Entities = Telligent.Evolution.Extensibility.Api.Entities.Version1;
using Telligent.Evolution.Extensibility.Version1;
using Telligent.Evolution.Extensibility.UI.Version1;

using GTT = GoToTraining.Api.Components;

using Omnicell.Custom.Components;

namespace Omnicell.Custom
{
    public class GoToTrainingPlugin : IConfigurablePlugin, IScriptedContentFragmentExtension
    {

        #region IConfigurablePlugin Members

        IPluginConfiguration Configuration { get; set; }

        public void Initialize() { }
        public string Name
        {
            get { return "GoToTraining.com API Plugin"; }
        }
        public string Description
        {
            get { return "Provides access to GoToTraining.com api functionality"; }
        }
        public Telligent.DynamicConfiguration.Components.PropertyGroup[] ConfigurationOptions
        {
            get
            {
                PropertyGroup[] groups = new[] { new PropertyGroup("access", "API Access Options", 0), new PropertyGroup("tracking", "Attendence Tracking Options", 1) };

                Property gotoTrainingKeyProp = new Property("gttAccessToken", "GoToTraining Access Token", PropertyType.String, 0, "2dc9708997e3e008c7fb1a5faa5b1a67");
                gotoTrainingKeyProp.DescriptionText = "Enter the access token used to access the GoToTraining API";
                groups[0].Properties.Add(gotoTrainingKeyProp);

                Property gotoTrainingAccountKeyProp = new Property("gttAccountKey", "GoToTraining Account Token", PropertyType.String, 1, "2290222");
                gotoTrainingAccountKeyProp.DescriptionText = "Enter the account key used to access the GoToTraining API";
                groups[0].Properties.Add(gotoTrainingAccountKeyProp);

                Property gotoTrainingOrgKeyProp = new Property("gttOrganizerKey", "GoToTraining Organizer Token", PropertyType.String, 2, "2524788");
                gotoTrainingOrgKeyProp.DescriptionText = "Enter the organizer key used to access the GoToTraining API";
                groups[0].Properties.Add(gotoTrainingOrgKeyProp);

                Property gotoTrainingRestAPIUrl = new Property("gttRestAPIUrl", "Go To Training Rest API Url", PropertyType.String, 3, "http://dev.myomnicell.com");
                gotoTrainingRestAPIUrl.DescriptionText = "Enter the full url for the Rest API.";
                groups[0].Properties.Add(gotoTrainingRestAPIUrl);

                Property gotoTrainingRestAPIUser = new Property("gttRestAPIUser", "Go To Training Rest API User", PropertyType.String, 4, "admin");
                gotoTrainingRestAPIUser.DescriptionText = "Enter the user account name for the Rest API.";
                groups[0].Properties.Add(gotoTrainingRestAPIUser);

                Property gotoTrainingRestAPIKey = new Property("gttRestAPIKey", "Go To Training Rest API Key", PropertyType.String, 5, "n0s00srf8fjgivql6c");
                gotoTrainingRestAPIKey.DescriptionText = "Enter the user account API Key for the Rest API.";
                groups[0].Properties.Add(gotoTrainingRestAPIKey);

                Property gotoTrainingPreReqKey = new Property("gttPreReqKey", "Go To Training Prerequisite Title Code", PropertyType.String, 0, "OC001");
                gotoTrainingPreReqKey.DescriptionText = "Enter the Title Code for the prerequisite GoToTraining course.";
                groups[1].Properties.Add(gotoTrainingPreReqKey);

                Property gotoTrainingPreReqProfileFieldName = new Property("gttPreReqProfileFieldName", "Go To Training Prerequisite profile field", PropertyType.String, 1, "HasAttendedPreReq");
                gotoTrainingPreReqProfileFieldName.DescriptionText = "Enter the name of the profile field for the prerequisite GoToTraining course.";
                groups[1].Properties.Add(gotoTrainingPreReqProfileFieldName);

                Property gotoTrainingPreReqMinProp = new Property("gttPreRecCompletionMin", "Go To Training Compeletion Minutes", PropertyType.Int, 2, "8");
                gotoTrainingPreReqMinProp.DescriptionText = "Enter the number of minutes of the prerequisite GoToTraining course that the attendee must view to be marked as completed.";
                groups[1].Properties.Add(gotoTrainingPreReqMinProp);

                Property gotoTrainingSmtp = new Property("gttSmtp", "Go To Training Rest API User", PropertyType.String, 6, "localhost");
                gotoTrainingSmtp.DescriptionText = "Enter the smtp server for service request ticket digest.";
                groups[0].Properties.Add(gotoTrainingSmtp);

                return groups;
            }
        }
        public void Update(IPluginConfiguration configuration)
        {
            Configuration = configuration;

            AccessToken = configuration.GetString("gttAccessToken");
            AccountToken = configuration.GetString("gttAccountKey");
            OrganizerKey = configuration.GetString("gttOrganizerKey");
            PreRequisiteKey = configuration.GetString("gttPreReqKey");
            PreReqProfileFieldName = configuration.GetString("gttPreReqProfileFieldName");
            PreReqCompletionMinutes = configuration.GetInt("gttPreRecCompletionMin");
            RestAPIUrl = configuration.GetString("gttRestAPIUrl");
            RestAPIUser = configuration.GetString("gttRestAPIUser");
            RestAPIKey = configuration.GetString("gttRestAPIKey");
            Smtp = configuration.GetString("gttSmtp");

            EnsureProfileFieldExists(PreReqProfileFieldName);
        }

        public string AccessToken { get; private set; }
        public string AccountToken { get; private set; }
        public string OrganizerKey { get; private set; }
        public string PreRequisiteKey { get; private set; }
        public string PreReqProfileFieldName { get; private set; }
        public int PreReqCompletionMinutes { get; private set; }
        public string RestAPIUrl { get; private set; }
        public string RestAPIUser { get; private set; }
        public string RestAPIKey { get; private set; }
        public string Smtp { get; private set; }


        #endregion IConfigurablePlugin Members

        #region IScriptedContentFragmentExtension Members

        public object Extension
        {
            get { return new GoToTrainingAPIExtensionService(Configuration); }
        }
        public string ExtensionName
        {
            get { return "gototraining_v1_api"; }
        }

        #endregion IScriptedContentFragmentExtension Members

        private void EnsureProfileFieldExists(string fieldName)
        {
            IUserProfileService svcProfile = Telligent.Common.Services.Get<IUserProfileService>();
            try
            {
                UserProfileField upf = svcProfile.GetProfileField(fieldName) as UserProfileField;
                if (upf == null)
                {
                    upf = new UserProfileField { Name = fieldName, InternalName = fieldName, FieldTypeID = 1, IsSearchable = false };
                    svcProfile.SaveProfileField(upf);
                }
            }catch{}
        }

    }
    public class GoToTrainingAPIExtensionService
    {
        private readonly GoToTrainingService _svcGTT = null;

        public GoToTrainingAPIExtensionService(IPluginConfiguration configuration)
        {
            _svcGTT = new GoToTrainingService();
        }

        public List<GTT.Organizer> GetOrganizers()
        {
            return _svcGTT.GetOrganizers();
        }
        public GTT.Training GetTrainingPrerequisite()
        {
            return _svcGTT.GetTrainingPreRequisite();
        }
        public GTT.Training GetTraining(string trainingKey)
        {
            return _svcGTT.GetTraining(trainingKey);
        }
        public List<GTT.Training> GetTrainingsForCurrent()
        {
            return _svcGTT.GetTrainings();
        }
        public List<GTT.Training> GetTrainings(string organizerKey)
        {
            return _svcGTT.GetTrainings(organizerKey);
        }
        public List<GTT.Session> GetSessions(string trainingKey)
        {
            return _svcGTT.GetSessions(trainingKey);
        }
        public List<GTT.Session> GetSessions(DateTime startDate, DateTime endDate)
        {
            return _svcGTT.GetSessions(startDate, endDate);
        }
        public List<GTT.Registrant> GetRegistrant(string trainingKey)
        {
            return _svcGTT.GetRegistrants(trainingKey);
        }
        public bool HasTakenSubscriptionTrainingPreReq()
        {
            return _svcGTT.UserHasTakenTrainingPreRequisite();
        }
        public List<GTT.Attendee> GetAttendees(DateTime startDate, DateTime endDate, string titleFilter)
        {
            return _svcGTT.GetAttendees(startDate, endDate, titleFilter);
        }
        public DateTime UpdateTrainingPreReq(DateTime startTime)
        {
            return _svcGTT.UpdateTrainingPreReq(startTime);
        }
        public bool HasTakenSubscriptionTrainingPreReq(int userId)
        {
            return _svcGTT.UserHasTakenTrainingPreRequisite(userId);
        }
        public bool HasTakenSubscriptionTrainingPreReq(V1Entities.User user)
        {
            return _svcGTT.UserHasTakenTrainingPreRequisite(user);
        }

        public List<GTT.Session> GetSessions(string organizerKey, DateTime startDate, DateTime endDate)
        {
            return _svcGTT.GetSessions(organizerKey, startDate, endDate);
        }
    }
}
