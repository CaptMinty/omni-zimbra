﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
//using Microsoft.SharePoint.Client;
using Omnicell.Custom.Components;
using Omnicell.Data.Model;
using Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.Api.Version1;
using Telligent.Evolution.Extensibility.UI.Version1;
using Telligent.Evolution.Extensibility.Urls.Version1;

namespace Omnicell.Custom
{
    public class OmnicellUtilitiesPlugin : IScriptedContentFragmentExtension
    {
        public void Initialize()
        {

        }

        public string Name
        {
            get { return "Omnicell Utility Widget Methods"; }
        }
        public string Description
        {
            get { return "Enables scripted content fragments to use functionality for the Omnicell Utilities."; }
        }
        public object Extension
        {
            get { return new OmnicellUtilitiesExtension(); }
        }
        public string ExtensionName
        {
            get { return "omnicell_v1_utility"; }
        }


    }

    public class OmnicellUtilitiesExtension
    {
        private IRelatedDocumentsService _svcRelatedDocuments = null;

        public OmnicellUtilitiesExtension()
        {
            _svcRelatedDocuments = new RelatedDocumentsService();
        }

        public RelatedDocuments GetRelatedDocumentsByGroupId(int groupId)
        {
            return _svcRelatedDocuments.Get(groupId);
        }

        public RelatedDocuments SaveRelatedDocuments(RelatedDocuments relatedDocuments)
        {
            return _svcRelatedDocuments.Save(relatedDocuments);
        }

        public List<KeyValuePair<string, object>> SortDictionary(IDictionary dictionary)
        {
            var newList = dictionary.Keys.Cast<string>().Select(k => new KeyValuePair<string, object>(k, dictionary[k])).OrderBy(k => k.Key).ToList();
            return newList;
        }

        public string UrlEncrypt(string clearText)
        {
            byte[] clearBytes = System.Text.Encoding.Unicode.GetBytes(clearText);
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(_Pwd, _Salt);
            byte[] encryptedData = PrivateUrlEncrypt(clearBytes, pdb.GetBytes(32), pdb.GetBytes(16));
            return Convert.ToBase64String(encryptedData);

        }

        private byte[] PrivateUrlEncrypt(byte[] clearData, byte[] Key, byte[] IV)
        {
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = null;

            try
            {
                Rijndael alg = Rijndael.Create();
                alg.Key = Key;
                alg.IV = IV;
                cs = new CryptoStream(ms, alg.CreateEncryptor(), CryptoStreamMode.Write);
                cs.Write(clearData, 0, clearData.Length);
                cs.FlushFinalBlock();
                return ms.ToArray();
            }
            catch
            {
                return null;
            }
            finally
            {
                cs.Close();
            }
        }

        //public String GetSharepointLibrary()
        //{

        //    ClientContext cc = new ClientContext("http://sp.defaktolabs.com/Repository");
        //    cc.AuthenticationMode = ClientAuthenticationMode.Default;
        //    FormsAuthenticationLoginInfo formLog = new FormsAuthenticationLoginInfo("Administrator", "DGAdmin!1");
        //    cc.FormsAuthenticationLoginInfo = formLog;
        //    Web w = cc.Web;
        //    var lists = w.Lists.GetByTitle("Publications");
        //    //docList = cc.Web.Lists.GetByTitle("Publications");
        //    cc.ExecuteQuery();
        //    // lists.Cast().ToList();

        //    return lists.ToString();

        //}

        public bool CSNSubscribe(string csn, int? groupId, int? userId)
        {
            bool subscribe = false;

            try
            {
                if (csn.Length > 0)
                {
                    string[] csns = csn.Split(',');

                    var entities = new OmnicellEntities();

                    foreach (string number in csns)
                    {
                        if (entities.Omnicell_CSNSubscription.Any(x => x.CSN == number.Trim()))
                        {
                            addUser(groupId, userId);
                        }
                        else
                        {
                            deleteUser(groupId, userId);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string error = "Exception occurred Deleting CSN from subscription table(csn=" + csn + "):" + ex.ToString();
            }

            return subscribe;

        }

        public bool InactiveCSNTest(string csn)
        {
            bool subscribe = false;
            ;
            try
            {
                if (csn.Length > 0)
                {
                    string[] csns = csn.Split(',');

                    var entities = new OmnicellEntities();

                    foreach (string number in csns)
                    {
                        if (entities.Omnicell_Customer.Any(x => !x.IsActive && x.CSN == number.Trim()))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error checking customer/ international partner csn: {0}", csn), ex).Log();
            }

            return subscribe;
        }

        public bool ActiveCSNTest(string csn)
        {
            bool subscribe = false;

            try
            {
                if (csn.Length > 0)
                {
                    string[] csns = csn.Split(',');

                    var entities = new OmnicellEntities();

                    foreach (string number in csns)
                    {
                        if (entities.Omnicell_Customer.Any(x => x.IsActive && csn.Equals(x.CSN)))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error checking customer/ international partner csn: {0}", csn), ex).Log();
            }

            return subscribe;
        }

        public void LoginCheckCreateRole(string roleName, int? userId)
        {
            Telligent.Evolution.Extensibility.Api.Entities.Version1.User user =
                  PublicApi.Users.Get(new UsersGetOptions() { Id = userId });

            if (user != null)
            {
                RolesCreateOptions opt = new RolesCreateOptions();
                opt.Include = "user";
                opt.RoleName = roleName;
                opt.UserId = user.Id;

                var create = PublicApi.Roles.Create("", "", opt);


            }
        }

        public void LoginCheckDeleteRole(int roleId, int? userId)
        {
            Telligent.Evolution.Extensibility.Api.Entities.Version1.User user =
                 PublicApi.Users.Get(new UsersGetOptions() { Id = userId });

            if (user != null)
            {
                RolesDeleteOptions opt = new RolesDeleteOptions();
                opt.Include = "user";
                opt.UserId = user.Id;
            }


        }

        public bool addUser(int? groupId, int? userId)
        {

            GoToTrainingPlugin gtt = Telligent.Evolution.Extensibility.Version1.PluginManager.Get<GoToTrainingPlugin>().FirstOrDefault();

            bool added = false;
            var webClient = new WebClient();
            var adminKey = String.Format("{0}:{1}", gtt.RestAPIKey, gtt.RestAPIUser);
            var adminKeyBase64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(adminKey));
            webClient.Headers.Add("Rest-User-Token", adminKeyBase64);
            var restUrl = gtt.RestAPIUrl + "/api.ashx/v2/groups/{0}/members.xml";

            var values = new NameValueCollection();
            var requestUrl = string.Format(restUrl, groupId.ToString());
            values.Add("UserId", userId.ToString());
            values.Add("GroupMembershipType", "Member");
            values.Add("Message", "Member Added");
            var xml = Encoding.UTF8.GetString(webClient.UploadValues(requestUrl, values));

            return added;
        }

        public bool deleteUser(int? groupId, int? userId)
        {
            GoToTrainingPlugin gtt = Telligent.Evolution.Extensibility.Version1.PluginManager.Get<GoToTrainingPlugin>().FirstOrDefault();
            bool delete = false;

            var webClient = new WebClient();
            var adminKey = String.Format("{0}:{1}", gtt.RestAPIKey, gtt.RestAPIUser);
            var adminKeyBase64 = Convert.ToBase64String(Encoding.UTF8.GetBytes(adminKey));
            webClient.Headers.Add("Rest-User-Token", adminKeyBase64);
            webClient.Headers.Add("Rest-Method", "DELETE");

            var requestUrl = gtt.RestAPIUrl + "/api.ashx/v2/groups/{0}/members/users/{1}.xml";
            var url = string.Format(requestUrl, groupId.ToString(), userId.ToString());
            var values = new NameValueCollection();
            //values.Add("Rolename", "Member");

            var xml = Encoding.UTF8.GetString(webClient.UploadValues(url, values));



            return delete;
        }

        public string ScrubADub(string dirty, string reggie)
        {

            // Regex perps = new Regex("(\\(ZIP\\)|\\(CD\\)|\\(SCORM 1.2\\)|\\(INTRANET\\)|\\(RECORDED WEBINAR\\)|\\(EXE\\)|\\(HTML\\)|\\(HTML.ZIP\\)|\\(.zip\\)|\\(Training\\)|\\(SCORM\\)|.zip|- HTML.ZIP|- cd.zip| - EXE| - CD.TXT| - HTML.TXT|\\(INTRANET\\)|HTML|CD|EXE|ZIP|SCORM 1.2|Training|Intranet|Recorded Webinar)", RegexOptions.IgnoreCase);
            string replace = "";

            Regex perps = new Regex(reggie, RegexOptions.IgnoreCase);



            return perps.Replace(dirty, replace);
        }

        public bool InternationalDomain(string email)
        {
            bool internationalPartner = false;

            var entities = new OmnicellEntities();

            try
            {
                List<Omnicell_InternationalDomain> international = entities.Omnicell_InternationalDomain.ToList();

                if (email.Length > 0)
                {
                    foreach (var domain in international)
                    {
                        if (email.Contains(domain.EmailDomain))
                        {
                            internationalPartner = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string error = "Exception occurred while checking email domain." + ex.ToString();
            }

            return internationalPartner;
        }

        //public bool CreateUpdateKOLRecord(int userId, string optin)
        //{
        //    DateTime date = DateTime.Now;
        //    bool worked = false;
        //    int userID = System.Convert.ToInt32(userId);
        //    var db = new OmnicellEntities();
        //    int optIn = System.Convert.ToInt32(optin);
        //    try
        //    {
        //        var exists = db.Omnicell_KeyOpinionOptIn.FirstOrDefault(x => x.UserId == userID);

        //        if (exists != null)
        //        {
        //            exists.Opt_In = optIn;
        //            exists.Updated = date;

        //            db.Omnicell_KeyOpinionOptIn.ApplyCurrentValues(exists);
        //            db.SaveChanges();
        //            worked = true;
        //        }
        //        else
        //        {
        //            Omnicell_KeyOpinionOptIn signUp = new Omnicell_KeyOpinionOptIn
        //            {
        //                UserId = userID,
        //                Opt_In = optIn,
        //                Updated = date
        //            };

        //            db.Omnicell_KeyOpinionOptIn.AddObject(signUp);
        //            db.SaveChanges();
        //            worked = true;

        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }

        //    return false;

        //}

        //public bool kolRecordExists(int userId)
        //{
        //    bool exists = false;

        //    var db = new OmnicellEntities();

        //    try
        //    {
        //        var record = db.Omnicell_KeyOpinionOptIn.FirstOrDefault(x => x.UserId == userId);

        //        if (record != null)
        //        {
        //            exists = true;
        //        }
        //    }
        //    catch
        //    {
        //        return false;
        //    }


        //    return exists;
        //}

        //public int retrieveKolOptin(int userId)
        //{
        //    var db = new OmnicellEntities();

        //    int optIn = -1;

        //    try
        //    {
        //        var record = db.Omnicell_KeyOpinionOptIn.FirstOrDefault(x => x.UserId == userId);

        //        if (record != null)
        //        {
        //            optIn = record.Opt_In;
        //        }
        //    }
        //    catch
        //    {
        //        return -1;
        //    }


        //    return optIn;

        //}

        static string _Pwd = "Hot0@Pants";
        static byte[] _Salt = new byte[] { 0x44, 0xe4, 0x77, 0x6c, 0x67, 0x1, 0x88, 0x1a, 0x29, 0x8e, 0x65, 0x74, 0x11 };


    }
}
