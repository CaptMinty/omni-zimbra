﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Common;
using Telligent.Evolution.Components;
using Telligent.Evolution.Components.Search;
using V1Entities = Telligent.Evolution.Extensibility.Api.Entities.Version1;
using Telligent.Evolution.Extensibility.Api.Version1;
using Telligent.Evolution.Extensibility.UI.Version1;
using Telligent.Evolution.Extensibility.Version1;

using Omnicell.Custom;
using Omnicell.Custom.Components;
using Omnicell.Custom.Search;
using MoreLinq;

namespace Omnicell.Custom
{
    public class OmnicellCoursePlugin : IScriptedContentFragmentExtension
    {
        public void Initialize() { }
        public string Name
        {
            get { return "Omnicell Course Widget Methods"; }
        }
        public string Description
        {
            get { return "Enables scripted content fragments to use functionality for the Omnicell Course."; }
        }
        public object Extension
        {
            get { return new OmnicellCourseWidgetExtension(); }
        }
        public string ExtensionName
        {
            get { return "omnicell_v1_course"; }
        }
    }
    public class OmnicellCourseWidgetExtension
    {
        private ICourseService _svcC = null;

        public OmnicellCourseWidgetExtension()
        {
            _svcC = new CourseService();
        }

        public OmnicellCoursePost Get(int coursePostId)
        {
            return _svcC.Get(coursePostId);
        }

        [Documentation("Creates a new course item")]
        public OmnicellCoursePost Create(int galleryId, string name, string contentType, string fileName,
            [
                Documentation(Name = "Description", Type = typeof(string), Description = "Description"),
                Documentation(Name = "IsFeatured", Type = typeof(bool), Description = "Is Featured", Options = new string[] { "true", "false" }),
                Documentation(Name = "FeaturedImage", Type = typeof(string), Description = "Used to include a featured image when IsFeatured is true."),
                Documentation(Name = "FileData", Type = typeof(byte[]), Description = "FileData or FileUrl is required."),
                Documentation(Name = "FileUrl", Type = typeof(string), Description = "FileData or FileUrl is required."),
                Documentation(Name = "Tags", Type = typeof(string), Description = "A comma seperated list of tags."),
                Documentation(Name = "DocumentTypeId", Type = typeof(int), Description = "Document Type"),
                Documentation(Name = "MediaFormatId", Type = typeof(int), Description = "Media Format"),
                Documentation(Name = "CourseTypeId", Type = typeof(int), Description = "Scheduled Course Type"),
                Documentation(Name = "CourseInfo", Type = typeof(string), Description = "Course Info"),
                Documentation(Name = "Products", Type = typeof(string), Description = "A comma seperated list of products"),
                Documentation(Name = "BeginDate", Type = typeof(DateTime), Description = "If not specified, the course will not be given a begin date."),
                Documentation(Name = "EndDate", Type = typeof(DateTime), Description = "If not specified, course will not be given an end date.(Required if BeginDate is provided)"),
                Documentation(Name = "Action", Type = typeof(string), Description = "Action type: Download, Register or View"),
                Documentation(Name = "Sku", Type = typeof(string), Description = "Unique value to identify the same training content despite file extension")
            ]
            IDictionary options)
        {
            MediaCreateOptions createOptions = new MediaCreateOptions();

            string products = string.Empty;
            string action = options.GetValue("Action", String.Empty);
            string sku = options.GetValue("Sku", String.Empty);

            int mediaFormatId = -1;
            if (options != null)
            {
                products = options.GetValue("Products");
                mediaFormatId = options.GetValue<int>("MediaFormatId");

                if (options["Description"] != null)
                    createOptions.Description = options["Description"].ToString();

                if (options["FileData"] != null)
                    createOptions.FileData = (byte[])options["FileData"];

                if (options["FileUrl"] != null)
                    createOptions.FileUrl = options["FileUrl"].ToString();

                if (options["Tags"] != null)
                    createOptions.Tags = options["Tags"].ToString();

                if (options["IsFeatured"] != null)
                    createOptions.IsFeatured = new bool?(Convert.ToBoolean(options["IsFeatured"]));

                if (options["FeaturedImage"] != null)
                    createOptions.FeaturedImage = options["FeaturedImage"].ToString();

                if (options["ExtendedAttributes"] != null)
                {
                    var attributes = options["ExtendedAttributes"] as IDictionary;
                    var keys = attributes.Keys.Cast<int>().ToList();

                    var attrList = keys.Select(k => new V1Entities.ExtendedAttribute { Key = k.ToString(), Value = attributes[k].ToString() }).ToList();
                    createOptions.ExtendedAttributes = attrList;
                }

                if (!string.IsNullOrEmpty(sku))
                    createOptions.Sku(sku);

                if (!string.IsNullOrEmpty(action))
                    createOptions.Action(action);

                if (options["MediaFormatId"] != null)
                    createOptions.MediaFormatId(options["MediaFormatId"].ToString());

                if (options["CourseTypeId"] != null)
                    createOptions.CourseTypeId(options["CourseTypeId"].ToString());

                if (options["CourseInfo"] != null)
                    createOptions.CourseInfo(options["CourseInfo"].ToString());

                if (options["Products"] != null && !string.IsNullOrWhiteSpace(options["Products"].ToString()))
                    createOptions.ProductFilters(options["Products"].ToString());

                DateTime bdt = options.GetValue<DateTime>("BeginDate", DateTime.MinValue);
                DateTime edt = options.GetValue<DateTime>("EndDate", DateTime.MinValue);

                if (bdt > DateTime.MinValue)
                    createOptions.BeginDate(bdt);
                if (edt > DateTime.MinValue)
                    createOptions.EndDate(edt);
            }
            var media = PublicApi.Media.Create(galleryId, name, contentType, fileName, createOptions);

           // return _svcC.Get(media.Id.Value);

            return _svcC.Save(media.Id.Value, mediaFormatId, products);

        }

        [Documentation("Updates a new course item")]
        public OmnicellCoursePost Update(int galleryId, int fileId,
            [
                Documentation(Name = "Name", Type = typeof(string), Description = "Name"),
                Documentation(Name = "ContentType", Type = typeof(string), Description = "Required when updating FileData or FileUrl"),
                Documentation(Name = "FileName", Type = typeof(string), Description = "Required when updating FileData or FileUrl"),
                Documentation(Name = "Description", Type = typeof(string), Description = "Description"),
                Documentation(Name = "IsFeatured", Type = typeof(bool), Description = "Is Featured", Options = new string[] { "true", "false" }),
                Documentation(Name = "FeaturedImage", Type = typeof(string), Description = "Used to include a featured image when IsFeatured is true."),
                Documentation(Name = "FileData", Type = typeof(byte[]), Description = "FileData or FileUrl is required."),
                Documentation(Name = "FileUrl", Type = typeof(string), Description = "FileData or FileUrl is required."),
                Documentation(Name = "Tags", Type = typeof(string), Description = "A comma seperated list of tags."),
                Documentation(Name = "DocumentTypeId", Type = typeof(int), Description = "Document Type"),
                Documentation(Name = "MediaFormatId", Type = typeof(int), Description = "Media Format"),
                Documentation(Name = "CourseTypeId", Type = typeof(int), Description = "Scheduled Course Type"),
                Documentation(Name = "CourseInfo", Type = typeof(string), Description = "Course Info"),
                Documentation(Name = "Products", Type = typeof(string), Description = "A comma seperated list of products"),
                Documentation(Name = "BeginDate", Type = typeof(DateTime), Description = "If not specified, the course will not be given a begin date."),
                Documentation(Name = "EndDate", Type = typeof(DateTime), Description = "If not specified, the course will not be given an end date.(Required if BeginDate is provided)"),
                Documentation(Name = "Action", Type = typeof(string), Description = "Action type: Download, Register or View"),
                Documentation(Name = "Sku", Type = typeof(string), Description = "Unique value to identify the same training content despite file extension")
            ]
            IDictionary options)
        {
            MediaUpdateOptions updateOptions = new MediaUpdateOptions();
            V1Entities.Media media = null;
            string action = options.GetValue("Action", String.Empty);
            string sku = options.GetValue("Sku", String.Empty);

            OmnicellCoursePost returnValue = null;

            if (options != null)
            {
                string products = options.GetValue("Products");
                int mediaFormatId = options.GetValue<int>("MediaFormatId");

                if (options["Name"] != null)
                    updateOptions.Name = options["Name"].ToString();

                if (options["ContentType"] != null)
                    updateOptions.ContentType = options["ContentType"].ToString();

                if (options["FileName"] != null)
                    updateOptions.FileName = options["FileName"].ToString();

                if (options["Description"] != null)
                    updateOptions.Description = options["Description"].ToString();

                if (options["FileData"] != null)
                    updateOptions.FileData = (byte[])options["FileData"];

                if (options["FileUrl"] != null)
                    updateOptions.FileUrl = options["FileUrl"].ToString();

                if (options["Tags"] != null)
                    updateOptions.Tags = options["Tags"].ToString();

                if (options["IsFeatured"] != null)
                    updateOptions.IsFeatured = new bool?(Convert.ToBoolean(options["IsFeatured"]));

                if (options["FeaturedImage"] != null)
                    updateOptions.FeaturedImage = options["FeaturedImage"].ToString();

                if (options["ExtendedAttributes"] != null)
                {
                    var attributes = options["ExtendedAttributes"] as IDictionary;
                    var keys = attributes.Keys.Cast<int>().ToList();

                    var attrList = keys.Select(k => new V1Entities.ExtendedAttribute { Key = k.ToString(), Value = attributes[k].ToString() }).ToList();
                    updateOptions.ExtendedAttributes = attrList;
                }
                if (!string.IsNullOrEmpty(action))
                    updateOptions.Action(action);

                if (!string.IsNullOrEmpty(sku))
                     updateOptions.Sku(sku);

                if (options["MediaFormatId"] != null)
                    updateOptions.MediaFormatId(options["MediaFormatId"].ToString());

                if (options["CourseTypeId"] != null)
                    updateOptions.CourseTypeId(options["CourseTypeId"].ToString());

                if (options["CourseInfo"] != null)
                    updateOptions.CourseInfo(options["CourseInfo"].ToString());

                if (options["Products"] != null && !string.IsNullOrWhiteSpace(options["Products"].ToString()))
                    updateOptions.ProductFilters(options["Products"].ToString());
                else
                    updateOptions.ProductFilters(string.Empty);

                DateTime bdt = options.GetValue<DateTime>("BeginDate", DateTime.MinValue);
                DateTime edt = options.GetValue<DateTime>("EndDate", DateTime.MinValue);

                if (bdt > DateTime.MinValue)
                    updateOptions.BeginDate(bdt);
                if (edt > DateTime.MinValue)
                    updateOptions.EndDate(edt);

                media = PublicApi.Media.Update(galleryId, fileId, updateOptions);
                //returnValue = _svcC.Get(media.Id.Value);
                
                returnValue = _svcC.Save(media.Id.Value, mediaFormatId, products);
            }

            if (returnValue == null)
                returnValue = new OmnicellCoursePost(fileId);

            return returnValue;
        }

        public V1Entities.PagedList<OmnicellCoursePost> List(int galleryId,
            [
                Documentation(Name = "Product", Type = typeof(string), Description = "Product GroupId(s) to be found. For many groups use format '(1 OR 2 OR 3)'"),
                Documentation(Name = "CourseType", Type = typeof(int), Description = "Course Type to be found"),
                Documentation(Name = "MediaType", Type = typeof(int), Description = "Media Type to be found"),
                Documentation(Name = "PageIndex", Type = typeof(int), Description = "Specify the page number of paged results to return. Zero-based index.", Default = 0),
                Documentation(Name = "PageSize", Type = typeof(int), Description = "Specify the number of results to return per page.", Default = 20),
                Documentation(Name = "Query", Type = typeof(string), Description = "Query is not required but you should use either Query or Filters otherwise you'll get all documents in the search index."),
                Documentation(Name = "Sort", Type = typeof(string), Description = "Sort", Default = "date+desc", Options = new string[] { "date", "date+asc", "date+desc", "titlesort", "titlesort+asc", "titlesort+desc", "coursebegindate", "coursebegindate+asc", "coursebegindate+desc" }),
                Documentation(Name = "OnlyShowCurrent", Type = typeof(bool), Description = "Shows only corses that have not ended", Default = false, Options = new string[] { "true" })
            ]
            IDictionary options)
        {
            SearchResultsListOptions searchOptions = new SearchResultsListOptions();

            string query = options.GetValue("Query");
            string product = options.GetValue("Product");
            int courseTypeId = options.GetValue<int>("CourseType");
            int mediaTypeId = options.GetValue<int>("MediaType");
            int pageIndex = options.GetValue<int>("PageIndex", 0);
            int pageSize = options.GetValue<int>("PageSize", 20);
            string sort = options.GetValue("Sort", "date+desc").Replace("+", " ").Replace("coursebegindate", OmnicellSearchFields.CourseBeginDate);
            bool onlyShowCurrent = options.GetValue<bool>("OnlyShowCurrent", false);

            int currId = 0;

            // this will always search for files so that filter is hard coded           
            List<FieldFilter> fieldFilters = new List<FieldFilter> { new FieldFilter { Id = currId++, FieldName = SearchFields.ContentType, FieldValue = "file" } };
            fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = SearchFields.SectionID, FieldValue = galleryId.ToString() });

            if (!string.IsNullOrEmpty(query))
                searchOptions.Query = query;


            if (!string.IsNullOrEmpty(product))
                fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = OmnicellSearchFields.ProductIdFilter, FieldValue = product });

            if (courseTypeId > 0)
                fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = OmnicellSearchFields.CourseTypeId, FieldValue = courseTypeId.ToString() });

            if (mediaTypeId > 0)
                fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = OmnicellSearchFields.MediaFormatId, FieldValue = mediaTypeId.ToString() });

            if (onlyShowCurrent)
                fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = OmnicellSearchFields.CourseEndDate, FieldValue = "[ NOW TO * ]" });

            searchOptions.PageIndex = pageIndex;
            searchOptions.PageSize = pageSize < 100 ? pageSize : 100;
            searchOptions.FieldFilters = fieldFilters;

            searchOptions.Sort = sort;

            V1Entities.SearchResults results = PublicApi.Search.List(searchOptions);

            List<OmnicellCoursePost> posts = new List<OmnicellCoursePost>();

            results.ToList().ForEach(r => posts.Add(new OmnicellCoursePost(int.Parse(r.ContentId))));
            posts = posts.Where(x => x.Id != null).ToList();

            return new V1Entities.PagedList<OmnicellCoursePost>(posts, pageSize, pageIndex, results.TotalCount);
        }

        public V1Entities.PagedList<OmnicellCoursePost> CourseList(string galleryId,
           [
               Documentation(Name = "Product", Type = typeof(string), Description = "Product GroupId(s) to be found. For many groups use format '(1 OR 2 OR 3)'"),
               Documentation(Name = "CourseType", Type = typeof(int), Description = "Course Type to be found"),
               Documentation(Name = "MediaType", Type = typeof(int), Description = "Media Type to be found"),
               Documentation(Name = "PageIndex", Type = typeof(int), Description = "Specify the page number of paged results to return. Zero-based index.", Default = 0),
               Documentation(Name = "PageSize", Type = typeof(int), Description = "Specify the number of results to return per page.", Default = 20),
               Documentation(Name = "Query", Type = typeof(string), Description = "Query is not required but you should use either Query or Filters otherwise you'll get all documents in the search index."),
               Documentation(Name = "Sort", Type = typeof(string), Description = "Sort", Default = "date+desc", Options = new string[] { "date", "date+asc", "date+desc", "titlesort", "titlesort+asc", "titlesort+desc", "coursebegindate", "coursebegindate+asc", "coursebegindate+desc" }),
               Documentation(Name = "OnlyShowCurrent", Type = typeof(bool), Description = "Shows only corses that have not ended", Default = false, Options = new string[] { "true" })
           ]
            IDictionary options)
        {
            SearchResultsListOptions searchOptions = new SearchResultsListOptions();

            string query = options.GetValue("Query");
            string product = options.GetValue("Product");
            int courseTypeId = options.GetValue<int>("CourseType");
            int mediaTypeId = options.GetValue<int>("MediaType");
            int pageIndex = options.GetValue<int>("PageIndex", 0);
            int pageSize = options.GetValue<int>("PageSize", 20);
            string sort = options.GetValue("Sort", "date+desc").Replace("+", " ").Replace("coursebegindate", OmnicellSearchFields.CourseBeginDate);
            bool onlyShowCurrent = options.GetValue<bool>("OnlyShowCurrent", false);

            int currId = 0;

            // this will always search for files so that filter is hard coded           
            List<FieldFilter> fieldFilters = new List<FieldFilter> { new FieldFilter { Id = currId++, FieldName = SearchFields.ContentType, FieldValue = "file" } };
            fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = SearchFields.SectionID, FieldValue = galleryId.ToString() });

            if (!string.IsNullOrEmpty(query))
                searchOptions.Query = query;


            if (!string.IsNullOrEmpty(product))
                fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = OmnicellSearchFields.ProductIdFilter, FieldValue = product });

            if (courseTypeId > 0)
                fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = OmnicellSearchFields.CourseTypeId, FieldValue = courseTypeId.ToString() });

            if (mediaTypeId > 0)
                fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = OmnicellSearchFields.MediaFormatId, FieldValue = mediaTypeId.ToString() });

            if (onlyShowCurrent)
                fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = OmnicellSearchFields.CourseEndDate, FieldValue = "[ NOW TO * ]" });

            searchOptions.PageIndex = pageIndex;
            searchOptions.PageSize = pageSize < 100 ? pageSize : 100;
            searchOptions.FieldFilters = fieldFilters;

            searchOptions.Sort = sort;

            V1Entities.SearchResults results = PublicApi.Search.List(searchOptions);

            List<OmnicellCoursePost> posts = new List<OmnicellCoursePost>();

            results.ToList().ForEach(r => posts.Add(new OmnicellCoursePost(int.Parse(r.ContentId))));
            posts = posts.Where(x => x.Id != null).ToList();

            return new V1Entities.PagedList<OmnicellCoursePost>(posts, pageSize, pageIndex, results.TotalCount);
        }

        public V1Entities.PagedList<OmnicellCoursePost> OnlineDownloadList(int galleryId,
          [
              Documentation(Name = "Product", Type = typeof(string), Description = "Product GroupId(s) to be found. For many groups use format '(1 OR 2 OR 3)'"),
              Documentation(Name = "CourseType", Type = typeof(int), Description = "Course Type to be found"),
              Documentation(Name = "MediaType", Type = typeof(int), Description = "Media Type to be found"),
              Documentation(Name = "PageIndex", Type = typeof(int), Description = "Specify the page number of paged results to return. Zero-based index.", Default = 0),
              Documentation(Name = "PageSize", Type = typeof(int), Description = "Specify the number of results to return per page.", Default = 20),
              Documentation(Name = "Query", Type = typeof(string), Description = "Query is not required but you should use either Query or Filters otherwise you'll get all documents in the search index."),
              Documentation(Name = "Sort", Type = typeof(string), Description = "Sort", Default = "date+desc", Options = new string[] { "date", "date+asc", "date+desc", "titlesort", "titlesort+asc", "titlesort+desc", "coursebegindate", "coursebegindate+asc", "coursebegindate+desc" }),
              Documentation(Name = "OnlyShowCurrent", Type = typeof(bool), Description = "Shows only courses that have not ended", Default = false, Options = new string[] { "true" })
          ]
            IDictionary options)
        {
            SearchResultsListOptions searchOptions = new SearchResultsListOptions();

            string query = options.GetValue("Query");
            string product = options.GetValue("Product");
            int courseTypeId = options.GetValue<int>("CourseType");
            int mediaTypeId = options.GetValue<int>("MediaType");
            int pageIndex = options.GetValue<int>("PageIndex", 0);
            int pageSize = options.GetValue<int>("PageSize", 1000);
            string sort = options.GetValue("Sort", "date+desc").Replace("+", " ").Replace("coursebegindate", OmnicellSearchFields.CourseBeginDate);
            bool onlyShowCurrent = options.GetValue<bool>("OnlyShowCurrent", false);

            int currId = 0;

            // this will always search for files so that filter is hard coded           
            List<FieldFilter> fieldFilters = new List<FieldFilter> { new FieldFilter { Id = currId++, FieldName = SearchFields.ContentType, FieldValue = "file" } };
            fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = SearchFields.SectionID, FieldValue = galleryId.ToString() });

            if (!string.IsNullOrEmpty(query))
                searchOptions.Query = query;


            if (!string.IsNullOrEmpty(product))
                fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = OmnicellSearchFields.ProductIdFilter, FieldValue = product });

            if (courseTypeId > 0)
                fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = OmnicellSearchFields.CourseTypeId, FieldValue = courseTypeId.ToString() });

            if (mediaTypeId > 0)
                fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = OmnicellSearchFields.MediaFormatId, FieldValue = mediaTypeId.ToString() });

            if (onlyShowCurrent)
                fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = OmnicellSearchFields.CourseEndDate, FieldValue = "[ NOW TO * ]" });

            searchOptions.PageIndex = 0;
            searchOptions.PageSize = 1000;
            searchOptions.FieldFilters = fieldFilters;

            searchOptions.Sort = sort;

            V1Entities.SearchResults results = PublicApi.Search.List(searchOptions);

            List<OmnicellCoursePost> posts = new List<OmnicellCoursePost>();

            results.ToList().ForEach(r => posts.Add(new OmnicellCoursePost(int.Parse(r.ContentId))));
            posts = posts.Where(x => x.Id != null).ToList();
            posts = posts.DistinctBy(x => x.Sku).ToList();
            int count = posts.Count;
            posts = posts.Skip((pageIndex) * pageSize).Take(pageSize).ToList();
           //  omnicellMedias = omnicellMedias.Skip((nPageIndex) * nPageSize).Take(nPageSize).ToList()
            return new V1Entities.PagedList<OmnicellCoursePost>(posts, pageSize, pageIndex, count);
        }

        public V1Entities.PagedList<OmnicellCoursePost> GetOnlineForSku(int postId, int galleryId, string sort )
        {
            int currId = 0;
            int pageIndex = 0;
            int pageSize = 1000;

            OmnicellCoursePost post = _svcC.Get(postId);
            List<OmnicellCoursePost> posts = new List<OmnicellCoursePost>();
            SearchResultsListOptions searchOptions = new SearchResultsListOptions();

            List<FieldFilter> fieldFilters = new List<FieldFilter> { new FieldFilter { Id = currId++, FieldName = SearchFields.ContentType, FieldValue = "file" } };
            fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = SearchFields.SectionID, FieldValue = galleryId.ToString() });
            searchOptions.PageIndex = pageIndex;
            searchOptions.PageSize = pageSize;
            searchOptions.FieldFilters = fieldFilters;
            searchOptions.Sort = sort;

            V1Entities.SearchResults results = PublicApi.Search.List(searchOptions);

            results.ToList().ForEach(r => posts.Add(new OmnicellCoursePost(int.Parse(r.ContentId))));
            posts = posts.Where(x => x.Id != null && x.Sku == post.Sku).ToList();

            if (!posts.Any(x => x.Id == post.Id))
            {
                posts.Add(post);
            }
            int count = posts.Count;

            return new V1Entities.PagedList<OmnicellCoursePost>(posts, pageSize, pageIndex, count);

        }
    }
}
