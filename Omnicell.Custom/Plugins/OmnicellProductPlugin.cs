﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Common;
using Telligent.DynamicConfiguration.Components;
using Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.UI.Version1;
using Telligent.Evolution.Extensibility.Version1;
using TEntities = Telligent.Evolution.Extensibility.Api.Entities.Version1;
using TApi = Telligent.Evolution.Extensibility.Api.Version1;


using Omnicell.Custom;
using Omnicell.Custom.Components;

namespace Omnicell.Custom
{
    public class OmnicellProductPlugin : IConfigurablePlugin, IScriptedContentFragmentExtension
    {
        private readonly IUrlManipulation UrlManipulation = Telligent.Common.Services.Get<IUrlManipulation>();

        public void Initialize() { }
        public string Name
        {
            get { return "Omnicell Product Widget Methods"; }
        }
        public string Description
        {
            get { return "Enables scripted content fragments to use functionality for the Omnicell Products."; }
        }
        public string ExtensionName
        {
            get { return "omnicell_v1_product"; }
        }
        public object Extension
        {
            get { return new OmnicellProductWidgetExtension(_configuration); }
        }

        IPluginConfiguration _configuration = null;
        public Telligent.DynamicConfiguration.Components.PropertyGroup[] ConfigurationOptions
        {
            get
            {
                PropertyGroup[] groups = new[] { new PropertyGroup("productSettings", "Product Settings", 0) };

                Property productParentGroupIdProp = new Property("productParentGroupId", "Product Group", PropertyType.Custom, 0, "Group=5");
                productParentGroupIdProp.DescriptionText = "Select the parent Product group.";
                productParentGroupIdProp.ControlType = typeof(Telligent.Evolution.Controls.GroupSelectionList);
                groups[0].Properties.Add(productParentGroupIdProp);

                return groups;
            }
        }
        public void Update(IPluginConfiguration configuration)
        {
            _configuration = configuration;

            ProductParentGroupId = GetValue(configuration.GetCustom("productParentGroupId"), -1);
        }

        public int ProductParentGroupId { get; private set; }

        private int GetValue(string pair, int defaultValue)
        {
            int returnValue = defaultValue;
            if (!string.IsNullOrEmpty(pair) && pair.Contains("="))
            {
                if (pair.Contains("&"))
                    pair = pair.Split("&".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[0];

                string[] segs = pair.Split("=".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                if (segs.Length > 1)
                    int.TryParse(segs[1], out returnValue);
            }
            return returnValue;
        }
    }
    public class OmnicellProductWidgetExtension
    {
        private readonly IUrlManipulation UrlManipulation = Telligent.Common.Services.Get<IUrlManipulation>();

        public OmnicellProductWidgetExtension(IPluginConfiguration configuration)
        {
            ProductParentGroupId = GetValue(configuration.GetCustom("productParentGroupId"), -1);
        }

        public int ProductParentGroupId { get; private set; }

        private TEntities.Group _productParentGroup = null;
        public TEntities.Group ProductParentGroup
        {
            get
            {
                if (_productParentGroup == null && ProductParentGroupId > 0)
                {
                    _productParentGroup = TApi.PublicApi.Groups.Get(new TApi.GroupsGetOptions { Id = ProductParentGroupId });
                }
                return _productParentGroup;
            }
        }

        private int GetValue(string pair, int defaultValue)
        {
            int returnValue = defaultValue;
            if (!string.IsNullOrEmpty(pair) && pair.Contains("="))
            {
                if (pair.Contains("&"))
                    pair = pair.Split("&".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[0];

                string[] segs = pair.Split("=".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                if (segs.Length > 1)
                    int.TryParse(segs[1], out returnValue);
            }
            return returnValue;
        }
    }
}