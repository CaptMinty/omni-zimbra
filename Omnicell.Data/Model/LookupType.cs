﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Omnicell.Data.Model
{
    public class LookupType
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
