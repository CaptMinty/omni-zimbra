﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Omnicell.Data.Model
{
    public class FailedAccountAttemptsViewModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Zip { get; set; }
        public string CSN { get; set; }
        public string FailReason { get; set; }
        public DateTime CreateDate { get; set; }
        public string IpAddress { get; set; }
    }
}
