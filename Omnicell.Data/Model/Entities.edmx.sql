
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 12/07/2014 17:21:57
-- Generated from EDMX file: C:\development\omnicell\trunk-current\Omnicell.Data\Model\Entities.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [myomnicell-castiron];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_Omnicell_CSN_Omnicell_CSNGroup]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Omnicell_CSN] DROP CONSTRAINT [FK_Omnicell_CSN_Omnicell_CSNGroup];
GO
IF OBJECT_ID(N'[dbo].[FK_Omnicell_EmailBatch_Omnicell_EmailBatchStatus]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Omnicell_EmailBatch] DROP CONSTRAINT [FK_Omnicell_EmailBatch_Omnicell_EmailBatchStatus];
GO
IF OBJECT_ID(N'[dbo].[FK_Omnicell_EmailBatchRecipient_Omnicell_EmailBatch]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Omnicell_EmailBatchRecipient] DROP CONSTRAINT [FK_Omnicell_EmailBatchRecipient_Omnicell_EmailBatch];
GO
IF OBJECT_ID(N'[dbo].[FK_Omnicell_EmailBatchRecipient_Omnicell_EmailBatchRecipientStatus]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Omnicell_EmailBatchRecipient] DROP CONSTRAINT [FK_Omnicell_EmailBatchRecipient_Omnicell_EmailBatchRecipientStatus];
GO
IF OBJECT_ID(N'[dbo].[FK_Omnicell_EmailBatchRecipientTicket_Omnicell_EmailBatchRecipient]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Omnicell_EmailBatchRecipientTicket] DROP CONSTRAINT [FK_Omnicell_EmailBatchRecipientTicket_Omnicell_EmailBatchRecipient];
GO
IF OBJECT_ID(N'[dbo].[FK_Omnicell_EmailBatchRecipientTicket_Omnicell_Ticket]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Omnicell_EmailBatchRecipientTicket] DROP CONSTRAINT [FK_Omnicell_EmailBatchRecipientTicket_Omnicell_Ticket];
GO
IF OBJECT_ID(N'[dbo].[FK_Omnicell_MediaGallery_Omnicell_MediaDocumentType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Omnicell_MediaGallery] DROP CONSTRAINT [FK_Omnicell_MediaGallery_Omnicell_MediaDocumentType];
GO
IF OBJECT_ID(N'[dbo].[FK_Omnicell_MediaGallery_Omnicell_MediaFormat]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Omnicell_MediaGallery] DROP CONSTRAINT [FK_Omnicell_MediaGallery_Omnicell_MediaFormat];
GO
IF OBJECT_ID(N'[dbo].[FK_Omnicell_MediaGallery_Omnicell_MediaGalleryType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Omnicell_MediaGallery] DROP CONSTRAINT [FK_Omnicell_MediaGallery_Omnicell_MediaGalleryType];
GO
IF OBJECT_ID(N'[dbo].[FK_Omnicell_MediaGallery_Omnicell_ProductType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Omnicell_MediaGallery] DROP CONSTRAINT [FK_Omnicell_MediaGallery_Omnicell_ProductType];
GO
IF OBJECT_ID(N'[dbo].[FK_Omnicell_MediaGalleryGroup_Omnicell_MediaGallery]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Omnicell_MediaGalleryGroup] DROP CONSTRAINT [FK_Omnicell_MediaGalleryGroup_Omnicell_MediaGallery];
GO
IF OBJECT_ID(N'[dbo].[FK_Omnicell_MediaItemAudience_Omnicell_MediaAudienceType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Omnicell_MediaItemAudience] DROP CONSTRAINT [FK_Omnicell_MediaItemAudience_Omnicell_MediaAudienceType];
GO
IF OBJECT_ID(N'[dbo].[FK_Omnicell_MediaItemAudience_Omnicell_MediaGallery]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Omnicell_MediaItemAudience] DROP CONSTRAINT [FK_Omnicell_MediaItemAudience_Omnicell_MediaGallery];
GO
IF OBJECT_ID(N'[dbo].[FK_Omnicell_MediaItemMarket_Omnicell_MediaGallery]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Omnicell_MediaItemMarket] DROP CONSTRAINT [FK_Omnicell_MediaItemMarket_Omnicell_MediaGallery];
GO
IF OBJECT_ID(N'[dbo].[FK_Omnicell_MediaItemMarket_Omnicell_MediaMarket]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Omnicell_MediaItemMarket] DROP CONSTRAINT [FK_Omnicell_MediaItemMarket_Omnicell_MediaMarket];
GO
IF OBJECT_ID(N'[dbo].[FK_Omnicell_Ticket_Omnicell_TicketArea]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Omnicell_Ticket] DROP CONSTRAINT [FK_Omnicell_Ticket_Omnicell_TicketArea];
GO
IF OBJECT_ID(N'[dbo].[FK_Omnicell_Ticket_Omnicell_TicketArea1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Omnicell_Ticket] DROP CONSTRAINT [FK_Omnicell_Ticket_Omnicell_TicketArea1];
GO
IF OBJECT_ID(N'[dbo].[FK_Omnicell_Ticket_Omnicell_TicketResolutionCode]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Omnicell_Ticket] DROP CONSTRAINT [FK_Omnicell_Ticket_Omnicell_TicketResolutionCode];
GO
IF OBJECT_ID(N'[dbo].[FK_Omnicell_Ticket_Omnicell_TicketStatus]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Omnicell_Ticket] DROP CONSTRAINT [FK_Omnicell_Ticket_Omnicell_TicketStatus];
GO
IF OBJECT_ID(N'[dbo].[FK_Omnicell_Ticket_Omnicell_TicketSubStatus]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Omnicell_Ticket] DROP CONSTRAINT [FK_Omnicell_Ticket_Omnicell_TicketSubStatus];
GO
IF OBJECT_ID(N'[dbo].[FK_Omnicell_Ticket_Omnicell_TicketSymptomCode]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Omnicell_Ticket] DROP CONSTRAINT [FK_Omnicell_Ticket_Omnicell_TicketSymptomCode];
GO
IF OBJECT_ID(N'[dbo].[FK_Omnicell_TicketSubscription_Omnicell_Ticket]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Omnicell_TicketSubscription] DROP CONSTRAINT [FK_Omnicell_TicketSubscription_Omnicell_Ticket];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Omnicell_CourseType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_CourseType];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_CSN]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_CSN];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_CSNGroup]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_CSNGroup];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_CSNSubscription]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_CSNSubscription];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_Customer]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_Customer];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_CustomerOverride]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_CustomerOverride];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_EmailBatch]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_EmailBatch];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_EmailBatchRecipient]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_EmailBatchRecipient];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_EmailBatchRecipientStatus]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_EmailBatchRecipientStatus];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_EmailBatchRecipientTicket]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_EmailBatchRecipientTicket];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_EmailBatchStatus]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_EmailBatchStatus];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_EmailCounter]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_EmailCounter];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_Employee]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_Employee];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_EmployeeOverride]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_EmployeeOverride];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_FailedAccountAttempts]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_FailedAccountAttempts];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_GroupOrder]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_GroupOrder];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_InternationalDomain]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_InternationalDomain];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_KeyOpinionOptIn]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_KeyOpinionOptIn];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_MediaAudienceType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_MediaAudienceType];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_MediaDocumentType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_MediaDocumentType];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_MediaFormat]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_MediaFormat];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_MediaGallery]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_MediaGallery];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_MediaGalleryGroup]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_MediaGalleryGroup];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_MediaGalleryType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_MediaGalleryType];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_MediaItemAudience]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_MediaItemAudience];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_MediaItemMarket]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_MediaItemMarket];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_MediaMarket]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_MediaMarket];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_ProductType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_ProductType];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_RestrictedAddress]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_RestrictedAddress];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_Ticket]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_Ticket];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_TicketArea]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_TicketArea];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_TicketEmailInfo]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_TicketEmailInfo];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_TicketResolutionCode]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_TicketResolutionCode];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_TicketStatus]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_TicketStatus];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_TicketSubscription]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_TicketSubscription];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_TicketSubStatus]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_TicketSubStatus];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_TicketSymptomCode]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_TicketSymptomCode];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_TicketXml]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_TicketXml];
GO
IF OBJECT_ID(N'[dbo].[Omnicell_WikiMetaData2]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Omnicell_WikiMetaData2];
GO
IF OBJECT_ID(N'[dbo].[sysdiagrams]', 'U') IS NOT NULL
    DROP TABLE [dbo].[sysdiagrams];
GO
IF OBJECT_ID(N'[dbo].[WikiPageMetaDatas]', 'U') IS NOT NULL
    DROP TABLE [dbo].[WikiPageMetaDatas];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Customers'
CREATE TABLE [dbo].[Customers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [CSN] varchar(50)  NOT NULL,
    [Name] varchar(100)  NULL,
    [IsActive] bit  NOT NULL,
    [Updated] datetime  NULL,
    [AccountName] varchar(50)  NULL,
    [City] varchar(50)  NULL,
    [State] varchar(10)  NULL,
    [PostalCode] varchar(10)  NULL,
    [BusinessPartnerType] varchar(50)  NULL,
    [IDN] int  NULL,
    [IDName] varchar(50)  NULL,
    [GPO] int  NULL,
    [GPOName] varchar(50)  NULL
);
GO

-- Creating table 'CustomerOverrides'
CREATE TABLE [dbo].[CustomerOverrides] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Email] varchar(250)  NULL,
    [Created] datetime  NULL
);
GO

-- Creating table 'Employees'
CREATE TABLE [dbo].[Employees] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Email] varchar(150)  NOT NULL,
    [IsActive] bit  NOT NULL,
    [FirstName] varchar(50)  NULL,
    [LastName] varchar(50)  NULL,
    [ADID] varchar(50)  NULL,
    [EmployeeNumber] int  NULL,
    [Updated] datetime  NULL
);
GO

-- Creating table 'EmployeeOverrides'
CREATE TABLE [dbo].[EmployeeOverrides] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Email] varchar(250)  NULL,
    [Created] datetime  NULL
);
GO

-- Creating table 'FailedAccountAttempts'
CREATE TABLE [dbo].[FailedAccountAttempts] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FirstName] varchar(250)  NULL,
    [LastName] varchar(250)  NULL,
    [Email] varchar(250)  NULL,
    [Zip] varchar(10)  NULL,
    [CSN] varchar(50)  NULL,
    [FailReason] varchar(50)  NULL,
    [Date] datetime  NULL,
    [IpAddress] varchar(15)  NULL
);
GO

-- Creating table 'RestrictedAddresses'
CREATE TABLE [dbo].[RestrictedAddresses] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Address] varchar(250)  NOT NULL,
    [Created] datetime  NOT NULL,
    [IsDomain] bit  NOT NULL
);
GO

-- Creating table 'Tickets'
CREATE TABLE [dbo].[Tickets] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IsActive] bit  NOT NULL,
    [Updated] datetime  NULL,
    [Number] varchar(50)  NULL,
    [Abstract] varchar(max)  NULL,
    [Status] varchar(250)  NULL,
    [SubStatus] varchar(250)  NULL,
    [CSN] varchar(50)  NULL,
    [SymptomCode] varchar(250)  NULL,
    [ResolutionCode] varchar(250)  NULL,
    [LastActivityUpdate] varchar(50)  NULL,
    [StatusId] int  NULL,
    [SubStatusId] int  NULL,
    [SymptomCodeId] int  NULL,
    [SymptomAreaId] int  NULL,
    [ResolutionCodeId] int  NULL,
    [ResolutionAreaId] int  NULL,
    [LastActivityUpdateDate] datetime  NULL,
    [SerialNumber] varchar(50)  NULL
);
GO

-- Creating table 'OmnicellCSNs'
CREATE TABLE [dbo].[OmnicellCSNs] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [CSN] varchar(50)  NULL,
    [CSNGroupId] int  NULL,
    [EditDate] datetime  NULL,
    [CreateDate] datetime  NULL
);
GO

-- Creating table 'CSNGroups'
CREATE TABLE [dbo].[CSNGroups] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [CreateDate] datetime  NULL,
    [EditDate] datetime  NULL
);
GO

-- Creating table 'GroupOrders'
CREATE TABLE [dbo].[GroupOrders] (
    [Id] int  NOT NULL,
    [GroupId] int  NOT NULL,
    [OrdNo] int  NOT NULL
);
GO

-- Creating table 'MediaAudienceTypes'
CREATE TABLE [dbo].[MediaAudienceTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Value] varchar(50)  NULL
);
GO

-- Creating table 'MediaDocumentTypes'
CREATE TABLE [dbo].[MediaDocumentTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Value] varchar(50)  NULL
);
GO

-- Creating table 'MediaFormats'
CREATE TABLE [dbo].[MediaFormats] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Value] varchar(75)  NULL
);
GO

-- Creating table 'MediaGalleries'
CREATE TABLE [dbo].[MediaGalleries] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [MediaGalleryId] int  NULL,
    [MediaGaleryTypeId] int  NULL,
    [DocumentTypeId] int  NULL,
    [MediaFormatId] int  NULL,
    [PublishDate] datetime  NULL,
    [ScheduledDate] datetime  NULL,
    [ProductTypeId] int  NULL
);
GO

-- Creating table 'MediaGalleryGroups'
CREATE TABLE [dbo].[MediaGalleryGroups] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [MediaGalleryId] int  NULL,
    [GroupId] int  NULL,
    [OmnicellMediaId] int  NULL
);
GO

-- Creating table 'MediaGalleryTypes'
CREATE TABLE [dbo].[MediaGalleryTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Value] varchar(50)  NULL
);
GO

-- Creating table 'MediaItemAudiences'
CREATE TABLE [dbo].[MediaItemAudiences] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [MediaAudienceTypeId] int  NULL,
    [MediaItemId] int  NULL
);
GO

-- Creating table 'MediaMarkets'
CREATE TABLE [dbo].[MediaMarkets] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Value] varchar(50)  NULL
);
GO

-- Creating table 'ProductTypes'
CREATE TABLE [dbo].[ProductTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Value] varchar(50)  NULL
);
GO

-- Creating table 'TicketAreas'
CREATE TABLE [dbo].[TicketAreas] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Value] varchar(50)  NULL
);
GO

-- Creating table 'TicketResolutionCodes'
CREATE TABLE [dbo].[TicketResolutionCodes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Value] varchar(50)  NULL
);
GO

-- Creating table 'TicketStatuses'
CREATE TABLE [dbo].[TicketStatuses] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Value] varchar(50)  NULL
);
GO

-- Creating table 'TicketSubscriptions'
CREATE TABLE [dbo].[TicketSubscriptions] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TicketId] int  NULL,
    [UserId] int  NULL,
    [CSN] nvarchar(max)  NULL
);
GO

-- Creating table 'TicketSubStatuses'
CREATE TABLE [dbo].[TicketSubStatuses] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Value] varchar(50)  NULL
);
GO

-- Creating table 'TicketSymptomCodes'
CREATE TABLE [dbo].[TicketSymptomCodes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Value] varchar(50)  NULL
);
GO

-- Creating table 'EmailBatches'
CREATE TABLE [dbo].[EmailBatches] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ScheduledDate] datetime  NULL,
    [StatusId] int  NULL,
    [LastAttemptedDate] datetime  NULL,
    [SucceedDate] datetime  NULL
);
GO

-- Creating table 'EmailBatchRecipients'
CREATE TABLE [dbo].[EmailBatchRecipients] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [BatchId] int  NULL,
    [UserId] int  NULL,
    [Email] varchar(250)  NULL,
    [StatusId] int  NULL,
    [DateSubmitted] datetime  NULL
);
GO

-- Creating table 'EmailBatchRecipientStatuses'
CREATE TABLE [dbo].[EmailBatchRecipientStatuses] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Value] varchar(50)  NULL
);
GO

-- Creating table 'EmailBatchRecipientTickets'
CREATE TABLE [dbo].[EmailBatchRecipientTickets] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [EmailBatchRecipientId] int  NULL,
    [TicketId] int  NULL
);
GO

-- Creating table 'EmailBatchStatuses'
CREATE TABLE [dbo].[EmailBatchStatuses] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Value] varchar(50)  NULL
);
GO

-- Creating table 'TicketEmailInfoes'
CREATE TABLE [dbo].[TicketEmailInfoes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IsImmediateUpdate] int  NULL,
    [Email] varchar(250)  NULL,
    [UserId] int  NULL
);
GO

-- Creating table 'MediaItemMarkets'
CREATE TABLE [dbo].[MediaItemMarkets] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [MediaItemId] int  NULL,
    [MediaMarketId] int  NULL
);
GO

-- Creating table 'CourseTypes'
CREATE TABLE [dbo].[CourseTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Value] nvarchar(75)  NULL
);
GO

-- Creating table 'sysdiagrams'
CREATE TABLE [dbo].[sysdiagrams] (
    [name] nvarchar(128)  NOT NULL,
    [principal_id] int  NOT NULL,
    [diagram_id] int IDENTITY(1,1) NOT NULL,
    [version] int  NULL,
    [definition] varbinary(max)  NULL
);
GO

-- Creating table 'Omnicell_CSNSubscription'
CREATE TABLE [dbo].[Omnicell_CSNSubscription] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [CSN] varchar(50)  NOT NULL,
    [Facility] varchar(100)  NOT NULL
);
GO

-- Creating table 'Omnicell_EmailCounter'
CREATE TABLE [dbo].[Omnicell_EmailCounter] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [UserId] int  NOT NULL,
    [Date] datetime  NOT NULL
);
GO

-- Creating table 'Omnicell_InternationalDomain'
CREATE TABLE [dbo].[Omnicell_InternationalDomain] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [EmailDomain] varchar(100)  NOT NULL
);
GO

-- Creating table 'Omnicell_KeyOpinionOptIn'
CREATE TABLE [dbo].[Omnicell_KeyOpinionOptIn] (
    [UserId] int  NOT NULL,
    [Opt_In] int  NOT NULL,
    [Updated] datetime  NOT NULL
);
GO

-- Creating table 'Omnicell_WikiMetaData2'
CREATE TABLE [dbo].[Omnicell_WikiMetaData2] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ContentId] uniqueidentifier  NULL,
    [Product] nvarchar(max)  NULL,
    [ProductVersion] nvarchar(10)  NULL,
    [Audiences] nvarchar(max)  NULL,
    [Interfaces] nvarchar(max)  NULL,
    [Features] nvarchar(max)  NULL,
    [DocumentType] nvarchar(50)  NULL,
    [GroupId] int  NULL,
    [WikiId] int  NULL,
    [RevNum] nvarchar(50)  NULL,
    [PartName] nvarchar(50)  NULL
);
GO

-- Creating table 'WikiPageMetaDatas'
CREATE TABLE [dbo].[WikiPageMetaDatas] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ContentId] nvarchar(250)  NULL,
    [Product] nvarchar(250)  NULL,
    [ProductVersion] nvarchar(250)  NULL,
    [Audience] nvarchar(250)  NULL,
    [Interfaces] nvarchar(250)  NULL,
    [Features] nvarchar(max)  NULL,
    [DocumentType] nvarchar(10)  NULL,
    [GroupId] int  NULL,
    [WikiId] int  NULL,
    [WikiPageId] int  NULL,
    [ParentPageId] int  NULL,
    [OrderNumber] int  NULL,
    [PageKey] nvarchar(250)  NULL,
    [ShortDesc] nvarchar(max)  NULL,
    [RevNum] nvarchar(50)  NULL,
    [PartName] nvarchar(50)  NULL
);
GO

-- Creating table 'Omnicell_TicketXml'
CREATE TABLE [dbo].[Omnicell_TicketXml] (
    [id] int IDENTITY(1,1) NOT NULL,
    [xmldata] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Customers'
ALTER TABLE [dbo].[Customers]
ADD CONSTRAINT [PK_Customers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CustomerOverrides'
ALTER TABLE [dbo].[CustomerOverrides]
ADD CONSTRAINT [PK_CustomerOverrides]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Employees'
ALTER TABLE [dbo].[Employees]
ADD CONSTRAINT [PK_Employees]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EmployeeOverrides'
ALTER TABLE [dbo].[EmployeeOverrides]
ADD CONSTRAINT [PK_EmployeeOverrides]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'FailedAccountAttempts'
ALTER TABLE [dbo].[FailedAccountAttempts]
ADD CONSTRAINT [PK_FailedAccountAttempts]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'RestrictedAddresses'
ALTER TABLE [dbo].[RestrictedAddresses]
ADD CONSTRAINT [PK_RestrictedAddresses]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Tickets'
ALTER TABLE [dbo].[Tickets]
ADD CONSTRAINT [PK_Tickets]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'OmnicellCSNs'
ALTER TABLE [dbo].[OmnicellCSNs]
ADD CONSTRAINT [PK_OmnicellCSNs]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CSNGroups'
ALTER TABLE [dbo].[CSNGroups]
ADD CONSTRAINT [PK_CSNGroups]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'GroupOrders'
ALTER TABLE [dbo].[GroupOrders]
ADD CONSTRAINT [PK_GroupOrders]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'MediaAudienceTypes'
ALTER TABLE [dbo].[MediaAudienceTypes]
ADD CONSTRAINT [PK_MediaAudienceTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'MediaDocumentTypes'
ALTER TABLE [dbo].[MediaDocumentTypes]
ADD CONSTRAINT [PK_MediaDocumentTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'MediaFormats'
ALTER TABLE [dbo].[MediaFormats]
ADD CONSTRAINT [PK_MediaFormats]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'MediaGalleries'
ALTER TABLE [dbo].[MediaGalleries]
ADD CONSTRAINT [PK_MediaGalleries]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'MediaGalleryGroups'
ALTER TABLE [dbo].[MediaGalleryGroups]
ADD CONSTRAINT [PK_MediaGalleryGroups]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'MediaGalleryTypes'
ALTER TABLE [dbo].[MediaGalleryTypes]
ADD CONSTRAINT [PK_MediaGalleryTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'MediaItemAudiences'
ALTER TABLE [dbo].[MediaItemAudiences]
ADD CONSTRAINT [PK_MediaItemAudiences]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'MediaMarkets'
ALTER TABLE [dbo].[MediaMarkets]
ADD CONSTRAINT [PK_MediaMarkets]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ProductTypes'
ALTER TABLE [dbo].[ProductTypes]
ADD CONSTRAINT [PK_ProductTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TicketAreas'
ALTER TABLE [dbo].[TicketAreas]
ADD CONSTRAINT [PK_TicketAreas]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TicketResolutionCodes'
ALTER TABLE [dbo].[TicketResolutionCodes]
ADD CONSTRAINT [PK_TicketResolutionCodes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TicketStatuses'
ALTER TABLE [dbo].[TicketStatuses]
ADD CONSTRAINT [PK_TicketStatuses]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TicketSubscriptions'
ALTER TABLE [dbo].[TicketSubscriptions]
ADD CONSTRAINT [PK_TicketSubscriptions]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TicketSubStatuses'
ALTER TABLE [dbo].[TicketSubStatuses]
ADD CONSTRAINT [PK_TicketSubStatuses]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TicketSymptomCodes'
ALTER TABLE [dbo].[TicketSymptomCodes]
ADD CONSTRAINT [PK_TicketSymptomCodes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EmailBatches'
ALTER TABLE [dbo].[EmailBatches]
ADD CONSTRAINT [PK_EmailBatches]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EmailBatchRecipients'
ALTER TABLE [dbo].[EmailBatchRecipients]
ADD CONSTRAINT [PK_EmailBatchRecipients]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EmailBatchRecipientStatuses'
ALTER TABLE [dbo].[EmailBatchRecipientStatuses]
ADD CONSTRAINT [PK_EmailBatchRecipientStatuses]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EmailBatchRecipientTickets'
ALTER TABLE [dbo].[EmailBatchRecipientTickets]
ADD CONSTRAINT [PK_EmailBatchRecipientTickets]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EmailBatchStatuses'
ALTER TABLE [dbo].[EmailBatchStatuses]
ADD CONSTRAINT [PK_EmailBatchStatuses]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TicketEmailInfoes'
ALTER TABLE [dbo].[TicketEmailInfoes]
ADD CONSTRAINT [PK_TicketEmailInfoes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'MediaItemMarkets'
ALTER TABLE [dbo].[MediaItemMarkets]
ADD CONSTRAINT [PK_MediaItemMarkets]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CourseTypes'
ALTER TABLE [dbo].[CourseTypes]
ADD CONSTRAINT [PK_CourseTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [diagram_id] in table 'sysdiagrams'
ALTER TABLE [dbo].[sysdiagrams]
ADD CONSTRAINT [PK_sysdiagrams]
    PRIMARY KEY CLUSTERED ([diagram_id] ASC);
GO

-- Creating primary key on [Id] in table 'Omnicell_CSNSubscription'
ALTER TABLE [dbo].[Omnicell_CSNSubscription]
ADD CONSTRAINT [PK_Omnicell_CSNSubscription]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Omnicell_EmailCounter'
ALTER TABLE [dbo].[Omnicell_EmailCounter]
ADD CONSTRAINT [PK_Omnicell_EmailCounter]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Omnicell_InternationalDomain'
ALTER TABLE [dbo].[Omnicell_InternationalDomain]
ADD CONSTRAINT [PK_Omnicell_InternationalDomain]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [UserId] in table 'Omnicell_KeyOpinionOptIn'
ALTER TABLE [dbo].[Omnicell_KeyOpinionOptIn]
ADD CONSTRAINT [PK_Omnicell_KeyOpinionOptIn]
    PRIMARY KEY CLUSTERED ([UserId] ASC);
GO

-- Creating primary key on [Id] in table 'Omnicell_WikiMetaData2'
ALTER TABLE [dbo].[Omnicell_WikiMetaData2]
ADD CONSTRAINT [PK_Omnicell_WikiMetaData2]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'WikiPageMetaDatas'
ALTER TABLE [dbo].[WikiPageMetaDatas]
ADD CONSTRAINT [PK_WikiPageMetaDatas]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [id] in table 'Omnicell_TicketXml'
ALTER TABLE [dbo].[Omnicell_TicketXml]
ADD CONSTRAINT [PK_Omnicell_TicketXml]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [CSNGroupId] in table 'OmnicellCSNs'
ALTER TABLE [dbo].[OmnicellCSNs]
ADD CONSTRAINT [FK_Omnicell_CSN_Omnicell_CSNGroup]
    FOREIGN KEY ([CSNGroupId])
    REFERENCES [dbo].[CSNGroups]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Omnicell_CSN_Omnicell_CSNGroup'
CREATE INDEX [IX_FK_Omnicell_CSN_Omnicell_CSNGroup]
ON [dbo].[OmnicellCSNs]
    ([CSNGroupId]);
GO

-- Creating foreign key on [MediaAudienceTypeId] in table 'MediaItemAudiences'
ALTER TABLE [dbo].[MediaItemAudiences]
ADD CONSTRAINT [FK_Omnicell_MediaItemAudience_Omnicell_MediaAudienceType]
    FOREIGN KEY ([MediaAudienceTypeId])
    REFERENCES [dbo].[MediaAudienceTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Omnicell_MediaItemAudience_Omnicell_MediaAudienceType'
CREATE INDEX [IX_FK_Omnicell_MediaItemAudience_Omnicell_MediaAudienceType]
ON [dbo].[MediaItemAudiences]
    ([MediaAudienceTypeId]);
GO

-- Creating foreign key on [DocumentTypeId] in table 'MediaGalleries'
ALTER TABLE [dbo].[MediaGalleries]
ADD CONSTRAINT [FK_Omnicell_MediaGallery_Omnicell_MediaDocumentType]
    FOREIGN KEY ([DocumentTypeId])
    REFERENCES [dbo].[MediaDocumentTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Omnicell_MediaGallery_Omnicell_MediaDocumentType'
CREATE INDEX [IX_FK_Omnicell_MediaGallery_Omnicell_MediaDocumentType]
ON [dbo].[MediaGalleries]
    ([DocumentTypeId]);
GO

-- Creating foreign key on [MediaFormatId] in table 'MediaGalleries'
ALTER TABLE [dbo].[MediaGalleries]
ADD CONSTRAINT [FK_Omnicell_MediaGallery_Omnicell_MediaFormat]
    FOREIGN KEY ([MediaFormatId])
    REFERENCES [dbo].[MediaFormats]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Omnicell_MediaGallery_Omnicell_MediaFormat'
CREATE INDEX [IX_FK_Omnicell_MediaGallery_Omnicell_MediaFormat]
ON [dbo].[MediaGalleries]
    ([MediaFormatId]);
GO

-- Creating foreign key on [MediaGaleryTypeId] in table 'MediaGalleries'
ALTER TABLE [dbo].[MediaGalleries]
ADD CONSTRAINT [FK_Omnicell_MediaGallery_Omnicell_MediaGalleryType]
    FOREIGN KEY ([MediaGaleryTypeId])
    REFERENCES [dbo].[MediaGalleryTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Omnicell_MediaGallery_Omnicell_MediaGalleryType'
CREATE INDEX [IX_FK_Omnicell_MediaGallery_Omnicell_MediaGalleryType]
ON [dbo].[MediaGalleries]
    ([MediaGaleryTypeId]);
GO

-- Creating foreign key on [ProductTypeId] in table 'MediaGalleries'
ALTER TABLE [dbo].[MediaGalleries]
ADD CONSTRAINT [FK_Omnicell_MediaGallery_Omnicell_ProductType]
    FOREIGN KEY ([ProductTypeId])
    REFERENCES [dbo].[ProductTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Omnicell_MediaGallery_Omnicell_ProductType'
CREATE INDEX [IX_FK_Omnicell_MediaGallery_Omnicell_ProductType]
ON [dbo].[MediaGalleries]
    ([ProductTypeId]);
GO

-- Creating foreign key on [OmnicellMediaId] in table 'MediaGalleryGroups'
ALTER TABLE [dbo].[MediaGalleryGroups]
ADD CONSTRAINT [FK_Omnicell_MediaGalleryGroup_Omnicell_MediaGallery]
    FOREIGN KEY ([OmnicellMediaId])
    REFERENCES [dbo].[MediaGalleries]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Omnicell_MediaGalleryGroup_Omnicell_MediaGallery'
CREATE INDEX [IX_FK_Omnicell_MediaGalleryGroup_Omnicell_MediaGallery]
ON [dbo].[MediaGalleryGroups]
    ([OmnicellMediaId]);
GO

-- Creating foreign key on [MediaItemId] in table 'MediaItemAudiences'
ALTER TABLE [dbo].[MediaItemAudiences]
ADD CONSTRAINT [FK_Omnicell_MediaItemAudience_Omnicell_MediaGallery]
    FOREIGN KEY ([MediaItemId])
    REFERENCES [dbo].[MediaGalleries]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Omnicell_MediaItemAudience_Omnicell_MediaGallery'
CREATE INDEX [IX_FK_Omnicell_MediaItemAudience_Omnicell_MediaGallery]
ON [dbo].[MediaItemAudiences]
    ([MediaItemId]);
GO

-- Creating foreign key on [SymptomAreaId] in table 'Tickets'
ALTER TABLE [dbo].[Tickets]
ADD CONSTRAINT [FK_Omnicell_Ticket_Omnicell_TicketArea]
    FOREIGN KEY ([SymptomAreaId])
    REFERENCES [dbo].[TicketAreas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Omnicell_Ticket_Omnicell_TicketArea'
CREATE INDEX [IX_FK_Omnicell_Ticket_Omnicell_TicketArea]
ON [dbo].[Tickets]
    ([SymptomAreaId]);
GO

-- Creating foreign key on [ResolutionAreaId] in table 'Tickets'
ALTER TABLE [dbo].[Tickets]
ADD CONSTRAINT [FK_Omnicell_Ticket_Omnicell_TicketArea1]
    FOREIGN KEY ([ResolutionAreaId])
    REFERENCES [dbo].[TicketAreas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Omnicell_Ticket_Omnicell_TicketArea1'
CREATE INDEX [IX_FK_Omnicell_Ticket_Omnicell_TicketArea1]
ON [dbo].[Tickets]
    ([ResolutionAreaId]);
GO

-- Creating foreign key on [ResolutionCodeId] in table 'Tickets'
ALTER TABLE [dbo].[Tickets]
ADD CONSTRAINT [FK_Omnicell_Ticket_Omnicell_TicketResolutionCode]
    FOREIGN KEY ([ResolutionCodeId])
    REFERENCES [dbo].[TicketResolutionCodes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Omnicell_Ticket_Omnicell_TicketResolutionCode'
CREATE INDEX [IX_FK_Omnicell_Ticket_Omnicell_TicketResolutionCode]
ON [dbo].[Tickets]
    ([ResolutionCodeId]);
GO

-- Creating foreign key on [StatusId] in table 'Tickets'
ALTER TABLE [dbo].[Tickets]
ADD CONSTRAINT [FK_Omnicell_Ticket_Omnicell_TicketStatus]
    FOREIGN KEY ([StatusId])
    REFERENCES [dbo].[TicketStatuses]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Omnicell_Ticket_Omnicell_TicketStatus'
CREATE INDEX [IX_FK_Omnicell_Ticket_Omnicell_TicketStatus]
ON [dbo].[Tickets]
    ([StatusId]);
GO

-- Creating foreign key on [SubStatusId] in table 'Tickets'
ALTER TABLE [dbo].[Tickets]
ADD CONSTRAINT [FK_Omnicell_Ticket_Omnicell_TicketSubStatus]
    FOREIGN KEY ([SubStatusId])
    REFERENCES [dbo].[TicketSubStatuses]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Omnicell_Ticket_Omnicell_TicketSubStatus'
CREATE INDEX [IX_FK_Omnicell_Ticket_Omnicell_TicketSubStatus]
ON [dbo].[Tickets]
    ([SubStatusId]);
GO

-- Creating foreign key on [SymptomCodeId] in table 'Tickets'
ALTER TABLE [dbo].[Tickets]
ADD CONSTRAINT [FK_Omnicell_Ticket_Omnicell_TicketSymptomCode]
    FOREIGN KEY ([SymptomCodeId])
    REFERENCES [dbo].[TicketSymptomCodes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Omnicell_Ticket_Omnicell_TicketSymptomCode'
CREATE INDEX [IX_FK_Omnicell_Ticket_Omnicell_TicketSymptomCode]
ON [dbo].[Tickets]
    ([SymptomCodeId]);
GO

-- Creating foreign key on [TicketId] in table 'TicketSubscriptions'
ALTER TABLE [dbo].[TicketSubscriptions]
ADD CONSTRAINT [FK_Omnicell_TicketSubscription_Omnicell_Ticket]
    FOREIGN KEY ([TicketId])
    REFERENCES [dbo].[Tickets]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Omnicell_TicketSubscription_Omnicell_Ticket'
CREATE INDEX [IX_FK_Omnicell_TicketSubscription_Omnicell_Ticket]
ON [dbo].[TicketSubscriptions]
    ([TicketId]);
GO

-- Creating foreign key on [StatusId] in table 'EmailBatches'
ALTER TABLE [dbo].[EmailBatches]
ADD CONSTRAINT [FK_Omnicell_EmailBatch_Omnicell_EmailBatchStatus]
    FOREIGN KEY ([StatusId])
    REFERENCES [dbo].[EmailBatchStatuses]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Omnicell_EmailBatch_Omnicell_EmailBatchStatus'
CREATE INDEX [IX_FK_Omnicell_EmailBatch_Omnicell_EmailBatchStatus]
ON [dbo].[EmailBatches]
    ([StatusId]);
GO

-- Creating foreign key on [BatchId] in table 'EmailBatchRecipients'
ALTER TABLE [dbo].[EmailBatchRecipients]
ADD CONSTRAINT [FK_Omnicell_EmailBatchRecipient_Omnicell_EmailBatch]
    FOREIGN KEY ([BatchId])
    REFERENCES [dbo].[EmailBatches]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Omnicell_EmailBatchRecipient_Omnicell_EmailBatch'
CREATE INDEX [IX_FK_Omnicell_EmailBatchRecipient_Omnicell_EmailBatch]
ON [dbo].[EmailBatchRecipients]
    ([BatchId]);
GO

-- Creating foreign key on [StatusId] in table 'EmailBatchRecipients'
ALTER TABLE [dbo].[EmailBatchRecipients]
ADD CONSTRAINT [FK_Omnicell_EmailBatchRecipient_Omnicell_EmailBatchRecipientStatus]
    FOREIGN KEY ([StatusId])
    REFERENCES [dbo].[EmailBatchRecipientStatuses]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Omnicell_EmailBatchRecipient_Omnicell_EmailBatchRecipientStatus'
CREATE INDEX [IX_FK_Omnicell_EmailBatchRecipient_Omnicell_EmailBatchRecipientStatus]
ON [dbo].[EmailBatchRecipients]
    ([StatusId]);
GO

-- Creating foreign key on [EmailBatchRecipientId] in table 'EmailBatchRecipientTickets'
ALTER TABLE [dbo].[EmailBatchRecipientTickets]
ADD CONSTRAINT [FK_Omnicell_EmailBatchRecipientTicket_Omnicell_EmailBatchRecipient]
    FOREIGN KEY ([EmailBatchRecipientId])
    REFERENCES [dbo].[EmailBatchRecipients]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Omnicell_EmailBatchRecipientTicket_Omnicell_EmailBatchRecipient'
CREATE INDEX [IX_FK_Omnicell_EmailBatchRecipientTicket_Omnicell_EmailBatchRecipient]
ON [dbo].[EmailBatchRecipientTickets]
    ([EmailBatchRecipientId]);
GO

-- Creating foreign key on [TicketId] in table 'EmailBatchRecipientTickets'
ALTER TABLE [dbo].[EmailBatchRecipientTickets]
ADD CONSTRAINT [FK_Omnicell_EmailBatchRecipientTicket_Omnicell_Ticket]
    FOREIGN KEY ([TicketId])
    REFERENCES [dbo].[Tickets]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Omnicell_EmailBatchRecipientTicket_Omnicell_Ticket'
CREATE INDEX [IX_FK_Omnicell_EmailBatchRecipientTicket_Omnicell_Ticket]
ON [dbo].[EmailBatchRecipientTickets]
    ([TicketId]);
GO

-- Creating foreign key on [MediaItemId] in table 'MediaItemMarkets'
ALTER TABLE [dbo].[MediaItemMarkets]
ADD CONSTRAINT [FK_Omnicell_MediaItemMarket_Omnicell_MediaGallery]
    FOREIGN KEY ([MediaItemId])
    REFERENCES [dbo].[MediaGalleries]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Omnicell_MediaItemMarket_Omnicell_MediaGallery'
CREATE INDEX [IX_FK_Omnicell_MediaItemMarket_Omnicell_MediaGallery]
ON [dbo].[MediaItemMarkets]
    ([MediaItemId]);
GO

-- Creating foreign key on [MediaMarketId] in table 'MediaItemMarkets'
ALTER TABLE [dbo].[MediaItemMarkets]
ADD CONSTRAINT [FK_Omnicell_MediaItemMarket_Omnicell_MediaMarket]
    FOREIGN KEY ([MediaMarketId])
    REFERENCES [dbo].[MediaMarkets]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Omnicell_MediaItemMarket_Omnicell_MediaMarket'
CREATE INDEX [IX_FK_Omnicell_MediaItemMarket_Omnicell_MediaMarket]
ON [dbo].[MediaItemMarkets]
    ([MediaMarketId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------