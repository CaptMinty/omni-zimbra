﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Omnicell.Data.Model
{
    public class EmailCountViewModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public DateTime Date { get; set; }
    }
}
