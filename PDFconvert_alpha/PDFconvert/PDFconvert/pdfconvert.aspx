﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="pdfconvert.aspx.cs" Inherits="PDFconvert.pdfconvert" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
    .title.topictitle1{
    font-family: Myriad Pro Light Cond;
  font-size: 26pt;
  color: #7AC142;
}
.shortdesc{
  font-size: 11pt;
     font-family: helvetica;
}

.post-name{
   font-family: Myriad Pro Light Cond;
  font-size: 14pt;
  color: #7AC142;
}

.post-shortDesc{
  font-size: 11pt;
     font-family: Myriad Pro Light SemiCond;
}
.title.sectiontitle{
    font-family: Myriad Pro Light Cond;
  font-size: 19pt;
  color: #7AC142;
}
.ExistingPageLink{
  color: #0000E1;
  text-decoration: underline;
     font-family: Myriad Pro Light SemiCond;
}
.p{
   font-size: 11pt;
      font-family: Myriad Pro Light SemiCond;
}
.li{
   font-size: 11pt;
   margin: 0 0 3pt 0;
      font-family: Myriad Pro Light SemiCond;
}

.li.step
{
 margin: 11pt 0pt 11pt 0pt;
}
.ul{
    padding-left:18pt;
    margin-top: 11pt;
       font-family: Myriad Pro Light SemiCond;
}
.image{
    margin: 12pt 0pt 12pt 0pt;
padding-left: 20px;

}
.ol{
    padding-left:18pt;
    margin-top: 11pt;
       font-family: Myriad Pro Light SemiCond;
}
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Button ID="Button2" runat="server" Text="Convert to PDF"  onclick="ConvertPDF_Click" />    
    </div>

    <div id="divTest" runat="server">
<a name="task_jlp_vdr_2n"><!-- --></a>

<h1 class="title topictitle1">Connecting oXygen to our SharePoint CMS</h1>
<div class="body taskbody"><p class="shortdesc">oXygen provides the ability to author and edit maps or topics directly on our server through its browse remote files, Data Source Explorer, and SharePoint-specific contextual menu options.</p>
<div class="section prereq p"><p class="p">Download and install the current installation of oXygen Author from the <a class="xref" href="http://www.oxygenxml.com/download_oxygenxml_author.html" target="_blank">oXygen website</a>. You will need a license key, which you can obtain from your manager or DITA lead.</p>
</div>

<ol class="ol steps"><li class="li step"><span class="ph cmd">Select database perspective (<span class="ph uicontrol">Window</span> &gt; <span class="ph uicontrol">Open perspective</span> &gt; <span class="ph uicontrol">Database</span> or click on the database icon on the upper righthand screen <a name="task_jlp_vdr_2n__image_xmp_kpr_2n"><!-- --></a><img class="image" id="task_jlp_vdr_2n__image_xmp_kpr_2n" src="http://findicons.com/files/icons/287/maneki_neko/128/cat_6_2.png" alt=" ">).</span></li>
<li class="li step"><span class="ph cmd">In the Data Source Explorer view, click the <span class="ph uicontrol">Configure Database Sources</span> button<a name="task_jlp_vdr_2n__image_hgt_qpr_2n"><!-- --></a><img class="image" id="task_jlp_vdr_2n__image_hgt_qpr_2n" src="http://findicons.com/files/icons/287/maneki_neko/128/cat_6_2.png" alt=" ">.</span> <div class="note tip"><span class="tiptitle">Tip:</span> If the Data Source Explorer view does not automatically open, select <span class="ph uicontrol">Window</span> &gt; <span class="ph uicontrol">Show View</span> &gt; <span class="ph uicontrol">Data Source Explorer</span>.</div>
 <br><a name="task_jlp_vdr_2n__image_llx_drr_2n"><!-- --></a><img class="image" id="task_jlp_vdr_2n__image_llx_drr_2n" src="http://findicons.com/files/icons/287/maneki_neko/128/cat_6_2.png" height="324" width="255" alt=" "><br></li>
<li class="li step"><span class="ph cmd">Highlight <span class="ph uicontrol">SharePoint</span> in the Data Sources box.</span></li>
<li class="li step"><span class="ph cmd">Click the <span class="ph uicontrol">+</span> button underneath the Connections box (not the Data Sources box).</span> The connections box opens.<br><a name="task_jlp_vdr_2n__image_rf1_mrr_2n"><!-- --></a><img class="image" id="task_jlp_vdr_2n__image_rf1_mrr_2n" src="http://findicons.com/files/icons/287/maneki_neko/128/cat_6_2.png" alt=" "><br></li>
<li class="li step"><span class="ph cmd">Fill in the details as follows to set up a connection directly to the maps library:</span> <dl class="dl"><dt class="dt dlterm">Name</dt>
<dd class="dd">Whatever name you want to use to identify the specific connection, for example Omnicell Maps.</dd>
<dt class="dt dlterm">Data Source</dt>
<dd class="dd">Should be set to SharePoint. If not, select SharePoint from the drop down menu.</dd>
<dt class="dt dlterm">SharePoint URL</dt>
<dd class="dd"><span class="ph filepath">http://infodevcms.omnicell.com/Repository/Maps1/</span></dd>
<dt class="dt dlterm">Domain</dt>
<dd class="dd">Omnicell</dd>
<dt class="dt dlterm">User</dt>
<dd class="dd">Your standard user login</dd>
<dt class="dt dlterm">Password</dt>
<dd class="dd">Your standard password</dd>
</dl>
 <div class="note note"><span class="notetitle">Note:</span> When your password changes, you will need to edit this information. Use the wrench icon to edit your connection information.</div>
</li>
<li class="li step"><span class="ph cmd">In the main Connections box, click <span class="ph uicontrol">Apply</span> and then <span class="ph uicontrol">OK</span>.</span></li>
<li class="li step"><span class="ph cmd">Repeat the above steps for the additional folders or highlight your configured connection, click the <span class="ph uicontrol">Duplicate</span> button (icon that looks like double sheets of paper), and edit the details.</span> Use <span class="ph filepath">http://infodevcms.omnicell.com/Repository/Pictures/</span> and <span class="ph filepath">http://infodevcms.omnicell.com/Repository/Topics/</span> for the SharePoint URL when configuring the additional folders. If you want a generic connection to the CMS rather than to a specific library, use <span class="ph filepath">http://infodevcms.omnicell.com</span></li>
</ol>
</div>
<div class="related-links">
<div class="familylinks">
<div class="nextlink"><strong>Next topic:</strong> <span><span>How do I add a map to the project view?</span></span></div>
</div>
</div>




    </div>
    <asp:TextBox ID="TextBox1" runat="server" Width="799px"></asp:TextBox>
   

<div id="cssFile" runat="server">
.dita-page .title.topictitle1{
    font-family: Myriad Pro Light Cond;
  font-size: 26pt;
  color: #7AC142;
}
.dita-page .shortdesc{
  font-size: 11pt;
}

.dita-page .post-name{
   font-family: Myriad Pro Light Cond;
  font-size: 14pt;
  color: #7AC142;
}

.dita-page .post-shortDesc{
  font-size: 11pt;
}
.dita-page .title.sectiontitle{
    font-family: Myriad Pro Light Cond;
  font-size: 19pt;
  color: #7AC142;
}
.dita-page  .ExistingPageLink{
  color: #0000E1;
  text-decoration: underline;
}
.dita-page .p{
   font-size: 11pt;
}
.dita-page .li{
   font-size: 11pt;
   margin: 0 0 3pt 0;
}

.dita-page .li.step
{
 margin: 11pt 0pt 11pt 0pt;
}
.dita-page .ul{
    padding-left:18pt;
    margin-top: 11pt;
}
.dita-page .image{
    margin: 12pt 0pt 12pt 0pt;
padding-left: 20px;

}
.dita-page .ol{
    padding-left:18pt;
    margin-top: 11pt;
}
</div>
    

    </form>
</body>
</html>
